<!DOCTYPE html>
<html>

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
	<style>

	</style>
</head>
<body>
	<div class="row">
		<div class="col-md-6">
	<div class="container">
		<div class="jumbotron">
			<h2>NewsFeed</h2>
			<hr>
			<div class="row">
				<div class="col-md-1">
                    <span class="glyphicon glyphicon-paperclip" style="font-size:25px;"></span>
				</div>
				<div class="col-md-10">
					<h3 style="font-size:15px;">10 new articles published on <a href="#">Freelancer Community</a></h3>
						<p>Freelancer Community is the place for you to hang out between projects, learn top tips to improve your career, share your freelancing experiences, and learn from your peers.
 Read articles and learn something new.</p>

				</div>
				<div class="col-md-1">
                    <a href="#">
          <span class="glyphicon glyphicon-remove"></span>
        </a>
				</div>
				

			</div>
			<hr>
			<!-- ********************************row1 end******************* -->
			<div class="row">
				<div class="col-md-1">
                    <span class="glyphicon glyphicon-paperclip" style="font-size:25px;"></span>
				</div>
				<div class="col-md-10">
					<h3 style="font-size:15px;">10 new articles published on <a href="#">Freelancer Community</a></h3>
						<p>Freelancer Community is the place for you to hang out between projects, learn top tips to improve your career, share your freelancing experiences, and learn from your peers.
 Read articles and learn something new.</p>

				</div>
				<div class="col-md-1">
                    <a href="#">
          <span class="glyphicon glyphicon-remove"></span>
        </a>
				</div>
				
				
			</div>
			<hr>
			<!-- *****************************row2 end****************** -->
			<div class="row">
				<div class="col-md-1">
                    <span class="glyphicon glyphicon-paperclip" style="font-size:25px;"></span>
				</div>
				<div class="col-md-10">
					<h3 style="font-size:15px;">10 new articles published on <a href="#">Freelancer Community</a></h3>
						<p>Freelancer Community is the place for you to hang out between projects, learn top tips to improve your career, share your freelancing experiences, and learn from your peers.
 Read articles and learn something new.</p>

				</div>
				<div class="col-md-1">
                    <a href="#">
          <span class="glyphicon glyphicon-remove"></span>
        </a>
				</div>
				
				
			</div>
			<hr>
			<!-- *****************************row3 end*********************** -->
			<div class="row">
				<div class="col-md-1">
                    <span class="glyphicon glyphicon-paperclip" style="font-size:25px;"></span>
				</div>
				<div class="col-md-10">
					<h3 style="font-size:15px;">10 new articles published on <a href="#">Freelancer Community</a></h3>
						<p>Freelancer Community is the place for you to hang out between projects, learn top tips to improve your career, share your freelancing experiences, and learn from your peers.
 Read articles and learn something new.</p>

				</div>
				<div class="col-md-1">
                    <a href="#">
          <span class="glyphicon glyphicon-remove"></span>
        </a>
				</div>
				
				
			</div>
			<hr>
			<!-- *********************************row4 end********************* -->
	</div>
	</div>
</div>
<div class="col-md-6"></div>
</div>
	</body>
	</html>


<?php require_once ("header.php");?>

<style>
.banner{
    background: url("images/1.jpg") no-repeat center;
    background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    -ms-background-size: cover;
    -moz-background-size: cover;
    min-height: 700px;		
}
</style>
<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
	<?php require_once ("navigation.php");?>
	<div class="ban-bot text-center">
		<script src="js/responsiveslides.min.js"></script>
				<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						 // Slideshow 4
						$("#slider3").responsiveSlides({
							auto: true,
							pager: true,
							nav: false,
							speed: 500,
							namespace: "callbacks",
							before: function () {
						$('.events').append("<li>before event fired.</li>");
						},
						after: function () {
							$('.events').append("<li>after event fired.</li>");
							}
							});
						});
				</script>
		<div  id="top" class="callbacks_container">
			<!-- <ul class="rslides" id="slider3">
				<li>
					<div class="ban-info">
						<h3>Register With Your Skills And Get Notified  For Work And Earn Extra Income</h3>
						<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur</p>
						<a class="hvr-rectangle-out" href="about.html">See More About Us</a>
					</div>		
				</li>
				<li>
					<div class="ban-info">
						<h3>Register With Your Skills And Get Notified  For Work And Earn Extra Income</h3>
						<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur</p>
						<a class="hvr-rectangle-out" href="about.html">See More About Us</a>
					</div>		
				</li>
				<li>
					<div class="ban-info">
						<h3>Register With Your Skills And Get Notified  For Work And Earn Extra Income</h3>
						<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur</p>
						<a class="hvr-rectangle-out" href="about.html">See More About Us</a>
					</div>		
				</li>			
			</ul> -->
			<div class="row">
				        <div class="col-md-3 col-sm-3"></div>
                        <div class="col-md-3 col-sm-3">
                            <div class="service-item">
                            	<!-- <img src="images/2.png"> -->
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"> </i>
                                <i class="fa fa-cloud fa-stack-1x text-primary"><img src="images/2.png"></i>
                            </span>
                                <h4>
                                    <strong style="font-style: normal;font-size:26px;">Hire Expert</strong>
                                </h4>
                               <!--  <p>Hire Expert Freelancers To Finish Your Local <br>Or Technical Any Task</p> -->
                               
                            </div>
                        </div>
                        <!-- <div class="col-md-3"></div> -->
                        <div class="col-md-3 col-sm-3">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-compass fa-stack-1x text-primary"><img src="images/3.jpg"></i>
                            </span>
                            <!-- <img src="images/3.jpg"> -->
                                <h4>
                                    <strong>Get Hired </strong>
                                </h4>
                                <!-- <p>Get Hired And Have Extra Income Source</p> -->
                                
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3"></div>
                         
                        </div>

		</div>
		<div class="clearfix"></div>
	</div>
</div>

<!-- content-bottom -->

 <section id="services" class="services bg-primary">
        <div class="container">
            
    </section>

<!-- //content-bottom -->
<!-- content -->
<div class="content">
	<div class="container">
		<h3 class="title wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s" >Finish Your  <span>Technical Things</span> Like</h3>

		
		<div class="content-grids ">
			
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal">
						<img src="images/img1.png" alt=" " />
					</figure>
					<h4 data-toggle="modal" data-target="#myModal">Graphics & Design</h4>
				</div>
			</div>
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
		
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img2.png" alt=" " />
					</figure>
					<h4>Mobile App</h4>
				</div>
			</div>
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img3.png" alt=" " />
					</figure>
					<h4>Designers</h4>
				</div>
			</div>
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr btm-clr3">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img4.png" alt=" " />
					</figure>
					<h4>Writing</h4>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="content-grids ">
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img5.png" alt=" " />
					</figure>
					<h4>Data Entry</h4>
				</div>
			</div>
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img6.png" alt=" " />
					</figure>
					<h4>Photographer</h4>
				</div>
			</div>
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img7.png" alt=" " />
					</figure>
					<h4>Musician </h4>
				</div>
			</div>
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr btm-clr3">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img8.png" alt=" " />
					</figure>
					<h4>Sales And Marketing</h4>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- *******************Content2************* -->
<div class="content">
	<div class="container">
		<h3 class="title wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s" >Finish Your  <span>Local Things</span> Like</h3>
		<!-- <p class="con-para wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
		adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore
		magnam aliquam quaerat voluptatem.</p> -->
		<div class="content-grids ">
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img9.png" alt=" " />
					</figure>
					<h4>Shifting</h4>
				</div>
			</div>
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img10.png" alt=" " />
					</figure>
					<h4>Driving</h4>
				</div>
			</div>
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img11.png" alt=" " />
					</figure>
					<h4>Cook </h4>
				</div>
			</div>
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr btm-clr3">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img12.png" alt=" " />
					</figure>
					<h4>Worker</h4>
				</div>

			</div>
			<div class="clearfix"></div>
		</div>

		<div class="content-grids ">

			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img13.png" alt=" " />
					</figure>
					<h4>Garage</h4>
				</div>
			</div>
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img14.png" alt=" " />
					</figure>
					<h4>Tailor</h4>
				</div>
			</div>
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img15.png" alt=" " />
					</figure>
					<h4>Beauty Parlour </h4>
				</div>
			</div>
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr btm-clr3">
					<!-- <span></span> -->
					<figure class="icon">
						<img src="images/img16.png" alt=" " />
					</figure>
					<h4>Electrician</h4>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //content -->


 
<!-- our pets -->
<div class="our_pets">
	<div class="container">
		<h3 class="title wow fadeInUp animated" data-wow-delay=".5s" >Our <span> Pets</span></h3>
		<div class="flex-slider wow fadeInDown animated" data-wow-delay=".5s">

			<ul id="flexiselDemo1">			
				<li>
					<div class="laptop">
						<div class="pets-effect ver_line zoom">
							<div class="img-box"><img class="img-responsive zoom-img" src="images/pic10.jpg" alt=" " /></div>
							<div class="pets-info">
								<div class="pets-info-slid">
									<h4>Pets Love</h4>
									<span class="strip_line"></span>
									<p>Sit accusamus, vel blanditiis iure minima ipsa molestias minus laborum velit, nulla nisi hic quasi enim.</p>
									<span class="strip_line"></span>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="laptop">
						<div class="pets-effect ver_line zoom">
							<div class="img-box"><img class="img-responsive zoom-img" src="images/pic11.jpg" alt=" " /></div>
							<div class="pets-info">
								<div class="pets-info-slid">
									<h4>Pets Love</h4>
									<span class="strip_line"></span>
									<p>Sit accusamus, vel blanditiis iure minima ipsa molestias minus laborum velit, nulla nisi hic quasi enim.</p>
									<span class="strip_line"></span>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="laptop">
						<div class="pets-effect ver_line zoom">
							<div class="img-box"><img class="img-responsive zoom-img" src="images/pic12.jpg" alt=" " /></div>
							<div class="pets-info">
								<div class="pets-info-slid">
									<h4>Pets Love</h4>
									<span class="strip_line"></span>
									<p>Sit accusamus, vel blanditiis iure minima ipsa molestias minus laborum velit, nulla nisi hic quasi enim.</p>
									<span class="strip_line"></span>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="laptop">
						<div class="pets-effect ver_line zoom">
							<div class="img-box"><img class="img-responsive zoom-img" src="images/pic13.jpg" alt=" " /></div>
							<div class="pets-info">
								<div class="pets-info-slid">
									<h4>Pets Love</h4>
									<span class="strip_line"></span>
									<p>Sit accusamus, vel blanditiis iure minima ipsa molestias minus laborum velit, nulla nisi hic quasi enim.</p>
									<span class="strip_line"></span>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
			<script type="text/javascript">
							$(window).load(function() {
								$("#flexiselDemo1").flexisel({
									visibleItems: 3,
									animationSpeed: 1000,
									autoPlay: true,
									autoPlaySpeed: 3000,    		
									pauseOnHover: true,
									enableResponsiveBreakpoints: true,
									responsiveBreakpoints: { 
										portrait: { 
											changePoint:480,
											visibleItems: 1
										}, 
										landscape: { 
											changePoint:640,
											visibleItems: 2
										},
										tablet: { 
											changePoint:991,
											visibleItems: 2
										}
									}
								});
								
							});
					</script>
					<script type="text/javascript" src="js/jquery.flexisel.js"></script>
		</div>
	</div>
</div>
<!-- //our pets -->
<!-- our works -->
<div class="treatments">
	<div class="container">
		<h3 class="title wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s" >Our <span> Treatments</span></h3>
		<div class="col-md-6 treat-left">
		
		
		</div>
		<div class="col-md-6 treat-right wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0s">
			<div class="treat-icon">
			<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
			<h4>Laudantium</h4>
			<p> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, quisquam est, qui dolorem ipsum quia dolor sit amet</p>
			</div>
		</div>
		<div class="col-md-6 treat-right2 text-right wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0.1s">
			<div class="treat-icon1">
			<span class="glyphicon glyphicon-leaf" aria-hidden="true"></span>
			<h4>Laudantium</h4>
			<p> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, quisquam est, qui dolorem ipsum quia dolor sit amet</p>
			</div>
		</div>
		<div class="col-md-6 treat-left2">
		
		
		</div>
		<div class="col-md-6 treat-left">
		
		
		</div>
		<div class="col-md-6 treat-right wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0.2s">
			<div class="treat-icon">
			<span class="glyphicon glyphicon-grain" aria-hidden="true"></span>
			<h4>Laudantium</h4>
			<p> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, quisquam est, qui dolorem ipsum quia dolor sit amet</p>
			</div>
		</div>
		<div class="col-md-6 treat-right2 text-right no-bor wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0.3s">
			<div class="treat-icon1">
			<span class="glyphicon glyphicon-scissors" aria-hidden="true"></span>
			<h4>Laudantium</h4>
			<p> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, quisquam est, qui dolorem ipsum quia dolor sit amet</p>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<!-- //our works -->



<!-- contact -->
<div class="contact-form ">
		<div class="container">
			<h3 class="title">Contact <span>Us</span></h3>
			
			<div class="col-md-6 contact-right wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">				
				<form action="#" method="post">
					<input type="text" name="Name" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required="">
					<input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
					<input type="text" name="Telephone" value="Telephone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Telephone';}" required="">
					<textarea name="Message..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message...';}" required="">Message...</textarea>
					<input type="submit" value="Submit" >
				</form>
			</div>
			<div class="col-md-6 contact-left wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
				<h2>Contact Information</h2>
				<p>"Lorem Ipsum"is the common name dummy text often used in the design, printing, and type setting industriescommon name dummy text often used in the design, printing, and type setting industries Lorem Ipsum"is the common name dummy text often used in the design, printing, and type setting industries "</p>
				<ul class="contact-list">
					<li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>756 global Place, New York.</li>
					<li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:example@mail.com">mail@example.com</a></li>
					<li><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>+123 2222 222</li>
				</ul>
				<ul class="icons-list footer-bottom">
					<li><a href="#" class="use1"><span>Facebook</span></a></li>
					<li><a href="#" class="use2"><span>Twitter</span></a></li>
					<li><a href="#" class="use3"><span>Dribbble</span></a></li>
					<li><a href="#" class="use4"><span>Pinterest</span></a></li>
				</ul>
			</div>						
			<div class="clearfix"> </div>
			<p class="copy-right text-center">&copy; 2016 Pets Love. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
		</div>			
</div>

<!-- contact -->
	<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->

</body>
</html>


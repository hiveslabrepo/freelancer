<?php require_once ("header.php");?>
<head>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<style>
body {
  background: #F1F3FA;
}

/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}
 .rating span {
  font-size: 25px;
  cursor: pointer;
  float: right; //remove 'float right' for right to left
}
.rating span:hover, .rating span:hover ~ span{
  color: red; //gold or any other color 
}
td:hover {
          /*background-color: #ffff99;*/
          -webkit-box-shadow: 0px 2px 4px #e06547;
    -moz-box-shadow: 0px 2px 4px #e06547;
    box-shadow: 0px 2px 4px #e06547;
    background-color: #d4d5d6;
        }

        .dropdown {
    display:inline-block;
    margin-left:20px;
    padding:10px;
  }


.glyphicon-bell {
   
    font-size:1.5rem;
  }

.notifications {
   min-width:420px; 
  }
  
  .notifications-wrapper {
     overflow:auto;
      max-height:250px;
    }
    
 .menu-title {
     color:#ff7788;
     font-size:1.5rem;
      display:inline-block;
      }
 
.glyphicon-circle-arrow-right {
      margin-left:10px;     
   }
  
   
 .notification-heading, .notification-footer  {
  padding:2px 10px;
       }
      
        
.dropdown-menu.divider {
  margin:5px 0;          
  }

.item-title {
  
 font-size:1.3rem;
 color:#000;
    
}

.notifications a.content {
 text-decoration:none;
 background:#ccc;

 }
    
.notification-item {
 padding:10px;
 margin:5px;
 background:#ccc;
 border-radius:4px;
 }
</style>
 <script>
  $('.rating span').click(function(){
    alert($(this).attr('id'));
})

</script>  


</head>
<body>
<?php require_once ("navigation1.php");?>

<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="images/blank_img.jpg" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						Priyanshe Yadav
					</div>
					<div class="profile-usertitle-job">
						For Work
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<a href="#"><button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a>
					<!-- <button type="button" class="btn btn-danger btn-sm">Message</button> -->
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">


						
						<!-- <li class="active">
							<a href="index.php">
							<i class="glyphicon glyphicon-home"></i>
							Logout </a>
						</li> -->
						<li class="active">
							<a href="workprofile.php">
          <i class="glyphicon glyphicon-user"></i>
          Profile
        </a></li>

        <li>
							<a href="workprofile.php">
      <i class="glyphicon glyphicon-user"></i>
          Membership
        </a></li>
         
						<li>
							<a href="#">
          <i class="glyphicon glyphicon-cog"></i>
        
							 Settings </a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-user"></i>
							Invite friends </a>
						</li>
						<li>
							<a href="index.php">
							<i class="glyphicon glyphicon-home"></i>
							Logout </a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">

              <div class="container">
<!-- <div class="jumbotron" style="width:60%;"> -->
  <div class="row" style="margin-top:-10px;">
    <div class="col-md-6">
  <img src="images/hives.png"style="border:1px solid black;"> <strong style="font-size:20px;"> Hives Lab</strong>
</div>
<div class="col-md-6"></div>
</div>
    <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Job Title</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
     <!-- *******************row 1 end****************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Category</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>Software engineering & Technology</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
     <!-- **************************row 2 end*************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Skills</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>HTML5 </h4>
        </div>
         <!--  <div class="col-md-5"></div> -->
     </div>
     <!-- ***********************row 3 end********************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Payment</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>XYZ</h4>
        </div>
         <!--  <div class="col-md-5"></div> -->
     </div>
    <!--  *****************************row 4 end******************** -->
     <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Address</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
    <!--  ************************* row 5 end************************* -->
     <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Discription</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
    <!--  ***************************row 6 end***************** -->

  
   
 </div>
  <!-- ******************end container***************** -->
  <br><br>
   <form class="form-horizontal" style="margin-left:20px;">
    <div class="form-group">
      <label for="comment">Discription:</label>
  <textarea class="form-control" rows="5" id="comment"></textarea>

    </div>
<div class="form-group">
      <label for="sel1">Duration:</label>
      <select class="form-control" id="sel1">
        <option value="default"> Select Duration</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
    </div>
    <div class="form-group">
      <label for="sel1">Price:</label>
      <select class="form-control" id="sel1">
        <option value="default"> Select Price</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
    </div>

    <div class="form-group">
       <div class=" col-lg-7"></div>
      <div class=" col-lg-5">
     
   <a href="workprofile.php"> <button type="button" class="btn btn-info">Submit</button></a>
  </div>
</div>

  </form>
	</div>
<!-- **************************** end profile-content*********************** -->
  </div>
   <!--  ***********************end col-md-9 ****************** -->
  </div>
 <!--  ************************end row profile**************** -->
</div>
<!-- ****************************end container*************** -->





		
     </body>
     </html>
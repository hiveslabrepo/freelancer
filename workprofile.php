<?php require_once ("header.php");?>
<head>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<style>
body {
  background: #F1F3FA;
}

/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}
 .rating span {
  font-size: 25px;
  cursor: pointer;
  float: right; //remove 'float right' for right to left
}
.rating span:hover, .rating span:hover ~ span{
  color: red; //gold or any other color 
}
td:hover {
          /*background-color: #ffff99;*/
          -webkit-box-shadow: 0px 2px 4px #e06547;
    -moz-box-shadow: 0px 2px 4px #e06547;
    box-shadow: 0px 2px 4px #e06547;
    background-color: #d4d5d6;
        }

        .dropdown {
    display:inline-block;
    margin-left:20px;
    padding:10px;
  }


.glyphicon-bell {
   
    font-size:1.5rem;
  }

.notifications {
   min-width:420px; 
  }
  
  .notifications-wrapper {
     overflow:auto;
      max-height:250px;
    }
    
 .menu-title {
     color:#ff7788;
     font-size:1.5rem;
      display:inline-block;
      }
 
.glyphicon-circle-arrow-right {
      margin-left:10px;     
   }
  
   
 .notification-heading, .notification-footer  {
  padding:2px 10px;
       }
      
        
.dropdown-menu.divider {
  margin:5px 0;          
  }

.item-title {
  
 font-size:1.3rem;
 color:#000;
    
}

.notifications a.content {
 text-decoration:none;
 background:#ccc;

 }
    
.notification-item {
 padding:10px;
 margin:5px;
 background:#ccc;
 border-radius:4px;
 }
</style>
 <script>
  $('.rating span').click(function(){
    alert($(this).attr('id'));
})

</script>  


</head>
<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
    <div class="container">
        <div class="header-left grid">
            <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
                <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish" style="font-size: 23px; line-height: 1.1;" ><h4 style="font-size: 23px;">Work To Finish</h4></span></a></h1>
            </div>
        </div>
      
    
     <div class="header-middle">
     
</div>
<!-- ************************header middle***************** -->
</div>
<!-- ******************container********** -->
</div>
<!-- ***********************header wow fadeInDown animated*************** -->
</div>
 <!--  ***************************dropdown end************** -->

		
	
<!--   <div class="row" style="margin-top:-1px;">
      <div class="col-md-12">
    
      <nav class="navbar navbar-default"style="background-color:#4997D0 ;width:100%;">
  <div class="container-fluid">
   
    <ul class="nav navbar-nav">
   <li class="dropdown notificatiuons-menu" style="margin-left: 1200%;">
   <a href="#" class="driopdown-toggle" data-toggle="dropdown">
   <i class="fa fa-bell-o" style="font-size:20px;font-size: inherit;"> </i>
   <span class="label label-warning" style="margin-top:-20px;">10</span> 
   </a>
   <ul class="dropdown-menu"> -->
   <!--  <li class="header">u have 10 notifications </li> -->
    <!-- <li>
      <ul class="menu">
      <li><a href="#">
        <i class="fa fa-users text-aqua"></i>gh vhhjvjh
      </a>
    </li>
      </ul>

    </li>

   </ul>
 </li>
      

    </ul>
  </div>
</nav>
</div>
</div> -->
<div class="well well-sm" style="background-color:#4997D0 ;">
<div class="dropdown" style="margin-left:60%;">
  <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
    <i class="glyphicon glyphicon-bell" style="color: white;"></i>
  </a>
  
  <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel">
    
    <div class="notification-heading"><h4 class="menu-title">Notifications</h4><h4 class="menu-title pull-right">View all<i class="glyphicon glyphicon-circle-arrow-right"></i></h4>
    </div>
    <li class="divider"></li>
   <div class="notifications-wrapper">
     <a class="content" href="#">
      
       <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 · day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
       
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 · day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>

    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>

   </div>
    <li class="divider"></li>
    <div class="notification-footer"><h4 class="menu-title">View all<i class="glyphicon glyphicon-circle-arrow-right"></i></h4></div>
  </ul>
  
</div>
<!-- **********************bell 1 end***************** -->
<div class="dropdown" style="margin-left:62%;margin-bottom:-30px;">
  <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
    <i class="glyphicon glyphicon-bell" style="color: white;"></i>
  </a>
  
  <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel">
    
    <div class="notification-heading"><h4 class="menu-title">Notifications</h4><h4 class="menu-title pull-right">View all<i class="glyphicon glyphicon-circle-arrow-right"></i></h4>
    </div>
    <li class="divider"></li>
   <div class="notifications-wrapper">
     <a class="content" href="#">
      
       <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 · day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
       
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 · day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>

    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>

   </div>
    <li class="divider"></li>
    <div class="notification-footer"><h4 class="menu-title">View all<i class="glyphicon glyphicon-circle-arrow-right"></i></h4></div>
  </ul>
  
</div>
</div>
<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="images/blank_img.jpg" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						Priyanshe Yadav
					</div>
					<!-- <div class="profile-usertitle-job">
						For Work
					</div> -->
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<a href="#"><button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a>
					<!-- <button type="button" class="btn btn-danger btn-sm">Message</button> -->
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">


						
						<!-- <li class="active">
							<a href="index.php">
							<i class="glyphicon glyphicon-home"></i>
							Logout </a>
						</li> -->
						<li class="active">
							<a href="workprofile.php">
          <i class="glyphicon glyphicon-user"></i>
          Profile
        </a></li>

        <li>
							<a href="workprofile.php">
      <i class="glyphicon glyphicon-user"></i>
          Membership
        </a></li>
         
						<li>
							<a href="#">
          <i class="glyphicon glyphicon-cog"></i>
        
							 Settings </a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-user"></i>
							Invite friends </a>
						</li>
						<li>
							<a href="index.php">
							<i class="glyphicon glyphicon-home"></i>
							Logout </a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div><br><br>
      <!-- ***********************1 more side bar**************** -->
      <div class="profile-sidebar">
       
        <h4 style="text-align:center;">Previouslly Applied</h4>
       
        <div class="profile-usermenu">
          <ul class="nav">
          <li>
              <a href="workprofile.php">
         
          Job1
        </a></li>

        <li>
              <a href="workprofile.php">
     
          Job2
        </a></li>
         
            <li>
              <a href="#">
          
        
               Job3 </a>
            </li>
            <li>
              <a href="#" target="_blank">
             
              job4 </a>
            </li>
            <li>
              <a href="index.php">
            
              job5 </a>
            </li>
          </ul>
        </div>
        <!-- END MENU -->
      </div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
			
			           
              	 <div class="container"style="width:130%;">
      
            <input type="search" id="container-search" value="" class="form-control" placeholder="Search..." style="width:550px;margin-top: 20px;border:1px solid gray;">
       <!--  ******************search box***************** --> 
            </div><br>
        <h3>Education</h3><br>
        <h5 style="font-size:18px;">BE</h5>
        <!-- <h6 style="font-size:17px;">BE</h6> -->
        <p><strong style="font-size:15px;">Takshshila Institute of  Engineering & Technology, India</strong> 2012 - 2015</p>
         <button type="button" class="btn btn-success" style="float:right;"data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus"></span> Add Education</button>
             <!-- *********************modal 1 body start************** -->
             <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel"> <i class="glyphicon glyphicon-education"></i> Add Education</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal">
  <div class="form-group">
    <label class="control-label col-sm-3" for="title">Title:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="title" placeholder="Enter Title">
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-3" for="company name">Company name:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="company name" placeholder="Enter Company Name">
    </div>
  </div>
 <!--  <div class="form-group">
<label class="control-label col-sm-3">Book Date:</label>
<input type="date" name="bday" style="margin-left:100px;">
  <br/>
<br/>
<input type="date" name="bday" style="margin-left:10px;">
  <br/>
<br/>
</div> -->
 <div class="form-group">
      <label for="sel1" class="control-label col-sm-3">Start Year:</label>
       <div class="col-sm-3">
      <select class="form-control" id="sel1">
       
        <option>2016</option>
        <option>2015</option>
        <option>2014</option>
        <option>2013</option>
      </select>
    </div>
    <div class="form-group">
  
      <label for="sel1" class="control-label col-sm-3">End Year:</label>
       <div class="col-sm-3">
      <select class="form-control" id="sel2">
       
        <option>2016</option>
        <option>2015</option>
        <option>2014</option>
        <option>2013</option>
      </select>
    </div>
  </div> 
  </div> 

<div class="form-group">
  <label class="control-label col-sm-3" for="summary">Summary:</label>
   <div class="col-sm-9">
  <textarea class="form-control" rows="5" id="summary"></textarea>
</div>
</div>


</form>
      </div>
      <div class="modal-footer">
        <a href="workprofile.php"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button></a>
        <a href="workprofile.php"><button type="button" class="btn btn-primary" style="text-decoration:none;">Save </button></a>
      </div>
    </div>
  </div>
</div>
         <br>
        <hr>
        <h3>Certifications</h3><br>
        <p><strong style="font-size:15px;">Diploma in Java Technology</strong> from Profound Edutech.</p><br>
         <button type="button" class="btn btn-success" style="float:right;"data-toggle="modal" data-target="#myModal2"> <span class="glyphicon glyphicon-plus"></span> Add Certifications</button>
                <!-- **********************modal 3 body Start*************** -->
                <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel"> <i class="glyphicon glyphicon-certificate"></i> Add Certifications</h4>
      </div>
      <div class="modal-body">
             <form class="form-horizontal">
  <div class="form-group">
    <label class="control-label col-sm-3" for="title">Title:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="title" placeholder="Enter Title">
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-3" for="company name">Company name:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="company name" placeholder="Enter Company Name">
    </div>
  </div>
 <!--  <div class="form-group">
<label class="control-label col-sm-3">Book Date:</label>
<input type="date" name="bday" style="margin-left:100px;">
  <br/>
<br/>
<input type="date" name="bday" style="margin-left:10px;">
  <br/>
<br/>
</div> -->
 <div class="form-group">
      <label for="sel1" class="control-label col-sm-3">Start Year:</label>
       <div class="col-sm-3">
      <select class="form-control" id="sel1">
       
        <option>2016</option>
        <option>2015</option>
        <option>2014</option>
        <option>2013</option>
      </select>
    </div>
    <div class="form-group">
  
      <label for="sel1" class="control-label col-sm-3">End Year:</label>
       <div class="col-sm-3">
      <select class="form-control" id="sel2">
       
        <option>2016</option>
        <option>2015</option>
        <option>2014</option>
        <option>2013</option>
      </select>
    </div>
  </div> 
  </div> 

<div class="form-group">
  <label class="control-label col-sm-3" for="summary">Summary:</label>
   <div class="col-sm-9">
  <textarea class="form-control" rows="5" id="summary"></textarea>
</div>
</div>


</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
         <br>
        <hr>
         <h3>Experience</h3><br>
          <button type="button" class="btn btn-success" style="float:right;" data-toggle="modal" data-target="#myModal3"> <span class="glyphicon glyphicon-plus"></span> Add Experience</button>
           <!-- **********************modal 3 body Start*************** -->
           <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel"> <i class="glyphicon glyphicon-briefcase"></i> Add Experience</h4>
      </div>
      <div class="modal-body">
            <form class="form-horizontal">
  <div class="form-group">
    <label class="control-label col-sm-3" for="title">Title:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="title" placeholder="Enter Title">
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-3" for="company name">Company name:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="company name" placeholder="Enter Company Name">
    </div>
  </div>
 <!--  <div class="form-group">
<label class="control-label col-sm-3">Book Date:</label>
<input type="date" name="bday" style="margin-left:100px;">
  <br/>
<br/>
<input type="date" name="bday" style="margin-left:10px;">
  <br/>
<br/>
</div> -->
 <div class="form-group">
      <label for="sel1" class="control-label col-sm-3">Start Year:</label>
       <div class="col-sm-3">
      <select class="form-control" id="sel1">
       
        <option>2016</option>
        <option>2015</option>
        <option>2014</option>
        <option>2013</option>
      </select>
    </div>
    <div class="form-group">
  
      <label for="sel1" class="control-label col-sm-3">End Year:</label>
       <div class="col-sm-3">
      <select class="form-control" id="sel2">
       
        <option>2016</option>
        <option>2015</option>
        <option>2014</option>
        <option>2013</option>
      </select>
    </div>
  </div> 
  </div> 

<div class="form-group">
  <label class="control-label col-sm-3" for="summary">Summary:</label>
   <div class="col-sm-9">
  <textarea class="form-control" rows="5" id="summary"></textarea>
</div>
</div>


</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
          <br>
      

      <hr>
                    <h3>Skills</h3><br>
                     <div class="container">
 
  <table class="table table-bordered" style="width:65%;">
   <!--  <thead>
      <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
         <th>Email</th>
          <th>Email</th>
      </tr>
    </thead> -->
    <tbody>
      <tr >
        <td style="font-size:15px;color:black;font-weight:bold;">HTML5</td>
        <td style="font-size:15px;color:black;font-weight:bold;">CSS3</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Bootstrap</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Java Script</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Jquery</td>
      </tr>
      <tr>
        <td style="font-size:15px;color:black;font-weight:bold;">WordPress</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Photoshop</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Software Architecture</td>
        <td style="font-size:15px;color:black;font-weight:bold;">eCommerce</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Articles</td>
      </tr>

     
    </tbody>


  </table>

  <button type="button" class="btn btn-success" style="float:center;" data-toggle="modal" data-target="#myModal4" > <span class="glyphicon glyphicon-plus"></span> Add Skills</button>
                   <!--  *****************************modal4 body start******** -->
                   <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel"><i class="glyphicon glyphicon-book"></i> Add Skills</h4>
      </div>
      <div class="modal-body">
             <form class="form-horizontal">
  <div class="form-group">
    <label class="control-label col-sm-3" for="title">Title:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="title" placeholder="Enter Title">
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-3" for="company name">Company name:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="company name" placeholder="Enter Company Name">
    </div>
  </div>
 <!--  <div class="form-group">
<label class="control-label col-sm-3">Book Date:</label>
<input type="date" name="bday" style="margin-left:100px;">
  <br/>
<br/>
<input type="date" name="bday" style="margin-left:10px;">
  <br/>
<br/>
</div> -->
 <div class="form-group">
      <label for="sel1" class="control-label col-sm-3">Start Year:</label>
       <div class="col-sm-3">
      <select class="form-control" id="sel1">
       
        <option>2016</option>
        <option>2015</option>
        <option>2014</option>
        <option>2013</option>
      </select>
    </div>
    <div class="form-group">
  
      <label for="sel1" class="control-label col-sm-3">End Year:</label>
       <div class="col-sm-3">
      <select class="form-control" id="sel2">
       
        <option>2016</option>
        <option>2015</option>
        <option>2014</option>
        <option>2013</option>
      </select>
    </div>
  </div> 
  </div> 

<div class="form-group">
  <label class="control-label col-sm-3" for="summary">Summary:</label>
   <div class="col-sm-9">
  <textarea class="form-control" rows="5" id="summary"></textarea>
</div>
</div>


</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


  <br><br>

   
<div class="well well-md" style="width:60%;">
  <h3>Related Job To Your Profile</h3>
</div>
<div class="container">
<div class="col-md-3">
  <a href="demo15.php"><img src="images/hives.png" style="border:1px solid black;"></a>

  </div>
  <div class="col-sm-6">
   <a href="demo15.php"> <h3 style="margin-left:-41%; color:black;">Hives lab</h3></a>
   <P style="margin-left:-41%;"> HTML5,CSS3,JAVA SCRIPT,BOOTSTRAP</P>
   <P style="margin-left:-41%;"><strong style="font-size:15px;"> discription - </strong> We build light weight web pages which increase speed and reduce waiting time .</P>
</div>

</div>
<hr>
<div class="container">
<div class="col-md-3">
  <a href="demo15.php"><img src="images/hives.png" style="border:1px solid black;"></a>

  </div>
  <div class="col-sm-6" style="width:50%;">
    <a href="demo15.php"><h3 style="margin-left:-41%;color:black;">Hives lab</h3></a>
   <P style="margin-left:-41%;"> HTML5,CSS3,JAVA SCRIPT,BOOTSTRAP</P>
   <P style="margin-left:-41%;"><strong style="font-size:15px;"> discription - </strong> We build light weight web pages which increase speed and <br>reduce waiting time .</P>
</div>

</div>
<hr>




    
  </div>
</div>
</div>
    </div>
   <!--  *********************jumbotron container************* -->

</div>



   


               
                 
			 </div>
            </div>
		</div>
	</div>

<center>

</center>
<br>
<br>


		
     </body>
     </html>
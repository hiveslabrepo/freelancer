<?php require_once ("header.php");?>


<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
  <div class="container">
    <div class="header-left grid">
      <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
        <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
      </div>
    </div>
    <div class="header-middle">
     
      <div class="search">
        <form action="#" method="post">
          <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
          <input type="submit" value=" ">
        </form>
      </div>
    </div>
    <div class="header-right">
      </div>
  </div>
</div>

 
 <div class="container">
<div class="jumbotron" style="width:50%;margin-left:20%;border:1px solid black;">
  <div class="row" style="margin-top:-20px;">
    <div class="col-md-6">
  <img src="images/hives.png" style="border:1px solid black;"> <strong style="font-size:20px;"> Hives Lab</strong>
</div>
<div class="col-md-6"></div>
</div>
    <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Job Title</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
     <!-- *******************row 1 end****************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Category</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>Software engineering & Technology</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
     <!-- **************************row 2 end*************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Skills</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>HTML5 </h4>
        </div>
         <!--  <div class="col-md-5"></div> -->
     </div>
     <!-- ***********************row 3 end********************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Payment</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>XYZ</h4>
        </div>
         <!--  <div class="col-md-5"></div> -->
     </div>
    <!--  *****************************row 4 end******************** -->
     <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Address</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
    <!--  ************************* row 5 end************************* -->
     <div class="row" style="margin-top:20px;">
     
      <div class="col-md-3">
     
        <h4>Discription</h4>
      </div> 
        <div class="col-md-9">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
    <!--  ***************************row 6 end***************** -->
     
    </div>
   <!--  ************************end jumbotron*************** -->
  </div>
  <!-- *************************end container********************* -->

   <form class="form-horizontal">
    
    <div class="form-group" style="margin-left: 130px;">
      
  <label  class="control-label col-sm-3" for="discription"> Discription:</label>

  <div class="col-sm-9">
  <textarea class="form-control" rows="5" id="discription" style="width:50%;"></textarea>
</div>
</div>
    
     <div class="form-group" style="margin-left: 130px;">
      <label class="control-label col-sm-3" for="sel1">Duration:</label>
       <div class="col-sm-9">
      <select class="form-control" id="sel1" style="width:50%;">
        <option value="default">Select Duration</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
    </div>
  </div>

   <div class="form-group" style="margin-left: 130px;">
      <label class="control-label col-sm-3" for="sel2">Price:</label>
       <div class="col-sm-9">
      <select class="form-control" id="sel2" style="width:50%;">
        <option value="default">Select Price</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
    </div>
  </div>
<div class="form-group">
       <div class=" col-lg-7"></div>
      <div class=" col-lg-5">
     
   <a href="workprofile.php"> <button type="button" class="btn btn-info">Submit</button></a>
  </div>
</div>
  </form>

  
 
   

   
         
   

  </body>
  </html>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  .row-padding {
    margin-top: 25px;
    margin-bottom: 25px;
}

  </style>
  <script>
  $(function () {
    $( '#table' ).searchable({
        striped: true,
        oddRow: { 'background-color': '#f5f5f5' },
        evenRow: { 'background-color': '#fff' },
        searchType: 'fuzzy'
    });
    
    $( '#searchable-container' ).searchable({
        searchField: '#container-search',
        selector: '.row',
        childSelector: '.col-xs-4',
        show: function( elem ) {
            elem.slideDown(100);
        },
        hide: function( elem ) {
            elem.slideUp( 100 );
        }
    })
});

  </script>
</head>
<body>

<div class="container">
 
  <form>
    <label class="radio-inline" data-toggle="modal" data-target="#myModal">
      <input type="radio" name="optradio">Option 1
    </label>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      
  <div class="modal-dialog" role="document" >
     <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Select your skills and expertise</h4>
      </div>
      <div class="modal-body">
        <!-- *********body***** -->
        <div class="container">
    <div class="row">
        <div class="col-lg-12">
            <!-- <div class="page-header">
                
            </div> -->
            <!-- <p>You can find the source of this plugin at <a href="http://github.com/stidges/jquery-searchable" target="_blank">Github</a> (http://github.com/stidges/jquery-searchable)!</p> -->
        </div>
    </div>
    <!-- <div class="row">
        <div class="col-lg-12">
            <h3>Table / Fuzzy search example</h3>
        </div>
    </div> -->
    <div class="row">
        <!-- <div class="col-lg-4 col-lg-offset-4">
            <input type="search" id="search" value="" class="form-control" placeholder="Search using Fuzzy searching" style="margin-left:-350px;">
        </div> -->
    </div>
    
    <div class="row">
        <!-- <div class="col-lg-4">
            <h3>Non-table example</h3>
        </div> -->
    </div>
    <div class="row">
        <div class="col-lg-12 col-lg-offset-12;">
            <input type="search" id="container-search" value="" class="form-control" placeholder="Search..." style="margin-left:-10px;width:550px;">
        </div>
    </div>
</div>


<div class="row" style="margin-top:30px;margin-left:10px;">
  <div class="col-lg-2" style="border:1px solid lightgray;width:auto;height:auto;">
    <a><center><img src="images/k1.png" width="50px"></center>
    <h4>Website Development
    </h4></a>
  </div>

   <div class="col-lg-2" style="border:1px solid lightgray;width:auto;height:auto;">
    <a><center><img src="images/k1.png" width="50px"></center>
    <h4>Website Development
    </h4></a>
  </div>

  <div class="col-lg-2" style="border:1px solid lightgray;width:auto;height:auto;">
    <a><center><img src="images/k1.png" width="50px"></center>
    <h4>Website Development
    </h4></a>
  </div>

   <!-- <div class="col-lg-2" style="border:1px solid lightgray;width:auto;height:auto;">
    <a><center><img src="images/k1.png" width="50px"></center>
    <h4>Website Development
    </h4></a>
  </div> -->
 <!--  <div class="col-lg-6" style="width:auto;height:auto;margin-left:20px;">
   
  
  <ul class="list-group">
    <li class="list-group-item">First item First item</li>
   <li class="list-group-item">Second item Second item</li> 
     <li class="list-group-item">Third item Third item</li> 
  </ul>

  </div> -->

  </div>

  <div class="row" style="margin-top:20px;margin-left:10px;">
  <div class="col-lg-2" style="border:1px solid lightgray;width:auto;height:auto;">

    <a href="#"><center><img src="images/k1.png" width="50px"></center>
    <h4>Website Development
    </h4></a>

  </div>

   <div class="col-lg-2" style="border:1px solid lightgray;width:auto;height:auto;">
    <a><center><img src="images/k1.png" width="50px"></center>
    <h4>Website Development
    </h4></a>
  </div>

  <div class="col-lg-2" style="border:1px solid lightgray;width:auto;height:auto;">
    <a><center><img src="images/k1.png" width="50px"></center>
    <h4>Website Development
    </h4></a>
  </div>

   <!-- <div class="col-lg-2" style="border:1px solid lightgray;width:auto;height:auto;">
    <a><center><img src="images/k1.png" width="50px"></center>
    <h4>Website Development
    </h4></a>
  </div> -->
 
  </div>
  </div>

 
  


<!-- ***********body end***** -->
     
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</div>
   
  </form>
</div>

</body>
</html>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="modal fade login-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<section class="login_section">
    <span class="crcal_arro"><a href="<?php site_url();?>"><img src="<?php echo IMAGE_URL;?>aax.png"/></a></span>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h1><b>Log in</b> to your  account </h1>
        </div>
    </div>
    <div class="hidn">
        <div class="logn_info">
            <div class="row">
                <form method="post" action="<?php echo site_url('login');?>" id="login_form">
                    <div class="col-md-6 col-sm-12">
                        <div class="btn_lft">
                            <button type="button" class="fb"><i class="fa fa-facebook"></i> Log in with Facebook </button>
                            <button type="button" class="fb gogl"><i class="fa fa-google-plus"></i> Sign in wth Google </button>
                            <div class="checkbox checkbox-info checkbox-circle hide-login">
                                <input id="checkbox8" class="styled" type="checkbox" name="confirm"  <?php if(!empty($uname)) echo 'checked="checked"';?> value="confirm">
                                <label for="checkbox8">
                                Keep me logged in
                                </label> 
                                <span><a href="javascript:void(0)" class="forgot">Forgot your password?</a></span>
                            </div>
                        </div>
                    </div>
                    <span class="hr_line"></span>
                    <div class="col-md-6 col-sm-12">
                        <div class="btn_lft rht">

                            <input class="style" type ="text" name ="username" autocomplete ="off" placeholder="Email address" value="<?php if(isset($uname) && !empty($uname)){echo $uname;}else {echo set_value('username');}?>">

                            <?php echo form_error('username');?>

                            <input class="style" type ="password" name ="password" autocomplete ="off" placeholder="Password" value="<?php if(isset($password) && !empty($password)){echo $password;}else {echo set_value('password');}?>">

                            <?php echo form_error('password');?>

                            <p id="login_error" class="message-alert"></p>
                            <!-- <button type="button" id="login" name = "login" class="log hide-login">Login</button> -->
                            <input type="submit" id="login" name ="login"  class="log hide-login" value="Login"/>
                    </div>
                    </div>
                </form> 
            </div>
        </div> 
    </div>
    <article class="logn_footer">
        <span>Looking to <a data-toggle="modal"  data-name="login" onclick="loginSignupToggle(this);">create an account</a>?</span>
    </article>
</section>

<div class="modal fade sign-up-modal-lg" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">          
<section class="login_section">
    <span class="crcal_arro"><a href="<?php base_url();?>"><img src="<?php echo IMAGE_URL;?>aax.png"/></a></span>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h1><b>Sign up</b>  for </h1>
        </div>
    </div>
    <div class="logn_info">
        <div class="row">
            <form action="<?php echo site_url('signup');?>" method="post" id="signup_form">
                <div class="col-md-6 col-sm-12">
                    <div class="btn_lft">
                        <button type="button" class="fb"><i class="fa fa-facebook"></i> Log in with Facebook </button>
                        <button type="button" class="fb gogl"><i class="fa fa-google-plus"></i> Sign in wth Google </button>

                        <div class="checkbox checkbox-info checkbox-circle">
                            <span class="by">By creating an account you agree
                            to our <a href="javascript:void(0);">Terms Of Use</a> and <a href="javascript:void(0);">Privacy Policy</a>
                            </span>
                        </div>
                    </div>
                </div>
                <span class="hr_line"></span>
                <div class="col-md-6 col-sm-12">
                    <div class="btn_lft rht">
                        <input type ="text" id="user_email" name ="user_email" autocomplete ="off" class="style" placeholder="Email address" >
                        <?php echo form_error('user_email');?>
                        <input type ="password" name ="password" autocomplete ="off" class="style" placeholder="Password" >
                        <?php echo form_error('password');?>
                        <input type="submit" id="create_account" name ="create_account" class="log" value="create account">
                    </div>
                </div>
            </form>
        </div>
    </div> 
    <article class="logn_footer">
        <span>Did you mean to <a data-toggle="modal" data-name="signup" onclick="loginSignupToggle(this);">Login</a>?</span>
    </article>
</section> 

<script>
function() {
    $('#myModal').modal('show');
    $('#myModal1').modal('hide');
}
$(document).ready(function(){
    $("#myModal").modal('hide');
    $("#myModal2").modal('hide');

    $("#click1").click(function(){
    $("#myModal1").modal('show');
    });
    $("#click2").click(function(){
    $("#myModal2").modal('show');
    });

});
$(document).ready(function(){
    $("#myModal").modal('hide');
    $("#myModal2").modal('hide');
    $("#click1","#click2").click(function(){
        if($(event.target).is('#click1')){
             $("#myModal1").modal('show');
          }
        else if($(event.target).is('#click2')){
             $("#myModal2").modal('show');
          }
        });
    });
</script>
    
</body>
</html>
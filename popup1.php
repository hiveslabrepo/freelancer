<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
	.popup {
		display: none;
		background-color: #FFFFFF;
		position: fixed;
		top: 150px;
		padding: 40px 25px 25px 25px;
		width: 350px;
		z-index: 999;
		left: 50%;
		margin-left: -200px;
	}
	
	.pop_overlay{
		height: 100%;
		width: 100%;
		background-color: #F6F6F6;
		opacity: 0.9;
		position: fixed;
		z-index: 998;
	}
	
	a.close{
		color: #999;
		text-decoration: none;
		position: absolute;
		right: 15px;
		top: 15px;
	}
</style>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		// Show the popup on click
		$('#show_pop1, #show_pop2').on('click', function (e) {
			$('body').prepend('<div class="pop_overlay"></div>');
			if ($(this).attr('id') == 'show_pop1') 
				$('#popup1').fadeIn(500);
			else 
				$('#popup2').fadeIn(500);
			e.preventDefault();
		});		
		
		// Open popup from link inside another popup
		$('#pop1, #pop2').on('click', function (e) {
			toFadeOut = $('#popup1');
			toFadeIn = $('#popup2');
			if ($(this).attr('id') == 'pop1') {
				toFadeOut = $('#popup2');
				toFadeIn = $('#popup1');
			}
			toFadeOut.fadeOut(500, function () {
				toFadeIn.fadeIn();
			})
			return false;
		});
		
		// Close popup
		$(document).on('click', '.pop_overlay, .close', function () {
			$('#popup1, #popup2').fadeOut(500, function () {
				$('.pop_overlay').remove();
			});
			return false;
		});		
	});
</script>

</head>
<body>
	 <form>
	<a id="show_pop1" href=""> <label class="radio-inline">
      <input type="radio" name="optradio">Technical
    </label></a> | <a id="show_pop2" href=""><label class="radio-inline">
      <input type="radio" name="optradio">Non-Technical
    </label>
    </a>
<div id="popup1" class="popup" style="width:auto;height:auto;">
    <h3><a id="pop2" href="">Show popup two</a></h3>
    <hr />
    <h1 align="center">POPUP 1</h1>	
    <p align="justify">This is first popup. Place your content here.</p>
    <a class="close" href="">[ x ]</a>
</div>
<div id="popup2" class="popup">
    <h3><a id="pop1" href="">Show popup one</a></h3>
    <hr />
    <h1 align="center">POPUP 2</h1>	
    <p align="justify">This is second popup. Place your content here.</p>
    <a class="close" href="">[ x ]</a>    
</div>

	</body>
	</html>



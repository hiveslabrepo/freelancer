<?php require_once ("header.php");?>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

<style>
@import url(http://fonts.googleapis.com/css?family=Lato:400,700);
body
{
    font-family: 'Lato', 'sans-serif';

  background: #F1F3FA;

    }
.profile 
{
    min-height: auto;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }
.dropdown-menu 
{
    background-color: #34495e;    
    box-shadow: none;
    -webkit-box-shadow: none;
    width: 250px;
    margin-left: -125px;
    left: 50%;
    }
.dropdown-menu .divider 
{
    background:none;    
    }
.dropdown-menu>li>a
{
    color:#f5f5f5;
    }
.dropup .dropdown-menu 
{
    margin-bottom:10px;
    }
.dropup .dropdown-menu:before 
{
    content: "";
    border-top: 10px solid #34495e;
    border-right: 10px solid transparent;
    border-left: 10px solid transparent;
    position: absolute;
    bottom: -10px;
    left: 50%;
    margin-left: -10px;
    z-index: 10;
    }<style> 
#panel, #flip {
    padding: 5px;
    text-align: center;
    background-color: #e5eecc;
    border: solid 1px #c3c3c3;
}

#panel {
    padding: 50px;
    display: none;
}
</style>
</style>
<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
  <div class="container">
    <div class="header-left grid">
      <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
        <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
      </div>
    </div>
    <div class="header-middle">
      <!-- <ul>
        <li><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>+123 456 7890</li>
        <li><a href="mailto:info@example.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>info@example.com</a></li>
      </ul> -->
      <div class="search">
        <form action="#" method="post">
          <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
          <input type="submit" value=" ">
        </form>
      </div>
    </div>
    <div class="header-right">
      </div>
  </div>
</div>
</ul>
</div>
</div>
</div>
<!-- *****************************end search box****************** -->
<!-- <div class="row">
<div class="col-md-1"></div>
<div class="col-md-5">
  <div class="jumbotron" style="background-color:white;height:auto;">
   <div class="col-md-3">
      <img src="images/hives.png" style="background-color:blue;margin-top:-100px;width:70%;height:70%;">
    </div>
    <div class="col-md-3" style="width:30%;margin-top:-30px;">
      <h3>Hives Lab</h3>
      
      
</div>
 <div class="col-md-3" sttyle></div>
<p>Hello! I�m Christina Aguilera - Graphic Designer from USA. I�m always obsessed by the nice detail. I�m available to hire. Let feel free to contact me via...</p>

    
  </div>


  </div>


<div class="col-md-4">
  <div class="jumbotron" style="background-color:white;">

  </div>

</div>
  <div class="col-md-2"></div>

  </div> -->
 
<div class="container">
    <div id="flip">
		<button>Click to slide down panel</button></div>
    <div id="panel">
  <div class="row">
    <div class="col-md-6">
      <a href="test2.php" style="color:black;"> <div class="well profile">
            <div class="col-sm-12">
                <div class="col-xs-12 col-sm-8">
                    <h2 style="font-size:20px;">Nicole Pearson</h2>
                    <p ><strong style="font-size:20px;">About: </strong> Web Designer / UI. </p>
                    <p><strong style="font-size:20px;">Hobbies: </strong> Read, out with friends, listen to music, draw and learn new things. </p>
                    <p><strong style="font-size:20px;">Skills: </strong>
                        <span class="tags">html5</span> 
                        <span class="tags">css3</span>
                        <span class="tags">jquery</span>
                        <span class="tags">bootstrap3</span>
                    </p>
                </div> <br>            
                <div class="col-xs-12 col-sm-4 text-center">
                    <figure>
                        <img src="images/pic.jpg" alt="" class="img-circle img-responsive" style="margin-top: -6px;margin-left:30px;">
                        <figcaption class="ratings">
                            <p>Ratings
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                 <span class="fa fa-star-o"></span>
                            </a> 
                            </p>
                        </figcaption>
                    </figure>
                </div>
            </div> </a>           
            
       </div>                 
    </div>
    
     <div class="col-md-6">
      <a href="test2.php" style="color:black;"> <div class="well profile">
            <div class="col-sm-12">
                <div class="col-xs-12 col-sm-8">
                    <h2 style="font-size:20px;">Nicole Pearson</h2>
                    <p ><strong style="font-size:20px;">About: </strong> Web Designer / UI. </p>
                    <p><strong style="font-size:20px;">Hobbies: </strong> Read, out with friends, listen to music, draw and learn new things. </p>
                    <p><strong style="font-size:20px;">Skills: </strong>
                        <span class="tags">html5</span> 
                        <span class="tags">css3</span>
                        <span class="tags">jquery</span>
                        <span class="tags">bootstrap3</span>
                    </p>
                </div> <br>            
                <div class="col-xs-12 col-sm-4 text-center">
                    <figure>
                        <img src="images/pic.jpg" alt="" class="img-circle img-responsive" style="margin-top: -6px;margin-left:30px;">
                        <figcaption class="ratings">
                            <p>Ratings
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                 <span class="fa fa-star-o"></span>
                            </a> 
                            </p>
                        </figcaption>
                    </figure>
                </div>
            </div> </a>           
            
       </div>                 
    </div>
</div>
</div>
   <!--  ***************************2nd********************** -->
  <script> 
$(document).ready(function(){
    $("#flip").click(function(){
        $("#panel").slideDown("slow");
    });
});
</script>
   

</body>
</html>

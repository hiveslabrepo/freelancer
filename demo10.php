<!DOCTYPE  html>
<html>
<head>
    <style>
    

            /*--------------------------------------*/
            /*     jpolly168 AeVale/OneSixEight     */
            /*     XblackvelvetX                    */
            /*     Main Elements,,,,,,,             */
            /*     #wah comes int waves....wahhhhhh.*/
            /*--------------------------------------*/
            
    @import url(http://fonts.googleapis.com/css?family=Roboto:400,500,900);
body {
      background-color: #f44336;
                transition: background-color 300ms;
                font-family: 'Roboto', sans-serif;   
                background:url('http://40.media.tumblr.com/6e138b33e7e2fadf3ff47d02fd28d74a/tumblr_nscw8gfVlY1uc3lv1o1_1280.jpg');
                background-repeat:none;
                background-size:cover;
}
.avatar {
                width: 200px;
                height: 200px;
                margin-left: 2em;
                margin-top:2em;
                border-radius: 200px;
                float:center;
}
.jumbotron{
    background-color:#ffffff;
    border:1px solid #AAA;
    border-bottom:3px solid #BBB;
    padding:0px;
    margin-top:4em;
    overflow:hidden;
    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    font-family: 'Roboto', sans-serif;
    -webkit-transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
          transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);    
}
    .header{
                background: #607D8B;
                
                }
            .blue h1, h2, h3 {
                color: #2196F3;
            }
            .headline {
                color: #FFFFFF;
                margin: 1em;
            }
.card {
    background:#FFF;
    display: block;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    border:1px solid #AAA;
    border-bottom:3px solid #BBB;
    padding:0px;
    margin-top:5em;
    overflow:hidden;
    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    font-family: 'Roboto', sans-serif;
    -webkit-transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
          transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
}

.card-body{
 margin:1em;   
}

.pull-left {
  float: left;
}

.pull-none {
  float: none !important;
}

.pull-right {
  float: right;
}

.card-header{
    width:100%;
    color:#2196F3;
    border-bottom:3px solid #BBB;
    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    font-family: 'Roboto', sans-serif;
    padding:0px;
    margin-top:0px;
    overflow:hidden;
    -webkit-transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
          transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
    
}
.card-header-blue{
    background-color:#2196F3;
    color:#FFFFFF;
    border-bottom:3px solid #BBB;
    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    font-family: 'Roboto', sans-serif;
    padding:0px;
    margin-top:0px;
    overflow:hidden;
    -webkit-transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
          transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
}
.card-header-red{
    background-color:#F44336;
    color:#FFFFFF;
    border-bottom:3px solid #BBB;
    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    font-family: 'Roboto', sans-serif;
    padding:0px;
    margin-top:0px;
    overflow:hidden;
    -webkit-transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
          transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
}
.card-header-grey{
    background-color:#424242;
    color:#FFFFFF;
    border-bottom:3px solid #BBB;
    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    font-family: 'Roboto', sans-serif;
    padding:0px;
    margin-top:0px;
    overflow:hidden;
    -webkit-transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
          transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
}
.card-header-orange{
    background-color:#E65100;
    color:#FFFFFF;
    border-bottom:3px solid #BBB;
    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    font-family: 'Roboto', sans-serif;
    padding:0px;
    margin-top:0px;
    overflow:hidden;
    -webkit-transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
          transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
}
.card-header-pink{
    background-color:#E91E63;
    color:#FFFFFF;
    border-bottom:3px solid #BBB;
    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    font-family: 'Roboto', sans-serif;
    padding:0px;
    margin-top:0px;
    overflow:hidden;
    -webkit-transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
          transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
}

.card-heading {
    display: block;
    font-size: 24px;
    line-height: 28px;
    margin-bottom: 24px;
    margin-left:1em;
    border-bottom: 1px #2196F3;
    color:#fff;
   
}
.card-footer{
 border-top:1px solid #000;   
    
}

.btn:hover:not(.btn-link):not(.btn-flat) {
  box-shadow: 0 6px 10px rgba(0, 0, 0, 0.23), 0 10px 30px rgba(0, 0, 0, 0.19);
}
.btn:active:not(.btn-link):not(.btn-flat) {
  box-shadow: 0 6px 10px rgba(0, 0, 0, 0.23), 0 10px 30px rgba(0, 0, 0, 0.19);
}
.btn:not(.btn-link):not(.btn-flat),
.btn-default:not(.btn-link):not(.btn-flat) {
  background-color: transparent;
}
.btn-grey-mat{
    background-color:#607D8B;
    color:#ffffff;    
    solid 1px; 
    margin:10px;
}

.card-action-pink{
    color:#E91E63;

}
.card-action-red{
    color:#F44336;

}
.card-action-grey{
    color:#424242;

}
.card-action-pink{
    color:#E91E63;

}
.card-action-orange{
    color:#E65100;

}
        .img-fill  {
                max-height: 100%;
                max-width: 100% display :inline-block;
                margin: 1em;
            }
            
                .card-image {
                overflow: hidden;
                position: relative;
                margin-bottom: 1em;
            }
            .card-image:first-child {
                border-radius: 2px 2px 0 0;
            }
            .card-image:last-child {
                border-radius: 0 0 2px 2px;
            }
            .card-image img {
                display: block;
                height: auto;
                width: 100%;
            }

            .card-image-text {
                background-image: -webkit-linear-gradient(top, transparent, rgba(0, 0, 0, 0.5));
                background-image: linear-gradient(to bottom, transparent, rgba(0, 0, 0, 0.5));
                color: #ffffff;
                font-size: 20px;
                line-height: 28px;
                margin: 0;
                padding: 12px 16px;
                position: absolute;
                bottom: 0;
                left: 0;
                width: 100%;
            }
        /**OVERFLOW MENU**/
        /**MENU SETTINGS POPUP**/

            .menu {
                display: none;
                background:#607D8B;
                width: 100%;
                padding: 30px 10px 50px;
                margin: 0 auto;
                text-align: center;
                background-color: #fff;
            }
            .menu ul {
                margin-bottom: 0;
            }
            .menu a {
                color: #000;
                text-transform: uppercase;
                opacity: 0.8;
            }
            .menu a:hover {
                text-decoration: none;
                opacity: 1;
            }

    </style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/paper/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
 <div class="jumbotron">
            <h1 class="blue" style="color:#2196F3;">Material Design Like Cards </h1>
            <h3 class="blue">With Headers</h3>
          
        </div>
    </div>
          <!--small cards-->
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-content">
                        <div class="card-header-blue">
                           <h1 class="card-heading">Card Header Blue</h1>
                        </div>
                        <div class="card-body">
                          <p class="card-p">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,
                                or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there
                                isn't anything embarrassing hidden in the middle of text.
                                
                            </p>
                        </div>
                        
                        <nav class="nav-tabs">
                            <ul class="nav nav-pills pull-left">
                                <li class="card-action"><a href="#">Link</a></li>
                                <li class="card-action"><a href="#">Link</a></li>
                                <li class="card-action"><a href="#">Link</a></li>
                                <li class="card-action"><a href="#">Link</a></li>
                            </ul>
                            
                        </nav>
                            
                    </div>
                </div>
            </div>
        <div class="col-md-4">
            <div class="card">
                    <div class="card-content">
                        <div class="card-header-orange">
                           <h1 class="card-heading">Card Header Orange</h1>
                        </div>
                        <div class="card-body">
                          <p class="card-p">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,
                                or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there
                                isn't anything embarrassing hidden in the middle of text.
                                
                            </p>
                        </div>
                        <nav class="nav-tabs">
                            <ul class="nav nav-pills pull-left">
                                <li><a class="card-action-orange" href="#">Link</a></li>
                                <li><a class="card-action-orange" href="#">Link</a></li>
                                <li><a class="card-action-orange" href="#">Link</a></li>
                                <li><a class="card-action-orange" href="#">Link</a></li>
                            </ul>
                            
                        </nav>
                            
                    </div>
                </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                    <div class="card-content">
                        <div class="card-header-grey">
                           <h1 class="card-heading">Card Header Grey</h1>
                        </div>
                        <div class="card-body">
                          <p class="card-p">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,
                                or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there
                                isn't anything embarrassing hidden in the middle of text.
                                
                            </p>
                        </div>
                        
                        <nav class="nav-tabs">
                            <ul class="nav nav-pills pull-left">
                                <li ><a class="card-action-grey" href="#">Link</a></li>
                                <li><a class="card-action-grey" href="#">Link</a></li>
                                <li><a class="card-action-grey" href="#">Link</a></li>
                                <li><a class="card-action-grey" href="#">Link</a></li>
                            </ul>
                            
                        </nav>
                            
                    </div>
                </div>
        </div>
        </div>
        <!--row two-->
         <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-content">
                        <div class="card-header-pink">
                           <h1 class="card-heading">Card Header Pink</h1>
                        </div>
                        <div class="card-body">
                          <p class="card-p">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,
                                or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there
                                isn't anything embarrassing hidden in the middle of text.
                                
                            </p>
                        </div>
                        
                        <nav class="nav-tabs">
                            <ul class="nav nav-pills pull-left">
                                <li><a class="card-action-pink" href="#">Link</a></li>
                                <li><a class="card-action-pink" href="#">Link</a></li>
                                <li><a class="card-action-pink" href="#">Link</a></li>
                                <li><a class="card-action-pink" href="#">Link</a></li>
                            </ul>
                            
                        </nav>
                            
                    </div>
                </div>
            </div>
        <div class="col-md-4">
            <div class="card">
                    <div class="card-content">
                        <div class="card-header-red">
                           <h1 class="card-heading">Card Header Red</h1>
                        </div>
                        <div class="card-body">
                          <p class="card-p">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,
                                or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there
                                isn't anything embarrassing hidden in the middle of text.
                                
                            </p>
                        </div>
                        
                        <nav class="nav-tabs">
                            <ul class="nav nav-pills pull-left">
                                <li><a class="card-action-red" href="#">Link</a></li>
                                <li><a class="card-action-red" href="#">Link</a></li>
                                <li><a class="card-action-red" href="#">Link</a></li>
                                <li><a class="card-action-red" href="#">Link</a></li>
                            </ul>
                            
                        </nav>
                            
                    </div>
                </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                    <div class="card-content">
                        <div class="card-header">
                           <h1 class="card-heading">Card Header Blank</h1>
                        </div>
                        <div class="card-body">
                          <p class="card-p">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,
                                or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there
                                isn't anything embarrassing hidden in the middle of text.
                                
                            </p>
                        </div>
                        
                        <nav class="nav-tabs">
                            <ul class="nav nav-pills pull-left">
                                <li class="card-action"><a href="#"><span><i class="fa fa-facebook"></i></span></a></li>
                                <li class="card-action"><a href="#"><span><i class="fa fa-google-plus"></i></span></a></li>
                                <li class="card-action"><a href="#"><span><i class="fa fa-share-alt"></i></span></a></li>
                                <li class="card-action"><a href="#"><span><i class="fa fa-plus"></i></span></a></li>
                            </ul>
                            
                        </nav>
                            
                    </div>
                </div>
        </div>
        </div>
    </div>
</div>



<div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6" align=:"left">
                            <div class="card-header-grey" align="center" id="picture" style="" >
                                <div class="card-action">
                                    <ul class="nav nav-list pull-left">
                                        <li>
                                            <a data-toggle="collapse" href="#menu"><span style="font-size: x-large; color:ffffff;"> <i style="color:#ffffff;" class="glyphicon glyphicon-option-vertical"></i></span></a>
                                        </li>
                                    </ul>
                                    <div class="collapse menu" id="menu">
                                        <h3>Settings</h3>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="#">Profile</a>
                                            </li>
                                            <li>
                                                <a href="#">Data</a>
                                            </li>
                                            <li>
                                                <a href="#">Services</a>
                                            </li>
                                            <li>
                                                <a href="#">Wipe/Data Reset</a>
                                            </li>
                                            <li>
                                                <a href="#">Contact</a>
                                            </li>
                                        </ul>
                                    
                                    </div>
                            
                                </div>
                                <img class="avatar" src="http://aevale.com/images/hair_long_peace.jpg"/>
                                <h2 class="headline">jpolly168</h2>
                                <h5 class="headline">XblackvelvetX</h5>
                                  <div class="info">
                    <div class="title">
                        <a target="_blank" href="http://aevale.com/Welcome.html">Cigar and Coffee Time</a>
                    </div>
                    <div class="desc">Beers</div>
                    <div class="desc">Brunettes</div>
                    <div class="desc">Bootstrap</div>
                </div>
                                
                                <!--
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-6">
                                            <div class="offer offer-default">
                                                <div class="shape">
                                                    <div class="shape-text">
                                                        XbvX
                                                    </div>
                                                </div>
                                                <div class="offer-content">
                                                    <h3 class="lead"> Notifications and Offers </h3>
                                                    <p class="lead">
                                                     None
                                                        <br>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--
                                <div class="sidebar-nav">
                                <div class="well" style="width:300px; padding: 8px 0;">
                                <ul class="nav nav-list">
                                <li class="nav-header">
                                Admin Menu
                                </li>
                                <li>
                                <a href="index"><i class="icon-home"></i> Dashboard</a>
                                </li>
                                <li>
                                <a href="#"><i class="icon-envelope"></i> Messages <span class="badge badge-info">4</span></a>
                                </li>
                                <li>
                                <a href="#"><i class="icon-comment"></i> Comments <span class="badge badge-info">10</span></a>
                                </li>
                                <li class="active">
                                <a href="#"><i class="icon-user"></i> Members</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                <a href="#"><i class="icon-comment"></i> Settings</a>
                                </li>
                                <li>
                                <a href="#"><i class="icon-share"></i> Logout</a>
                                </li>
                                </ul>
                                </div>
                                </div>-->

                                <p>
                                    <div class="card-action" align="center">
                                        <ul class="nav nav-tabs-justified" >
                                            <li>
                                                <a href="http://www.github.com/jpolly168" class="btn btn-primary btn-lg"><span>Github   <i class="fa fa-github"></i></span></a>
                                            </li>
                                            <li>
                                                <a href="http://bootsnipp.com/user/snippets/M2Q19" class="btn btn-primary btn-lg"><span>Bootsnipp   <i class="fa fa-scissors"></i> </span></a>
                                            </li>
                                            <li>
                                                <a class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" data-original-title> <span>Email   <i class="glyphicon glyphicon-envelope"> </i> </span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </p>

                            </div>
                        </div>

                        <div class="col-lg-8"  align="center">
                            <div class="card-image" >
                                <img class="card-image"  src="http://38.media.tumblr.com/cd5937901a97e7fea254599455c2c6d2/tumblr_ns2hinPIXM1uc2yrlo1_500.gif"  />
                            </div>
                            <br/>
                            <div class="card-image" >
                                <img class="img-responsive"  src="http://aevale.com/images/warer-glisten.gif" />
                                <!--
                                <img class="card-img"  src="https://lh6.googleusercontent.com/-1-Wicwoi9O0/VGdNZDtA1tI/AAAAAAAADz4/mE2W7vMpf08/w1004-h803-no/Ultimate%2BMaterial%2BLollipop%2BCollection%2B-%2B470">
                                -->
                            </div>
                        </div>
                        <div class="card-action">
                            <ul class="nav nav-tabs pull-right" >
                                <li>
                                    <a href="Blog.html" class="btn btn-primary btn-sm"><span>Blog <i class="fa fa-venus-mars"></i></span></a>.
                                </li>
                                <li>
                                    <a href="Music.html" class="btn btn-primary btn-sm"><span>Music  <i class="fa fa-headphones"></i></span></a>.
                                </li>
                                <li>
                                    <a href="Movies.html" class="btn btn-info btn-sm"><span>Movies <i class="fa fa-television"></i></span></a>.
                                </li>
                                <li>
                                    <a href="Welcome.html" class="btn btn-info btn-sm" ><span>Welcome Carousel <i class="fa fa-commenting"></i> </span></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="card-footer">
                            <h3>Welcome. Enjoy the Resources!</h3>
                            <p class="lead">
                                Here are some code samples, Bootrap themes, and other components and resources for you to use
                                however you wish. All I ask is you give proper attribution to my work and any others. I will be adding and fixing this as time goes on. You may also alter
                                it in anyway that you like. Feel free to copy and play with it as you see fit.
                            </p>

                            <p class="lead">
                                For Bootstrap templates to work properly you should use Bootstrap 3.3.5 (or current) as well as JavaScript 1.11.3. In addition, fonts use FontAwesome webfonts.
                            </p>
                            <p class="lead">
                                If you have any requests feel free to ask. I will do my best to get it done for you.
                            </p>

                        </div>
                    </div>
                </div>
                <br/>
                <br/>
            </div>
        </body>
        </html>




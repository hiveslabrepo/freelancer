<?php require_once ("header.php");?>

<style>
.banner{
    background: url("images/bg2.jpg") no-repeat center;

    background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    -ms-background-size: cover;
    -moz-background-size: cover;
    min-height: 1000px;      
}
.navbar-nav > li > a{
    font-size: 15px;
}

.caption {
        width:100%;
        bottom: .3rem;
        position: absolute;
        background:#000;
        background: -webkit-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: -moz-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: -o-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: linear-gradient(to top, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
    }

    .thumbnail {
        border: 0 none;
        box-shadow: none;
        margin:0;
        padding:0;
    }

    .caption h4 {
        color: #fff;
        -webkit-font-smoothing: antialiased;
         margin-top:-400px;
         margin-left:500px;
    }
    
    .col-md-3 h2{
        margin-top:160px;
        
    }
   .container .well {
    margin-top:100px;

    }
   /* .control-label{
        margin-right: 10px;
    }*/
    .row{
            margin-top: -50px;
            /*margin-left: 500px;*/
    }
    select{
            width: 250px;
    }
    label{
      margin:-25px;
    }
    strong{
      font-size: 20px;
    }
     .add-photo-btn1{
   position:relative;
   overflow:hidden;
   cursor:pointer;
   text-align:center;
   background-color:#428bca;
   color:#fff;
   display:block;
   width:180px;
   height:28px;
   font-size:12px;
   line-height:30px;
   float:right;
   margin-right: 260px;
   margin-top: 1px;
 }
 .form input[type="file"]{
  z-index: 999;
  line-height: 0;
  font-size: 50px;
  position: absolute;
  opacity: 0;
  filter: alpha(opacity = 0);-ms-filter: "alpha(opacity=0)";
  cursor: pointer;
  _cursor: hand;
  margin: 0;
  padding:0;
  left:0;

  }
  .row-padding {
    margin-top: 25px;
    margin-bottom: 25px;
}
.modal.in .modal-dialog{
       margin-left: -20px;
}
#popup1 {
    background-color: white;
    width: 700px;
    height: 400px;
    overflow: scroll;
    margin-left: -30%;
    overflow-x: hidden;

}
#popup2 {
    background-color: white;
    width: 700px;
    height: 400px;
    overflow: scroll;
    margin-left: -30%;
    overflow-x: hidden;

}
  .popup {
    display: none;
    background-color: #FFFFFF;
    position: fixed;
    top: 150px;
    padding: 40px 25px 25px 25px;
    width: 350px;
    z-index: 999;
    left: 50%;
    margin-left: -200px;
  }
  
  .pop_overlay{
    height: 100%;
    width: 100%;
    background-color: #F6F6F6;
    opacity: 0.9;
    position: fixed;
    z-index: 998;
  }
  
  a.close{
    color: #999;
    text-decoration: none;
    position: absolute;
    right: 15px;
    top: 15px;
  }
  .btn{
    background-color: #008CBA;;
    color:white;
    font-size: 20px;
    border-radius: 10px;
    margin-left:500px;
}
/*.well{
  width:20%;
  margin-left: 350px;
  margin-top: -130px;
  height:500px;

}*/

  </style>
  <script>
  $(function () {
    $( '#table' ).searchable({
        striped: true,
        oddRow: { 'background-color': '#f5f5f5' },
        evenRow: { 'background-color': '#fff' },
        searchType: 'fuzzy'
    });
    
    $( '#searchable-container' ).searchable({
        searchField: '#container-search',
        selector: '.row',
        childSelector: '.col-xs-4',
        show: function( elem ) {
            elem.slideDown(100);
        },
        hide: function( elem ) {
            elem.slideUp( 100 );
        }
    })
});

  </script>
  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function ($) {
    // Show the popup on click
    $('#show_pop1, #show_pop2').on('click', function (e) {
      $('body').prepend('<div class="pop_overlay"></div>');
      if ($(this).attr('id') == 'show_pop1') 
        $('#popup1').fadeIn(500);
      else 
        $('#popup2').fadeIn(500);
      e.preventDefault();
    });   
    
    // Open popup from link inside another popup
    $('#pop1, #pop2').on('click', function (e) {
      toFadeOut = $('#popup1');
      toFadeIn = $('#popup2');
      if ($(this).attr('id') == 'pop1') {
        toFadeOut = $('#popup2');
        toFadeIn = $('#popup1');
      }
      toFadeOut.fadeOut(500, function () {
        toFadeIn.fadeIn();
      })
      return false;
    });
    
    // Close popup
    $(document).on('click', '.pop_overlay, .close', function () {
      $('#popup1, #popup2').fadeOut(500, function () {
        $('.pop_overlay').remove();
      });
      return false;
    });   
  });
</script>

     
    

<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
    <div class="container">
        <div class="header-left grid">
            <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
                <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
            </div>
        </div>
        <div class="header-middle">
            <!-- <ul>
                <li><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>+123 456 7890</li>
                <li><a href="mailto:info@example.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>info@example.com</a></li>
            </ul> -->
           <!--  <div class="search">
                <form action="#" method="post">
                    <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
                    <input type="submit" value=" ">
                </form>
            </div> -->
        </div>
        <div class="header-right">
            <!-- <div class="search">
                <form action="#" method="post">
                    <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
                    <input type="submit" value=" ">
                </form>
            </div> -->
             <ul class="nav navbar-nav menu__list">


                        <!-- <li class="active menu__item menu__item--current" class="page-scroll" a href="#portfolio" data-toggle="modal" data-target="#login"><a href="#portfolio" data-toggle="modal" data-target="#login">Login <span class="sr-only">(current)</span></a></li> -->
                       <!--  <li class=" menu__item menu__item--current"><a class="menu__link" href="index.php">Logout</a></li> -->
            

            <!-- <li class=" menu__item"><a href="#portfolio" data-toggle="modal" data-target="#register">Sign Up</a></li> -->
          </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
                    </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
 <!-- <div class="caption">
   <h4>Start Work</h4></div> -->
  
  <!--  <div class="col-md-3">
    <div class="service-item" style="margin-top:50px; ">
     <span class="fa-stack fa-4x">
        
       <img class="img-responsive" src="images/blank.png" class="" style="border:2px solid gray;"></i>
     </span>
                           
                                
    </div>
 </div>
                        <div class="col-md-3">
                            <h2>Priyanshe Yadav</h3>
                            </div> -->
 

  

  
 
  

<div class="banner">
     
   <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">

<div class="well well-lg" style="margin-top:50px;">

      <div class="container-fluid text-center">
  <div class="row content">
  
    <div class="col-sm-10 text-left">
      <h1 style="font-size:25px;" >Start Work:</h1>
      <div class="container">
      
 <form>
    <div class="form-group">
   <div class="col-xs-7">
  <label for="skill"><h3><strong style="font-size:18px;">Interest to work for:</strong></h3></label><br>
  <div class="radio">
  <label><input type="radio" name="optradio"><h4><strong style="font-size:18px;">Company</strong></h4></label>
  </div>
  <div class="radio">
  <label><input type="radio" name="optradio"><h4><strong style="font-size:18px;">Individuls<strong></h4></label>
  </div>
  </div>
   
    <div class="form-group">
    <div class="col-xs-7">
        <span class="glyphicon glyphicon-filename"></span> 
 <label for="pwd"><h3><strong style="font-size:18px;">User Name:</strong></h3></label> 
      <input type="text" class="form-control" placeholder="Enter name" id="name"><br>
    </div>
  </div>
    
     <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">Password:</strong></h3></label>
        <input type="password" class="form-control"  placeholder="Enter password"  id="password"><br>
    </div>
    </div>
    
     <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">Email:</strong></h3></label>
        <input type="text" class="form-control" placeholder="e.g. abc@xyz.com" id="email"><br>
    </div>
    </div>
        
  
      <div class="form-group">
  <div class="col-xs-7">
   <label for="mobile"><h3><strong style="font-size:18px;">Mobile Number:</strong></h3></label> 
      <input type="text" class="form-control" placeholder="Mobile Number"id="usr"><br>
    </div>
  </div>
  
   <div class="form-group">

    <div class="col-xs-7">
     <!--  <label for="project_type"><h3><strong style="font-size:18px;">Find Work :</strong></h3></label> -->
     
  <a id="show_pop1" href=""> <label class="radio-inline">
    <h3><strong style="font-size:18px;">Find Work :</strong></h3>
      <input type="radio" name="optradio"><h3 style="color:black;">Technical</h3>
    </label></a> <br> <a id="show_pop2" href=""><label class="radio-inline">
      <input type="radio" name="optradio"><h3 style="color:black;">Non-Technical</h3>
    </label>
    </a>
<div id="popup1" class="popup" style="width:auto;height:auto;">
    <h3><a id="pop2" href="">Select your skills and expertise</a></h3>
    
    <!-- <div class="row">
        <div class="col-lg-12 col-lg-offset-12;">
            <input type="search" id="container-search" value="" class="form-control" placeholder="Search..." style="margin-left:30px;width:550px;margin-top: -190px;">
        </div>
    </div> -->
    <!-- <h1 align="center">POPUP 1</h1> 
    <p align="justify">This is first popup. Place your content here.</p> -->
     <div class="row">
        <div class="col-lg-12 col-lg-offset-12;">
            <input type="search" id="container-search" value="" class="form-control" placeholder="Search..." style="margin-left:30px;width:550px;margin-top: 20px;">
        </div>
    </div>
    <div class="row" style="margin-top:40px;margin-left:25px;">
      <div class="col-xs-6" style="width:50%;">
         <ul class="list-group">
   <a id="pop2" href=""> <li class="list-group-item">First item</li></a>
    <li class="list-group-item">Second item</li>
    <li class="list-group-item">Third item</li>
  </ul>


    </div>

    <div class="col-xs-6"style="width:50%;margin-left:-30px;">
      <ul class="list-group">
    <li class="list-group-item">First item</li>
    <li class="list-group-item">Second item</li>
    <li class="list-group-item">Third item</li>
  </ul>

    </div>
  </div>

    <a class="close" href="">[ x ]</a>
</div>
<div id="popup2" class="popup">
    <h3><a id="pop1" href="">Show popup one</a></h3>
    <hr />
    <h1 align="center">POPUP 2</h1> 
    <p align="justify">This is second popup. Place your content here.</p>
    <a class="close" href="">[ x ]</a>    
</div>

    </div>
  </div>
  
     <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">Skills:</strong></h3></label>
        <input type="text" class="form-control" placeholder="e.g. Java,php,Sql" id="skills"><br>
    </div>
    </div>
    
       <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">PAN Card/Adaar Card No:</strong></h3></label>
        <input type="text" class="form-control" placeholder="e.g. Java,php,Sql" id="skills"><br>
    </div>
    </div>
    
    
    
     <div class="form-group">
    <div class="col-xs-7">
      <label for="discription"><h3><strong style="font-size:18px;">About You:</strong></h3></label>
      <div class="form-group">
      <textarea class="form-control" rows="5" id="comment" placeholder="Describe about you here....."></textarea>
    </div>
    </div>
  
      <div class="form-group">
      <div class="col-xs-7">
      <span class="icon-attach"></span>
     
      <p><span class="glyphicon glyphicon-paperclip"></span><strong style="font-size:15px;">Attach Resume:</strong></p>
     <input type="text" id="path" placeholder="Select File" style="font-size:15px;" />
  <label class="add-photo-btn1">Browse
    <span>
       <input type="file" id="myfile" name="myfile" />
    </span>
    </label><br><br>
    </div>
    </div>
  
   <div class="form-group">
        <div class="col-xs-7">
      <button type="button" class="btn btn-primary btn-lg">Submit</button>
    </div>
    </div>
  </form>
</div>
    
    </div>
    
    
  </form>
</div>                     
                                
    
                        <div class="col-md-3">
                           
                            </div>
    
    </div>
    <!-- <div class="caption">
   <h4 style="font-size:50px;">Start Work</h4></div> -->

      
<!-- <div class="well well-lg">
<div class="container-fluid text-center">
  <div class="row content">
  
    <div class="col-sm-10 text-left">
      <h1 >Start Work:</h1>
      <div class="container">
      
 
</div>
</div>
</div>
</div>
</div> -->

    </body>
    </html>
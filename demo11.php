<?php require_once ("header.php");?>
<head>
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Acme' rel='stylesheet' type='text/css'><!-- //fonts -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Pets Love Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- <link href="css/freelancer.min.css" rel="stylesheet"> -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>

<style>
.banner{
    background: url("images/bg2.jpg") no-repeat center;

    background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    -ms-background-size: cover;
    -moz-background-size: cover;
    min-height: 1000px;      
}
.navbar-nav > li > a{
    font-size: 15px;
}

.caption {
        width:100%;
        bottom: .3rem;
        position: absolute;
        background:#000;
        background: -webkit-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: -moz-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: -o-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: linear-gradient(to top, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
    }

    .thumbnail {
        border: 0 none;
        box-shadow: none;
        margin:0;
        padding:0;
    }

    .caption h4 {
        color: #fff;
        -webkit-font-smoothing: antialiased;
         margin-top:-400px;
         margin-left:500px;
    }
    
    .col-md-3 h2{
        margin-top:160px;
        
    }
   .container .well {
    margin-top:100px;

    }
   /* .control-label{
        margin-right: 10px;
    }*/
    .row{
            margin-top: -50px;
            /*margin-left: 500px;*/
    }
    select{
            width: 250px;
    }
    label{
      margin:-25px;
    }
    strong{
      font-size: 20px;
    }
     .add-photo-btn1{
   position:relative;
   overflow:hidden;
   cursor:pointer;
   text-align:center;
   background-color:#428bca;
   color:#fff;
   display:block;
   width:180px;
   height:28px;
   font-size:12px;
   line-height:30px;
   float:right;
   margin-right: 260px;
   margin-top: 1px;
 }
 .form input[type="file"]{
  z-index: 999;
  line-height: 0;
  font-size: 50px;
  position: absolute;
  opacity: 0;
  filter: alpha(opacity = 0);-ms-filter: "alpha(opacity=0)";
  cursor: pointer;
  _cursor: hand;
  margin: 0;
  padding:0;
  left:0;

  }
  .row-padding {
    margin-top: 25px;
    margin-bottom: 25px;
}
.modal.in .modal-dialog{
       margin-left: -20px;
}
#popup1 {
    background-color: white;
    width: 700px;
    height: 400px;
    overflow: scroll;
    margin-left: -30%;
    overflow-x: hidden;

}
#popup2 {
    background-color: white;
    width: 700px;
    height: 400px;
    overflow: scroll;
    margin-left: -30%;
    overflow-x: hidden;

}
  .popup {
    display: none;
    background-color: #FFFFFF;
    position: fixed;
    top: 150px;
    padding: 40px 25px 25px 25px;
    width: 350px;
    z-index: 999;
    left: 50%;
    margin-left: -200px;
  }
  
  .pop_overlay{
    height: 100%;
    width: 100%;
    background-color: #F6F6F6;
    opacity: 0.9;
    position: fixed;
    z-index: 998;
  }
  
  a.close{
    color: #999;
    text-decoration: none;
    position: absolute;
    right: 15px;
    top: 15px;
  }
  .btn{
    background-color: #008CBA;;
    color:white;
    font-size: 20px;
    border-radius: 10px;
    margin-left:500px;
}
.well{
  width:20%;
  margin-left: 350px;
  margin-top: -130px;
  height:500px;

}

  </style>

  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function ($) {
    // Show the popup on click
    $('#show_pop1, #show_pop2').on('click', function (e) {
      $('body').prepend('<div class="pop_overlay"></div>');
      if ($(this).attr('id') == 'show_pop1') 
        $('#popup1').fadeIn(500);
      else 
        $('#popup2').fadeIn(500);
      e.preventDefault();
    });   
    
    // Open popup from link inside another popup
    $('#pop1, #pop2').on('click', function (e) {
      toFadeOut = $('#popup1');
      toFadeIn = $('#popup2');
      if ($(this).attr('id') == 'pop1') {
        toFadeOut = $('#popup2');
        toFadeIn = $('#popup1');
      }
      toFadeOut.fadeOut(500, function () {
        toFadeIn.fadeIn();
      })
      return false;
    });
    
    // Close popup
    $(document).on('click', '.pop_overlay, .close', function () {
      $('#popup1, #popup2').fadeOut(500, function () {
        $('.pop_overlay').remove();
      });
      return false;
    });   
  });
</script>
  <script>
  $(function () {
    $( '#table' ).searchable({
        striped: true,
        oddRow: { 'background-color': '#f5f5f5' },
        evenRow: { 'background-color': '#fff' },
        searchType: 'fuzzy'
    });
    
    $( '#searchable-container' ).searchable({
        searchField: '#container-search',
        selector: '.row',
        childSelector: '.col-xs-4',
        show: function( elem ) {
            elem.slideDown(100);
        },
        hide: function( elem ) {
            elem.slideUp( 100 );
        }
    })
});

  </script>

     
    

</head>
<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
    <div class="container">
        <div class="header-left grid">
            <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
                <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
            </div>
        </div>
        <div class="header-middle">
           
        </div>
        <div class="header-right">
           
             <ul class="nav navbar-nav menu__list">
             </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
                    </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
 
<div class="banner">
     
   <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">

<div class="well well-lg" style="margin-top:50px;">

      <div class="container-fluid text-center">
  <div class="row content">
  
    <div class="col-sm-10 text-left">
      <h1 style="font-size:25px;" >Start Work:</h1>
      <div class="container">
      
 <form>
    <div class="form-group">
   <div class="col-xs-7">
  <label for="skill"><h3><strong style="font-size:18px;">Interest to work for:</strong></h3></label><br>
  <div class="radio">
  <label><input type="radio" name="optradio"><h4><strong style="font-size:18px;">Company</strong></h4></label>
  </div>
  <div class="radio">
  <label><input type="radio" name="optradio"><h4><strong style="font-size:18px;">Individuls<strong></h4></label>
  </div>
  </div>
   
    <div class="form-group">
    <div class="col-xs-7">
        <span class="glyphicon glyphicon-filename"></span> 
 <label for="pwd"><h3><strong style="font-size:18px;">User Name:</strong></h3></label> 
      <input type="text" class="form-control" placeholder="Enter name" id="name"><br>
    </div>
  </div>
    
     <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">Password:</strong></h3></label>
        <input type="password" class="form-control"  placeholder="Enter password"  id="password"><br>
    </div>
    </div>
    
     <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">Email:</strong></h3></label>
        <input type="text" class="form-control" placeholder="e.g. abc@xyz.com" id="email"><br>
    </div>
    </div>
        
  
      <div class="form-group">
  <div class="col-xs-7">
   <label for="mobile"><h3><strong style="font-size:18px;">Mobile Number:</strong></h3></label> 
      <input type="text" class="form-control" placeholder="Mobile Number"id="usr"><br>
    </div>
  </div>
  <form>
   <div class="form-group">
     <div class="col-xs-7">
  
   <a id="show_pop1" href="" style="text-decoration:none;"> <label class="radio-inline">
      <input type="radio" name="optradio">Technical
    </label></a> <br> <a id="show_pop2" href="" style="text-decoration:none;">
    <label class="radio-inline">
      <input type="radio" name="optradio">Non-Technical
    </label>
    </a>
  </div>
</div>
</form>
<div id="popup1" class="popup" >
    <h3 style="margin-left:40px;font-size:20px;margin-top:-30px;">Select your skills and expertise</h3>
     <div class="row">
        <div class="col-lg-12 col-lg-offset-12;">
            <input type="search" id="container-search" value="" class="form-control" placeholder="Search..." style="margin-left:30px;width:550px;margin-top: -190px;">
        </div>
    </div>
   <!--  <hr/> -->
    <!-- <h1 align="center">POPUP 1</h1>  
    <p align="justify">This is first popup. Place your content here.</p> -->
    <div class="row" style="margin-top:-100px;margin-left:50px;">
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;    margin-top: -130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Development1
    </h4></a>
  </div>
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;margin-left:170px;margin-top:-130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Design2
    </h4></a>
  </div>
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;margin-left:340px;margin-top:-130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Design3
    </h4></a>
  </div>
  </div>
  <!-- *******************2nd row*************** -->

  <div class="row" style="margin-top:100px;margin-left:50px;">
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;    margin-top: -130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Development1
    </h4></a>
  </div>
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;margin-left:170px;margin-top:-130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Design2
    </h4></a>
  </div>
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;margin-left:340px;margin-top:-130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Design3
    </h4></a>
  </div>
  </div>
  <div class="row" style="margin-top:100px;margin-left:50px;">
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;    margin-top: -130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Development1
    </h4></a>
  </div>
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;margin-left:170px;margin-top:-130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Design2
    </h4></a>
  </div>
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;margin-left:340px;margin-top:-130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Design3
    </h4></a>
  </div>
  </div>
  <div class="row" style="margin-top:100px;margin-left:50px;">
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;    margin-top: -130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Development1
    </h4></a>
  </div>
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;margin-left:170px;margin-top:-130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Design2
    </h4></a>
  </div>
  <div class="col-lg-4" style="border:2px solid lightgray;width:170px;height:100px;margin-left:340px;margin-top:-130px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4 style="text-align:center;">Website Design3
    </h4></a>
  </div>
  </div>

  
  <div style="font-size:30px;">
 
    <a class="close" href=""> x </a>
</div>
</div>
<!-- <div id="popup2" class="popup">
    <h3><a id="pop1" href=""> Back to category</a></h3>
    <hr />
    <h1 align="center">POPUP 2</h1> 
    <p align="justify">This is second popup. Place your content here.</p>
    <a class="close" href="">[ x ]</a>    
</div> -->
<div id="popup2" class="popup">
    <!-- <h3><a id="pop1" href=""> Back to category</a></h3> -->
    <h3 style="margin-left:40px;font-size:20px;margin-top:-30px;">Select your skills and expertise</h3>
     <div class="row">
        <div class="col-lg-12 col-lg-offset-12;">
            <input type="search" id="container-search" value="" class="form-control" placeholder="Search..." style="margin-left:30px;width:550px;margin-top: -190px;">
        </div>
    </div>
    
   <!--  <div class="row">
      <div class="col-md-12">
           <a id="pop1" href=""> <input type="submit" class="btn btn-info " value="Back To Categories"></a>
      </div>

    </div> -->
     <div class="row">
      <div class="col-md-12">

      </div>
    </div>

   <div class="row" >
    <div class="col-sm-6" style="margin-top:-520px;margin-left:20px;">
        <div class="container">
  
  <form>
    <label class="checkbox-inline" style="font-size:20px;">
      <input type="checkbox" value="" style="width:15px;height:15px;">Option 1
    </label>
    <label class="checkbox-inline" style="font-size:20px;">
      <input type="checkbox" value=""style="width:15px;height:15px;">Option 2
    </label>
    <label class="checkbox-inline"style="font-size:20px;">
      <input type="checkbox" value=""style="width:15px;height:15px;">Option 3
    </label>
   
  </form>
</div>
</div>
</div>

  <div class="col-sm-6" style="margin-top:-400px;">
    <div class="container">
  
  <div class="well well-sm">Small Well</div>
 <hr>
</div>

  </div>
  <!-- ************************end row************ -->
   <div class="row" >
    <div class="col-sm-6" style="margin-top:-690px;margin-left:20px;">
        <div class="container">
  
  <form>
    <label class="checkbox-inline" style="font-size:20px;">
      <input type="checkbox" value="" style="width:15px;height:15px;">Option 1
    </label>
    <label class="checkbox-inline" style="font-size:20px;">
      <input type="checkbox" value=""style="width:15px;height:15px;">Option 2
    </label>
    <label class="checkbox-inline"style="font-size:20px;">
      <input type="checkbox" value=""style="width:15px;height:15px;">Option 3
    </label>
   
  </form>
</div>
</div>
</div>
 <div class="row" >
    <div class="col-sm-6" style="margin-top:-860px;margin-left:20px;">
        <div class="container">
  
  <form>
    <label class="checkbox-inline" style="font-size:20px;">
      <input type="checkbox" value="" style="width:15px;height:15px;">Option 1
    </label>
    <label class="checkbox-inline" style="font-size:20px;">
      <input type="checkbox" value=""style="width:15px;height:15px;">Option 2
    </label>
    <label class="checkbox-inline"style="font-size:20px;">
      <input type="checkbox" value=""style="width:15px;height:15px;">Option 3
    </label>
   
  </form>
</div>
</div>
</div>
<div style="font-size:30px;">
   <a class="close" href=""> x </a>
</div>
</div>
</div>
  <!-- ****************************** -->
    
  
     <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">Skills:</strong></h3></label>
        <input type="text" class="form-control" placeholder="e.g. Java,php,Sql" id="skills"><br>
    </div>
    </div>
    
       <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">PAN Card/Adaar Card No:</strong></h3></label>
        <input type="text" class="form-control" placeholder="e.g. Java,php,Sql" id="skills"><br>
    </div>
    </div>
    
    
    
     <div class="form-group">
    <div class="col-xs-7">
      <label for="discription"><h3><strong style="font-size:18px;">About You:</strong></h3></label>
      <div class="form-group">
      <textarea class="form-control" rows="5" id="comment" placeholder="Describe about you here....."></textarea>
    </div>
    </div>
  
      <div class="form-group">
      <div class="col-xs-7">
      <span class="icon-attach"></span>
     
      <p><span class="glyphicon glyphicon-paperclip"></span><strong style="font-size:15px;">Attach Resume:</strong></p>
     <input type="text" id="path" placeholder="Select File" style="font-size:15px;" />
  <label class="add-photo-btn1">Browse
    <span>
       <input type="file" id="myfile" name="myfile" />
    </span>
    </label><br><br>
    </div>
    </div>
  
   <div class="form-group">
        <div class="col-xs-7">
      <button type="button" class="btn btn-primary btn-lg">Submit</button>
    </div>
    </div>
  </form>
</div>
    
    </div>
    
    
  </form>
</div>                     
                                
    
                        <div class="col-md-3">
                           
                            </div>
    
    </div>
   

    </body>
    </html>
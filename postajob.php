<?php require_once ("header.php");?>

<style>
.well{
  margin-top: -170px;
  width:50%;
  border: 1px solid lightgray;
  font-size:25px;
}
.table{
   margin-top: -332px;
  width:50%;
  border: 1px solid lightgray;
  color:blue;
  font-size: 21px;

}
.table > tbody > tr > td{
  color:black;
}


</style>





<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
  <div class="container">
    <div class="header-left grid">
      <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
        <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
      </div>
    </div>
    <div class="header-middle">
     
      <div class="search">
        <form action="#" method="post">
          <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
          <input type="submit" value=" ">
        </form>
      </div>
    </div>
    <div class="header-right">
      
     
    </div>
  
  </div>
</div>
          </ul>
    </div>
   
  </div>
</div>

<div class="container">

  <div class="jumbotron">
    <h3 style="text-align:center;">Post a Job</h3>

  <form class="form-horizontal">
    <div class="form-group">
      <label class="control-label col-sm-3" for="name">Full Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="name" placeholder="Full Name">
      </div>
    </div>

     <div class="form-group">
      <label class="control-label col-sm-3" for="contact">Contact No.:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="contact" placeholder="Contact No.">
      </div>
    </div>

     <div class="form-group">
      <label class="control-label col-sm-3" for="sel1">City:</label>
       <div class="col-sm-9">
      <select class="form-control" id="sel1">
        <option value="default">Select City</option>
        <option>Jabalpur</option>
        <option>Bhopal</option>
        <option>Indor</option>
        <option>Pune</option>
      </select>
    </div>
  </div>

   <div class="form-group">
      <label class="control-label col-sm-3" for="sel2">Select Job:</label>
       <div class="col-sm-9">
     <label class="radio-inline" data-toggle="modal" data-target="#myModal"><input type="radio" name="optradio">Local Job</label>
     <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">For Local Job</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal">
    <div class="form-group">
      <label class="control-label col-sm-4" for="skill">Required Skill Set:</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="skill" placeholder="Required Skill Set">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-4" for="date">Date:</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="date" placeholder="Date">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-4" for="workarea">Work Area:</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="workarea" placeholder="Work Area">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-4" for="name">No Of People Required:</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="name" placeholder="No Of People Required">
      </div>
    </div>
  </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">POST</button>
      </div>
    </div>
  </div>
</div>
<!-- ***************** -->
<label class="radio-inline" data-toggle="modal" data-target="#myModal1"><input type="radio" name="optradio">Technical Job</label>
<!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">For Technical Job</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
    <div class="form-group">
      <label class="control-label col-sm-4" for="skill">Required Skill Set:</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="skill" placeholder="Required Skill Set">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-4" for="work">Work:</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="work" placeholder="Work">
      </div>
    </div>
    
       <div class="form-group">
     
      <label class="control-label col-sm-4" for="sel1">Work Duration:</label>
      <div class="col-sm-8">
        <select class="form-control" id="sel1">
        <option value="default">Work Duration</option>  
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
      </div>
    </div>
     <div class="form-group">
     
      <label class="control-label col-sm-4" for="sel2">Budget:</label>
      <div class="col-sm-8">
        <select class="form-control" id="sel2">
        <option value="default">Budget</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
      </div>
    </div>

      <div class="form-group">
     
      <label class="control-label col-sm-4" for="sel3">Priority:</label>
      <div class="col-sm-8">
        <select class="form-control" id="sel3">
        <option value="default">Priority</option>  
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
      </div>
    </div>
  </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">POST</button>
      </div>
    </div>
  </div>
</div>
<!-- ****************************** -->
    </div>
  </div>

  <!-- <div class="form-group">
      <label class="control-label col-sm-3" for="sel2">Area:</label>
       <div class="col-sm-9">
      <select class="form-control" id="sel3">
        <option value="default">Area</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
    </div>
  </div>

  <div class="form-group">
      <label class="control-label col-sm-3" for="sel2">Payment Duration Wise:</label>
       <div class="col-sm-9">
      <select class="form-control" id="sel1">
        <option value="default">Payment Duration Wise</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
    </div>
  </div>

  <div class="form-group">
  <label  class="control-label col-sm-3" for="address">Address:</label>
  <div class="col-sm-9">
  <textarea class="form-control" rows="5" id="address"></textarea>
</div>
</div>

<div class="form-group">
  <label  class="control-label col-sm-3" for="discription">Job Discription:</label>
  <div class="col-sm-9">
  <textarea class="form-control" rows="5" id="discription"></textarea>
</div>
</div>
   
    <div class="form-group">
       <div class=" col-lg-8"></div>
      <div class=" col-lg-4">
      <a href="postjob.php">  <button type="button" class="btn btn-info">Edit</button></a>
   <a href="summary.php"> <button type="button" class="btn btn-info">Post</button></a>
  </div>
</div> -->
  </form>
</div>
</div>



</body>
</html>
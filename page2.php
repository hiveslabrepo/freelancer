<?php require_once ("header.php");?>
<style>
.row{
	margin-top: 20px;
}
ul > li {
    opacity: 1.0;
}

ul:hover > li:not(:hover) {
    opacity: 0.5;
}

</style>
<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
    <div class="container">
        <div class="header-left grid">
            <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
                <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
            </div>
        </div>
       </div>
     </div>
     <div class="container">
     <div class="row">
     	<!-- <div class="col-md-4"></div> -->
     	<div class="col-md-6">
     		<h1>Select your skills</h1>
     		<p>Select Upto 10 Skills</p>

     	</div>
     	<div class="col-md-6"></div>
     </div>
 </div>
     	

     <div class="row">
     	<div class="col-md-1"></div>
     	<div class="col-md-5">
     		
     <div class="container"style="width:130%;background-color:lightgray;border:1px solid lightgray;">
     	 <div class="col-lg-12 col-lg-offset-12;">
            <input type="search" id="container-search" value="" class="form-control" placeholder="Search..." style="margin-left:30px;width:550px;margin-top: 20px;border:1px solid gray;">
        </div>
       

  	<div class="col-md-6">
  	  <ul class="list-group" style="margin-top:20px;width:300px;margin-left:10px;">
    <li class="list-group-item" data-toggle="modal" data-target="#myModal1"><img src="images/1.png" alt="1.png"> Graphics & design</li>
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Graphics & design</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-md-6">
        		<ul class="list-group" style="width:105%;">
  <li class="list-group-item">Logo Design</li>
  <li class="list-group-item">Social Media design</li>
  <li class="list-group-item">3D & 2D Models</li>
</ul>

        	</div>
        	<div class="col-md-6">
        		<ul class="list-group" style="width:105%;margin-left:-17px;">
  <li class="list-group-item">T-Shirts</li>
  <li class="list-group-item">Second item</li>
  <li class="list-group-item">Third item</li>
</ul>
        	</div>



        </div>
        <!-- *******************row1*********** -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <!--  <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- *************************modal1****************** -->
    <li class="list-group-item" data-toggle="modal" data-target="#myModal2" ><img src="images/3.png" alt="3.png">mobile Phone</li>
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">mobile Phone</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;">
  <li class="list-group-item">Logo Design</li>
  <li class="list-group-item">Social Media design</li>
  <li class="list-group-item">3D & 2D Models</li>
</ul>

          </div>
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;margin-left:-17px;">
  <li class="list-group-item">T-Shirts</li>
  <li class="list-group-item">Second item</li>
  <li class="list-group-item">Third item</li>
</ul>
          </div>



        </div>
        <!-- *******************row1*********** -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <!--  <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- *************************************modal2************************* -->
    <li class="list-group-item" data-toggle="modal" data-target="#myModal3"><img src="images/4.png" alt="4.png">Designer</li>
    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Designer</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;">
  <li class="list-group-item">Logo Design</li>
  <li class="list-group-item">Social Media design</li>
  <li class="list-group-item">3D & 2D Models</li>
</ul>

          </div>
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;margin-left:-17px;">
  <li class="list-group-item">T-Shirts</li>
  <li class="list-group-item">Second item</li>
  <li class="list-group-item">Third item</li>
</ul>
          </div>



        </div>
        <!-- *******************row1*********** -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <!--  <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- ***************************modal3****************** -->
    <li class="list-group-item"data-toggle="modal" data-target="#myModal4"><img src="images/5.png" alt="5.png">Writing</li>
    <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">mobile Phone</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;">
  <li class="list-group-item">Logo Design</li>
  <li class="list-group-item">Social Media design</li>
  <li class="list-group-item">3D & 2D Models</li>
</ul>

          </div>
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;margin-left:-17px;">
  <li class="list-group-item">T-Shirts</li>
  <li class="list-group-item">Second item</li>
  <li class="list-group-item">Third item</li>
</ul>
          </div>



        </div>
        <!-- *******************row1*********** -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <!--  <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

   <!--  *************************************modal4****************** -->
  </ul>
    </div>
    <div class="col-md-6">
  	  <ul class="list-group" style="margin-top:20px;width:300px;margin-left:-35px;">
    <li class="list-group-item"data-toggle="modal" data-target="#myModal5"><img src="images/6.png" alt="1.png">Data Entry</li>
    <div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">mobile Phone</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;">
  <li class="list-group-item">Logo Design</li>
  <li class="list-group-item">Social Media design</li>
  <li class="list-group-item">3D & 2D Models</li>
</ul>

          </div>
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;margin-left:-17px;">
  <li class="list-group-item">T-Shirts</li>
  <li class="list-group-item">Second item</li>
  <li class="list-group-item">Third item</li>
</ul>
          </div>



        </div>
        <!-- *******************row1*********** -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <!--  <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
    <!--  *************************************modal5****************** -->
    <li class="list-group-item"data-toggle="modal" data-target="#myModal6"><img src="images/7.png" alt="1.png">Soft Development&design</li>
    <div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">mobile Phone</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;">
  <li class="list-group-item">Logo Design</li>
  <li class="list-group-item">Social Media design</li>
  <li class="list-group-item">3D & 2D Models</li>
</ul>

          </div>
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;margin-left:-17px;">
  <li class="list-group-item">T-Shirts</li>
  <li class="list-group-item">Second item</li>
  <li class="list-group-item">Third item</li>
</ul>
          </div>



        </div>
        <!-- *******************row1*********** -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <!--  <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
     <!-- *************************************modal6****************** -->
    <li class="list-group-item"data-toggle="modal" data-target="#myModal7"><img src="images/8.png" alt="1.png">IT&Networking</li>
    <div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">mobile Phone</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;">
  <li class="list-group-item">Logo Design</li>
  <li class="list-group-item">Social Media design</li>
  <li class="list-group-item">3D & 2D Models</li>
</ul>

          </div>
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;margin-left:-17px;">
  <li class="list-group-item">T-Shirts</li>
  <li class="list-group-item">Second item</li>
  <li class="list-group-item">Third item</li>
</ul>
          </div>



        </div>
        <!-- *******************row1*********** -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <!--  <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
     <!-- *************************************modal7****************** -->
    <li class="list-group-item"data-toggle="modal" data-target="#myModal8"><img src="images/9.png" alt="3.png">Data Science& Analytics</li>
    <div class="modal fade" id="myModal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">mobile Phone</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;">
  <li class="list-group-item">Logo Design</li>
  <li class="list-group-item">Social Media design</li>
  <li class="list-group-item">3D & 2D Models</li>
</ul>

          </div>
          <div class="col-md-6">
            <ul class="list-group" style="width:105%;margin-left:-17px;">
  <li class="list-group-item">T-Shirts</li>
  <li class="list-group-item">Second item</li>
  <li class="list-group-item">Third item</li>
</ul>
          </div>



        </div>
        <!-- *******************row1*********** -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <!--  <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

     <!-- *************************************modal8****************** -->
  </ul>
    </div>
  

 </div>
</div>
 <div class="col-md-5">
 	<div class="container">
 		<div class="jumbotron" style="width:30%;margin-left:15%;">
 			<ul class="list-group"style="width:120%;margin-left:-20%;">
    <li class="list-group-item">Logo Design&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">
          <span class="glyphicon glyphicon-remove" style="float:right;"></span>
        </a></li>
    <li class="list-group-item">Social Media Design&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">
          <span class="glyphicon glyphicon-remove" style="float:right;"></span>
        </a></li>
    <li class="list-group-item">3D & 2D Modfels&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">
          <span class="glyphicon glyphicon-remove"style="float:right;"></span>
        </a></li>
    <li class="list-group-item">T-Shirts&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">
          <span class="glyphicon glyphicon-remove"style="float:right;"></span>
        </a></li>
  </ul>

 		</div>

 	</div>

  </div>
 <div class="col-md-1"></div>

</div>
<div class="row">
  <div class="col-md-4"></div>
   <div class="col-md-4"></div>
    <div class="col-md-4">
<a href="workprofile.php"><input type="submit" class="btn btn-info" value="Go To Profile"></a>
</div>
</div>
<br><br>
<div class="container">
 
  
  <form>
    <div class="form-group">
      <label for="sel1"><h2>for local work:</h2></label>
      <select class="form-control" id="sel1">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
      </div>
  </form>
</div>


  </body>
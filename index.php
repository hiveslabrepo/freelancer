

<?php require_once ("header.php");?>

<style>
.banner{
    background: url("images/work.jpg") no-repeat center;
    background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    -ms-background-size: cover;
    -moz-background-size: cover;
    min-height: 700px;	
   

}

</style>
<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
	<?php require_once ("navigation.php");?>
	<div class="ban-bot text-center">
		<script src="js/responsiveslides.min.js"></script>
				<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						 // Slideshow 4
						$("#slider3").responsiveSlides({
							auto: true,
							pager: true,
							nav: false,
							speed: 500,
							namespace: "callbacks",
							before: function () {
						$('.events').append("<li>before event fired.</li>");
						},
						after: function () {
							$('.events').append("<li>after event fired.</li>");
							}
							});
						});
				</script>
		<div  id="top" class="callbacks_container">
			
			<div class="row">
				        <div class="col-md-3 col-sm-3" style="margin-top:-130px;">
                             <div class="service-item" style="margin-right: 50px;">
                                <!-- <img src="images/2.png"> -->
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"> </i>
                              <a href="post.php">  <i class="fa fa-cloud fa-stack-1x text-primary"><img src="images/2.png" class="img-circle" alt="Cinque Terre" width="200" height="200"></i></a>
                            </span>
                                <p>
                                    <strong style="font-style: normal;font-size:26px; background-color:white;">Hire Expert</strong>
                                </p>
                               <!--  <p>Hire Expert Freelancers To Finish Your Local <br>Or Technical Any Task</p> -->
                               
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3" style="margin-top:100px;">
                           
                        </div>
                        <!-- <div class="col-md-3"></div> -->
                        <div class="col-md-3 col-sm-3">
                           
                        </div>
                        <div class="col-md-3 col-sm-3" style="margin-top:-125px;">
                            <div class="service-item" style="margin-left: -70px;">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                               <a href="become_freelancer.php"> <i class="fa fa-compass fa-stack-1x text-primary"><img src="images/3.jpg" class="img-circle" alt="Cinque Terre" width="200" height="200"></i></a>
                            </span>
                            <!-- <img src="images/3.jpg"> -->
                                <p>
                                    <strong style="background-color:#0095B6;font-size:26px;">Get Hired </strong>
                                </p>
                                <!-- <p>Get Hired And Have Extra Income Source</p> -->
                                
                            </div>
                        </div>
                        
                        </div>

		</div>
		<div class="clearfix"></div>
	</div>
</div>

<!-- content-bottom -->

 <section id="services" class="services bg-primary">
        <div class="container">
            
    </section>

<!-- //content-bottom -->
<!-- content -->
<div class="content">
	<div class="container">
		<h3 class="title wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s" >Finish Your  <span>Technical Things</span> Like</h3>

		
		<div class="content-grids ">
			<!-- *******************1st************ -->
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal1">
						<img src="images/img1.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal1" style="font-size:20px;font-weight:bold;">Graphics & Design</p>
				</div>
			</div>
			<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Graphics & Design</h4>
      </div>
      <div class="modal-body">
       <ul class="list-group row">
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Logo Design</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Social Media Design</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Business Cards & stationery</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> cartoons & Caricatures</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Photoshop Editing</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Banner Ads</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> 3D & 2D Models</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> T-Shirts</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Flyers & Posters</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Book Covers & Packaging</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Web & Mobile Design</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Presentation design Invitations</a></li>
</ul>
<input type="submit" class="btn btn-info" data-dismiss="modal" value="Close">
      </div>
     
    </div>
  </div>
</div>
		<!-- *****************2nd************** -->
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal2">
						<img src="images/img2.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal2" style="font-size:20px;font-weight:bold;">Mobile Phone</p>
				</div>
			</div>
			<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Mobile Phone</h4>
      </div>
      <div class="modal-body">
       <ul class="list-group row">
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Mobile Phone</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> iphone</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Windows Phone</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Blackberry</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Palm</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Applewatch</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Amazon Fire</a></li>

</ul>
<input type="submit" class="btn btn-info" data-dismiss="modal" value="Close">
      </div>
   
    </div>
  </div>
</div>
<!-- ***********************3rd************* -->
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal3">
						<img src="images/img3.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal3" style="font-size:20px;font-weight:bold;">Designers</p>
				</div>
			</div>
			<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Designers</h4>
      </div>
      <div class="modal-body">
        <ul class="list-group row">
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Illustration</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Photo Editing</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Video Editing</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Video Production</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> 3D Design</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> T-Shirt</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Photoshop</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Banner</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Broucher</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Building Architecture</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Fashion</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> After Effects</a></li>
</ul>
<input type="submit" class="btn btn-info" data-dismiss="modal" value="Close">
      </div>
    
    </div>
  </div>
</div>
<!-- ************4th******************** -->
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr btm-clr3">
					<!-- <span></span> -->
					<figure class="icon" data-togglvcce="modal" data-toggle="modal" data-target="#myModal4" >
						<img src="images/img4.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal4" style="font-size:20px;font-weight:bold;">Writing</p>
				</div>
			</div>
			<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Writing</h4>
      </div>
      <div class="modal-body">
       <ul class="list-group row">
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Blog</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Travel</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> News</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Story</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Fiction</a></li>

</ul>
<input type="submit" class="btn btn-info" data-dismiss="modal" value="Close">
      </div>
     
    </div>
  </div>
</div>
			<div class="clearfix"></div>
		</div>
		<!-- ******************5th************** -->

		<div class="content-grids ">
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal5">
						<img src="images/img5.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal5" style="font-size:20px;font-weight:bold;">Data Entry</p>
				</div>
			</div>
			<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
      <ul class="list-group row">
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Logo Design</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Social Media Design</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Business Cards & stationery</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> cartoons & Caricatures</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Photoshop Editing</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Banner Ads</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> 3D & 2D Models</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> T-Shirts</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Flyers & Posters</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Book Covers & Packaging</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Web & Mobile Design</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Presentation design Invitations</a></li>
</ul>
<input type="submit" class="btn btn-info" data-dismiss="modal" value="Close">
      </div>
     
    </div>
  </div>
</div>
<!-- ***********************6th************** -->
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal6">
						<img src="images/img17.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal6" style="font-size:20px;font-weight:bold;">Web,Mobile & Soft Development & Design</p>
				</div>
			</div>
			<div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Web,Mobile & Soft Development & Design</h4>
      </div>
      <div class="modal-body">
         <ul class="list-group row">
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Desktop Software Development</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Ecommerce Development</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Game Development</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Mobile Development</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Mobile App Design</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Product Management</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Q.A. & Testing</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Script & Utilities</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Web Development</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Web & Mobile Development </a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png">Other Software Development</a></li>
 
</ul>
<input type="submit" class="btn btn-info" data-dismiss="modal" value="Close">
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
<!-- ***************7th*************** -->
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal7">
						<img src="images/img18.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal7" style="font-size:20px;font-weight:bold;">IT & Networking </p>
				</div>
			</div>
			<div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">IT & Networking</h4>
      </div>
      <div class="modal-body">
        <ul class="list-group row">
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png">DataBase Administration</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png">ERP/CRM Software</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png">Information Security</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png">Network & System Administration</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Other IT & Networking </a></li>

 
</ul>
<input type="submit" class="btn btn-info" data-dismiss="modal" value="Close">
      </div>
      
    </div>
  </div>
</div>
<!-- ***********8th************ -->
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr btm-clr3">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal8">
						<img src="images/img8.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal8" style="font-size:20px;font-weight:bold;">Data Science & Analytics</p>
				</div>
			</div>
			<div class="modal fade" id="myModal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Data Science & Analytics</h4>
      </div>
      <div class="modal-body">
         <ul class="list-group row">
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png">A/B Testing</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png">Data Visualization</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png">Data Extraction/ETL</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png">Data Mining & Management</a></li>
     <li class="list-group-item col-xs-6"><a href="jobavailabilty.php" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Machine Learning </a></li>
          <li class="list-group-item col-xs-6"><a href="#" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Quantitative Analysis </a></li>
               <li class="list-group-item col-xs-6"><a href="#" style="text-decoration:none;font-weight:bold;color:black"><img src="images/l1.png"> Other </a></li>

 
</ul>
<input type="submit" class="btn btn-info" data-dismiss="modal" value="Close">
      </div>
    
    </div>
  </div>
</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- *******************Content2************* -->
<div class="content">
	<div class="container">
		<h3 class="title wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s" >Finish Your  <span>Local Things</span> Like</h3>
		
		<div class="content-grids ">
			<!-- ****************9th**************** -->
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal9">
						<img src="images/img9.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal9" style="font-size:20px;font-weight:bold;">Shifting</p>
				</div>
			</div>
			<div class="modal fade" id="myModal9" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- *****************10th********** -->
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal10">
						<img src="images/img10.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal10" style="font-size:20px;font-weight:bold;">Driving</p>
				</div>
			</div>
			<div class="modal fade" id="myModal10" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- ****************11th************ -->
			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal11">
						<img src="images/img11.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal11" style="font-size:20px;font-weight:bold;">Cook </p>
				</div>
			</div>
			<div class="modal fade" id="myModal11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- ***************12th************** -->
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr btm-clr3">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal12">
						<img src="images/img12.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal12" style="font-size:20px;font-weight:bold;">Worker</p>
				</div>

			</div>
			<div class="modal fade" id="myModal12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- *****************13th*********** -->
			<div class="clearfix"></div>
		</div>

		<div class="content-grids ">

			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal13">
						<img src="images/img13.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal13" style="font-size:20px;font-weight:bold;">Garage</p>
				</div>
			</div>
			<div class="modal fade" id="myModal13" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- *****************14th********** -->
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal14">
						<img src="images/img14.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal14" style="font-size:20px;font-weight:bold;">Tailor</p>
				</div>
			</div>
			<div class="modal fade" id="myModal14" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- *****************15th************** -->

			<div class="col-md-3 wel-grid text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr4 btm-clr1">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal15">
						<img src="images/img15.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal15" style="font-size:20px;font-weight:bold;">Beauty Parlour </p>
				</div>
			</div>
			<div class="modal fade" id="myModal15" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- ********************16th**************** -->
			
			<div class="col-md-3 wel-grid btm-gre text-center wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<div class="btm-clr btm-clr3">
					<!-- <span></span> -->
					<figure class="icon" data-toggle="modal" data-target="#myModal16">
						<img src="images/img16.png" alt=" " />
					</figure>
					<p data-toggle="modal" data-target="#myModal16" style="font-size:20px;font-weight:bold;">Electrician</p>
				</div>
			</div>
			<div class="modal fade" id="myModal16" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //content -->
<!-- content-bottom -->


 

<!-- our pets -->
<div class="our_pets">
	<div class="container">
		<h3 class="title wow fadeInUp animated" data-wow-delay=".5s" >Our <span> Pets</span></h3>
		<div class="flex-slider wow fadeInDown animated" data-wow-delay=".5s">

			<ul id="flexiselDemo1">			
				<li>
					<div class="laptop">
						<div class="pets-effect ver_line zoom">
							<div class="img-box"><img class="img-responsive zoom-img" src="images/pic10.jpg" alt=" " /></div>
							<div class="pets-info">
								<div class="pets-info-slid">
									<h4>Pets Love</h4>
									<span class="strip_line"></span>
									<p>Sit accusamus, vel blanditiis iure minima ipsa molestias minus laborum velit, nulla nisi hic quasi enim.</p>
									<span class="strip_line"></span>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="laptop">
						<div class="pets-effect ver_line zoom">
							<div class="img-box"><img class="img-responsive zoom-img" src="images/pic11.jpg" alt=" " /></div>
							<div class="pets-info">
								<div class="pets-info-slid">
									<h4>Pets Love</h4>
									<span class="strip_line"></span>
									<p>Sit accusamus, vel blanditiis iure minima ipsa molestias minus laborum velit, nulla nisi hic quasi enim.</p>
									<span class="strip_line"></span>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="laptop">
						<div class="pets-effect ver_line zoom">
							<div class="img-box"><img class="img-responsive zoom-img" src="images/pic12.jpg" alt=" " /></div>
							<div class="pets-info">
								<div class="pets-info-slid">
									<h4>Pets Love</h4>
									<span class="strip_line"></span>
									<p>Sit accusamus, vel blanditiis iure minima ipsa molestias minus laborum velit, nulla nisi hic quasi enim.</p>
									<span class="strip_line"></span>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="laptop">
						<div class="pets-effect ver_line zoom">
							<div class="img-box"><img class="img-responsive zoom-img" src="images/pic13.jpg" alt=" " /></div>
							<div class="pets-info">
								<div class="pets-info-slid">
									<h4>Pets Love</h4>
									<span class="strip_line"></span>
									<p>Sit accusamus, vel blanditiis iure minima ipsa molestias minus laborum velit, nulla nisi hic quasi enim.</p>
									<span class="strip_line"></span>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
			<script type="text/javascript">
							$(window).load(function() {
								$("#flexiselDemo1").flexisel({
									visibleItems: 3,
									animationSpeed: 1000,
									autoPlay: true,
									autoPlaySpeed: 3000,    		
									pauseOnHover: true,
									enableResponsiveBreakpoints: true,
									responsiveBreakpoints: { 
										portrait: { 
											changePoint:480,
											visibleItems: 1
										}, 
										landscape: { 
											changePoint:640,
											visibleItems: 2
										},
										tablet: { 
											changePoint:991,
											visibleItems: 2
										}
									}
								});
								
							});
					</script>
					<script type="text/javascript" src="js/jquery.flexisel.js"></script>
		</div>
	</div>
</div>
<!-- //our pets -->
<!-- our works -->
<div class="treatments">
	<div class="container">
		<h3 class="title wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s" >Our <span> Services</span></h3>
		<div class="col-md-6 treat-left">
		
		
		</div>
		<div class="col-md-6 treat-right wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0s">
			<div class="treat-icon">
			<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
			<p style="font-size:20px;font-weight:bold;">GET HIRED</p>
		<!-- 	<p> </p> -->
			</div>
		</div>
		<div class="col-md-6 treat-right2 text-right wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0.1s">
			<div class="treat-icon1">
			<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
			<!-- <h4>HIRE EXPERT</h4> -->
			<p>Get Hired means people get work such as technical and non-technical according to their skills. </p>
			</div>
		</div>
		<div class="col-md-6 treat-left2">
    
		
		
		</div>
		<div class="col-md-6 treat-left">
		
		
		</div>
		<div class="col-md-6 treat-right wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0.2s">
			<div class="treat-icon">
			<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
			<p style="font-size:20px;font-weight:bold;">HIRE EXPERT</p>
			<!-- <p> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, quisquam est, qui dolorem ipsum quia dolor sit amet</p> -->
			</div>
		</div>
		<div class="col-md-6 treat-right2 text-right no-bor wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0.3s">
			<div class="treat-icon1">
			<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
			 
			<p> Hire Expert means people post the work such as technical and non-technical and hire the expert by their skills and handover the work to expert on define cost and time.  </p>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<!-- //our works -->
<!-- ****************List******************* -->
<!-- contact -->
<div class="contact-form ">
		<div class="container">
			<h3 class="title">Contact <span>Us</span></h3>
			
			<div class="col-md-6 contact-right wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">				
				<form action="#" method="post">
					<input type="text" name="Name" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required="">
					<input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
					<input type="text" name="Telephone" value="Telephone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Telephone';}" required="">
					<textarea name="Message..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message...';}" required="">Message...</textarea>
					<input type="submit" value="Submit" >
				</form>
			</div>
			<div class="col-md-6 contact-left wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
				<h2>Contact Information</h2>
				<!-- <p>"Lorem Ipsum"is the common name dummy text often used in the design, printing, and type setting industriescommon name dummy text often used in the design, printing, and type setting industries Lorem Ipsum"is the common name dummy text often used in the design, printing, and type setting industries "</p> -->
				<ul class="contact-list">
					<li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>01, Giridhar Society, Balewadi Fata, Baner, 
Pune, 411045</li>
					<li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:example@mail.com">contact@hiveslab.com</a></li>
					<li><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>+91-81499 88450</li>
				</ul>
				<ul class="icons-list footer-bottom">
					<li><a href="#" class="use1"><span>Facebook</span></a></li>
					<li><a href="#" class="use2"><span>Twitter</span></a></li>
					<li><a href="#" class="use3"><span>Dribbble</span></a></li>
					<li><a href="#" class="use4"><span>Pinterest</span></a></li>
				</ul>
			</div>						
			<div class="clearfix"> </div>
			<p class="copy-right text-center">&copy; 2016 Hives Online India pvt.ltd. All rights reserved | Design by <a href="http://hiveslab.com//">HivesLab</a></p>
		</div>			
</div>

<!-- contact -->
	<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
							
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->

</body>
</html>


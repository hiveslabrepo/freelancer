<?php require_once ("header.php");?>
<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
    <div class="container">
        <div class="header-left grid">
            <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
                <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
            </div>
        </div>
        <div class="header-middle">
			<!-- <ul>
				<li><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>+123 456 7890</li>
				<li><a href="mailto:info@example.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>info@example.com</a></li>
			</ul> -->
			<div class="search">
				<form action="#" method="post">
					<input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
					<input type="submit" value=" ">
				</form>
			</div>
			<!-- <button type="button" class="btn btn-success">Success</button> -->
		</div>
	</div>
     </div>
    
		<!-- <div class="container">
			<div class="well well-md"style="margin-top:5px;width:250px;margin-left:1000px;">

			</div>

		</div> -->
		<div class="row" style="margin-top:-1px;">
			<div class="col-md-12">
		
			<nav class="navbar navbar-default"style="background-color:#4997D0 ;width:100%;">
  <div class="container-fluid">
    <!-- <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div> -->
    <ul class="nav navbar-nav">
      <li class="active"><a href="#" style="color:white;font-size:18px;">My Projects</a></li>
      <li><a href="newsfeed.php"style="color:white;font-size:18px;">DashBoard</a></li>
      <li><a href="#"style="color:white;font-size:18px;">Index</a></li> 
      <li><a href="#"style="color:white;font-size:18px;">Feedback</a></li> 
    </ul>
  </div>
</nav>
</div>
</div>
<!-- <div class="row" style="margin-top:-20px;">
	
	<div class="col-md-6">
<div class="container">
<div class="jumbotron"style="width:80%;height:auto;">
<h2>NewsFeed</h2>
<hr>
<div class="col-md-1">
	<span class="glyphicon glyphicon-paperclip"></span>
	</div>
	<div class="col-md-5">
		<p>10 new articles published on Freelancer Community
8 hours ago
Freelancer Community is the place for you to hang out between projects, learn top tips to improve your career, share your freelancing experiences, and learn from your peers.
 Read articles and learn something new</p>
	</div>
</div>
</div>
</div>
<div class="col-md-6"></div>
</div> -->
<div class="row">
	<div class="col-md-1"></div>
	
	<div class="col-md-4">
<div class="container" style="width:700px;margin-top:-46%;">
		<div class="jumbotron">
			<h2>NewsFeed</h2>
			<hr>
			<div class="row" style="margin-top:-1%;">
				<div class="col-md-1">
                    <span class="glyphicon glyphicon-paperclip" style="font-size:25px;"></span>
				</div>
				<div class="col-md-10">
					<h3 style="font-size:20px;">10 new articles published on <a href="#">Freelancer Community</a></h3>
						<p style="font-size:13px;font-weight:bold;">Freelancer Community is the place for you to hang out between projects, learn top tips to improve your career, share your freelancing experiences, and learn from your peers.
 Read articles and learn something new.</p>

				</div>
				<div class="col-md-1">
                    <a href="#">
          <span class="glyphicon glyphicon-remove"></span>
        </a>
				</div>
				

			</div>
			<hr>
			<!-- ********************************row1 end******************* -->
			<div class="row"style="margin-top:-1%;">
				<div class="col-md-1">
                    <span class="glyphicon glyphicon-paperclip" style="font-size:25px;"></span>
				</div>
				<div class="col-md-10">
					<h3 style="font-size:20px;">10 new articles published on <a href="#">Freelancer Community</a></h3>
						<p style="font-size:13px;font-weight:bold;">Freelancer Community is the place for you to hang out between projects, learn top tips to improve your career, share your freelancing experiences, and learn from your peers.
 Read articles and learn something new.</p>

				</div>
				<div class="col-md-1">
                    <a href="#">
          <span class="glyphicon glyphicon-remove"></span>
        </a>
				</div>
				
				
			</div>
			<hr>
			<!-- *****************************row2 end****************** -->
			<div class="row"style="margin-top:-1%;">
				<div class="col-md-1">
                    <span class="glyphicon glyphicon-paperclip" style="font-size:25px;"></span>
				</div>
				<div class="col-md-10">
					<h3 style="font-size:20px;">10 new articles published on <a href="#">Freelancer Community</a></h3>
						<p style="font-size:13px;font-weight:bold;">Freelancer Community is the place for you to hang out between projects, learn top tips to improve your career, share your freelancing experiences, and learn from your peers.
 Read articles and learn something new.</p>

				</div>
				<div class="col-md-1">
                    <a href="#">
          <span class="glyphicon glyphicon-remove"></span>
        </a>
				</div>
				
				
			</div>
			<hr>
			<!-- *****************************row3 end*********************** -->
			<div class="row"style="margin-top:-1%;">
				<div class="col-md-1">
                    <span class="glyphicon glyphicon-paperclip" style="font-size:25px;"></span>
				</div>
				<div class="col-md-10">
					<h3 style="font-size:15px;">10 new articles published on <a href="#">Freelancer Community</a></h3>
						<p style="font-size:13px;font-weight:bold;">Freelancer Community is the place for you to hang out between projects, learn top tips to improve your career, share your freelancing experiences, and learn from your peers.
 Read articles and learn something new.</p>

				</div>
				<div class="col-md-1">
                    <a href="#">
          <span class="glyphicon glyphicon-remove"></span>
        </a>
				</div>
				
				
			</div>
			<hr>
			<!-- *********************************row4 end********************* -->


		

     </div>
	</div>

</div>
<div class="col-md-5">
	<div class="container" style="margin-top:-36%;width:90%;margin-left:230px;">
		<div class="jumbotron"style="height:200px;">
			
				<a href="#"><img src="images/blank_img.jpg" alt="images" style="border:1px solid gray;"></a>

				<a href="#"><h4 style="margin-left:110px;margin-top:-90px;">Shashi Bahir</h4></a>
<br>
			
<div class="progress" style="margin-top:70px;">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%;">
      40% Complete (success)
    </div>
  </div>
		</div>

	</div>

</div>



<div class="col-md-2"></div>
</div>

		
     </body>
     </html>
<?php require_once ("header.php");?>
<head>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  background: #F1F3FA;
}

/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}
 .rating span {
  font-size: 25px;
  cursor: pointer;
  float: right; //remove 'float right' for right to left
}
.rating span:hover, .rating span:hover ~ span{
  color: red; //gold or any other color 
}
td:hover {
          /*background-color: #ffff99;*/
          -webkit-box-shadow: 0px 2px 4px #e06547;
    -moz-box-shadow: 0px 2px 4px #e06547;
    box-shadow: 0px 2px 4px #e06547;
    background-color: #d4d5d6;
        }

        .dropdown {
    display:inline-block;
    margin-left:20px;
    padding:10px;
  }
   .animated {
    -webkit-transition: height 0.2s;
    -moz-transition: height 0.2s;
    transition: height 0.2s;
}

.stars
{
    margin: 20px 0;
    font-size: 24px;
    color: #d17581;
}
.well {
  width:50%;
}
</style>
 <script>
  $('.rating span').click(function(){
    alert($(this).attr('id'));
})

</script>  


</head>
<body>

     <?php require_once ("navigation1.php");?>
    
		
		
<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="images/blank_img.jpg" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name" style="background-color:#FFF44F;">
						Kalpesh Vasaikar
					</div>
					
				</div>
        <hr>
        <h4 style="text-align:center;">portfolio</h4><br>
        <ul class="nav">
        <li>
              <a href="workprofile.php">
         
        Project- SellOneG
        </a></li>

        <li>
              <a href="workprofile.php">
     
       Project- Apartment
        </a></li>
         
            <li>
              <a href="#">
          
        
                Project- Natural Language Processing </a>
            </li>
            <li>
              <a href="#" target="_blank">
             
             Project- Freelancer </a>
            </li>
            <li>
              <a href="index.php">
            
              Project- Mittre Bitter Monitoring System </a>
            </li>
          </ul>
			
				
				
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">

              <h3>Skills</h3><br>
              <table class="table table-bordered" style="width:95%;">
   
    <tbody>
      <tr >
        <td style="font-size:15px;color:black;font-weight:bold;">HTML5</td>
        <td style="font-size:15px;color:black;font-weight:bold;">CSS3</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Bootstrap</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Java Script</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Jquery</td>
      </tr>
      <tr>
        <td style="font-size:15px;color:black;font-weight:bold;">PHP</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Hadoop</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Java</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Adroid</td>
        <td style="font-size:15px;color:black;font-weight:bold;">SQL</td>
      </tr>
     
    </tbody>
  </table>
  <br>
  <p><strong style="font-size:15px;">Place - </strong> New Sangvi Pune Maharatra</p><br>
  <p><strong style="font-size:15px;">Discription - </strong>
 
Text in a pre element
is displayed in a fixed-width
font, and it preserves
both      spaces and
line breaks.</p><p>Removes the default list-style and left margin on list items This class only applies to immediate children list items (to remove the default list-style from any nested lists, apply this class to any nested lists as well)
</p>
<br>
<p><strong style="font-size:15px;">Price - </strong> 1000 &#x20A8;</p><br>
<div class="container">
<div class="well well-sm">
            <div class="text-right">
                <a class="btn btn-success btn-green" href="#reviews-anchor" id="open-review-box">Leave a Review</a>
            </div>
        
            <div id="post-review-box" style="display:none;margin-top:20px;">
               
                    <form accept-charset="UTF-8" action="" method="post">
                       <input type="text" class="form-control" id="usr" placeholder="Enter your Name..."><br>
                        <input id="ratings-hidden" name="rating" type="hidden"> 
                        <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5"></textarea>
        
                        <div class="text-right">
                            <div class="stars starrr" data-rating="0"></div>
                            <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
                            <span class="glyphicon glyphicon-remove"></span>Cancel</a>
                            <button class="btn btn-success btn-lg" type="submit">Save</button>
                        </div>
                    </form>
               
            </div>
        </div> 
         
   
</div>
<!-- **************************End Review*************** -->
<div class="container">
  <div class="well" style="margin-top:20px;width:55%;height:150px;">
      <div class="media">
        <a class="pull-left" href="postedprofile.php">
        <img class="media-object" src="images/blank_img.jpg">
      </a>
      <div class="media-body">
        <h4 class="media-heading">Rakesh Mahajan</h4>
        <p>hello!Text in a pre element is displayed in a fixed-width font, and it preserves both spaces and line breaks.
Removes the default list-style and left margin on list items This class only applies to immediate children</p><br>
         <ul class="list-inline list-unstyled">
          
        <li><span><i class="glyphicon glyphicon-calendar" style="font-size:15px;color:black;"></i><strong style="font-size:17px;"> in 10days</strong> </span></li>
            <li>|</li>
            <!-- <span style="font-size:17px;"><strong style="font-size:17px;">500</strong> <i class="fa fa-inr" style="font-size:15px;"></i></span>
            <li>|</li> -->
            <li>
             <div id="stars-existing" class="starrr" data-rating='4' style="margin-top:-200px;font-size:15px;"></div>
            </li>
           
      </ul>
       </div>
    </div>
  </div>
          

    </div>
    <!-- ************************ above show review 1**************** -->
    <div class="container">
  <div class="well" style="margin-top:20px;width:55%;height:150px;">
      <div class="media">
        <a class="pull-left" href="postedprofile.php">
        <img class="media-object" src="images/blank_img.jpg">
      </a>
      <div class="media-body">
        <h4 class="media-heading">Rakesh Mahajan</h4>
        <p>hello!Text in a pre element is displayed in a fixed-width font, and it preserves both spaces and line breaks.
Removes the default list-style and left margin on list items This class only applies to immediate children</p><br>
         <ul class="list-inline list-unstyled">
          
        <li><span><i class="glyphicon glyphicon-calendar" style="font-size:15px;color:black;"></i><strong style="font-size:17px;"> in 10days</strong> </span></li>
            <li>|</li>
            <!-- <span style="font-size:17px;"><strong style="font-size:17px;">500</strong> <i class="fa fa-inr" style="font-size:15px;"></i></span>
            <li>|</li> -->
            <li>
             <div id="stars-existing" class="starrr" data-rating='4' style="margin-top:-200px;font-size:15px;"></div>
            </li>
           
      </ul>
       </div>
    </div>
  </div>
          

    </div>
  <!--   ***************************above show reviwe2*************** -->
  <div class="container">
  <div class="well" style="margin-top:20px;width:55%;height:150px;">
      <div class="media">
        <a class="pull-left" href="postedprofile.php">
        <img class="media-object" src="images/blank_img.jpg">
      </a>
      <div class="media-body">
        <h4 class="media-heading">Rakesh Mahajan</h4>
        <p>hello!Text in a pre element is displayed in a fixed-width font, and it preserves both spaces and line breaks.
Removes the default list-style and left margin on list items This class only applies to immediate children</p><br>
         <ul class="list-inline list-unstyled">
          
        <li><span><i class="glyphicon glyphicon-calendar" style="font-size:15px;color:black;"></i><strong style="font-size:17px;"> in 10days</strong> </span></li>
            <li>|</li>
            <!-- <span style="font-size:17px;"><strong style="font-size:17px;">500</strong> <i class="fa fa-inr" style="font-size:15px;"></i></span>
            <li>|</li> -->
            <li>
             <div id="stars-existing" class="starrr" data-rating='4' style="margin-top:-200px;font-size:15px;"></div>
            </li>
           
      </ul>
       </div>
    </div>
  </div>
          

    </div>

       <!--  ******************* above show review3**************** -->
       
           <a href="term.php"> <input type="submit" class="btn btn-info" value="Hire"></a>
        
      </div>
      
             </div>
             <script>

             (function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);

var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:5,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if(e&&e<5){for(t=r=e;e<=4?r<=4:r>=4;t=e<=4?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if(!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})

$(function(){

  $('#new-review').autosize({append: "\n"});

  var reviewBox = $('#post-review-box');
  var newReview = $('#new-review');
  var openReviewBtn = $('#open-review-box');
  var closeReviewBtn = $('#close-review-box');
  var ratingsField = $('#ratings-hidden');

  openReviewBtn.click(function(e)
  {
    reviewBox.slideDown(400, function()
      {
        $('#new-review').trigger('autosize.resize');
        newReview.focus();
      });
    openReviewBtn.fadeOut(100);
    closeReviewBtn.show();
  });

  closeReviewBtn.click(function(e)
  {
    e.preventDefault();
    reviewBox.slideUp(300, function()
      {
        newReview.focus();
        openReviewBtn.fadeIn(200);
      });
    closeReviewBtn.hide();
    
  });

  $('.starrr').on('starrr:change', function(e, value){
    ratingsField.val(value);
  });
});</script>
		
     </body>
     </html>
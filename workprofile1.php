<?php require_once ("header.php");?>
<head>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  background: #F1F3FA;
}

/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}
 .rating span {
  font-size: 25px;
  cursor: pointer;
  float: right; //remove 'float right' for right to left
}
.rating span:hover, .rating span:hover ~ span{
  color: red; //gold or any other color 
}
</style>
 <script>
  $('.rating span').click(function(){
    alert($(this).attr('id'));
})

</script>  


</head>
<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
    <div class="container">
        <div class="header-left grid">
            <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
                <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
            </div>
        </div>
        <div class="header-middle">
			
			<div class="search">
				<form action="#" method="post">
					<input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
					<input type="submit" value=" ">
				</form>
			</div>
			
		</div>
	</div>
     </div>
    
		
		<div class="row" style="margin-top:-1px;">
			<div class="col-md-12">
		
			<nav class="navbar navbar-default"style="background-color:#4997D0 ;width:100%;">
  <div class="container-fluid">
   
    <ul class="nav navbar-nav">
      <li class="active"><a href="workprofile.php" style="color:white;font-size:18px;">MY PROFILE</a></li>
      <li><a href="newsfeed.php"style="color:white;font-size:18px;">IMPROVE PROFILE</a></li>
      <!-- <li><a href="#"style="color:white;font-size:18px;">MY SERVICE</a></li> 
      <li><a href="#"style="color:white;font-size:18px;">GET CERTIFIED</a></li> --> 
       <li><a href="#"style="color:white;font-size:18px;">PROMOTE PROFILE</a></li>
       <li><a href="#"style="color:white;font-size:18px;">MY REWARDS</a></li>
    </ul>
  </div>
</nav>
</div>
</div>
<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="images/blank_img.jpg" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						Shashi Bahir
					</div>
					<div class="profile-usertitle-job">
						For Work
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<a href="#"><button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a>
					<!-- <button type="button" class="btn btn-danger btn-sm">Message</button> -->
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">


						
						<!-- <li class="active">
							<a href="index.php">
							<i class="glyphicon glyphicon-home"></i>
							Logout </a>
						</li> -->
						<li class="active">
							<a href="workprofile.php">
          <i class="glyphicon glyphicon-user"></i>
          Profile
        </a></li>

        <li>
							<a href="workprofile.php">
      <i class="glyphicon glyphicon-user"></i>
          Membership
        </a></li>
         
						<li>
							<a href="#">
          <i class="glyphicon glyphicon-cog"></i>
        
							 Settings </a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-user"></i>
							Invite friends </a>
						</li>
						<li>
							<a href="index.php">
							<i class="glyphicon glyphicon-home"></i>
							Logout </a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
			
			 	<h3>About Shashi Bahir</h3><br>
			 	<h5 style="font-size:18px;">Founder of a IT start up,3 years MNC experienced and intrested in<br> writing!</h5><br>
			 	<p>I'm Shashi Bahir! Founder of a start up having 15 developer team.<br>
Good programmer!<br>

Having 3 Years MNC experience!<br>
Keen article writer!Mainly fond of sports and travelling!<br>


<br>
			 	</p>
			 	<hr>
			 	<h3>Experience</h3><br>
			 	<h5 style="font-size:18px;">Software engineer</h5>
			 		
                   <p><strong style="font-size:15px;">Syntel</strong> Mar 2014 - Jun 2016<br>Worked as a software developer and tester for 2.5 years in Syntel.</p><hr>

			 	<h3>Education</h3><br>
			 	<h5 style="font-size:18px;">BE</h5>
			 	<!-- <h6 style="font-size:17px;">BE</h6> -->
			 	<p><strong style="font-size:15px;">Maharashtra Institute of Technology, India</strong> 2008 - 2013 (5 years)</p><hr>
			 	<h3>Certifications</h3><br>
			 	<p>You do not have any certifications.</p><br>
			 	<button type="button" class="btn btn-primary">Get Certified</button><hr>
			 	<h3>Recent Reviews</h3><br>
			 	 <div class="rating" style="margin-right:600px;">
  <span id="5" class="glyphicon glyphicon-star"></span>
  <span id="4" class="glyphicon glyphicon-star"></span>
  <span id="3" class="glyphicon glyphicon-star"></span>
  <span id="2" class="glyphicon glyphicon-star"></span>
  <span id="1" class="glyphicon glyphicon-star"></span>
</div>
<br>

			 	<hr>

                <h3>My Top Skills</h3><br>
              
              	
   <div class="container">
 
  <table class="table table-bordered" style="width:65%;">
   <!--  <thead>
      <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
         <th>Email</th>
          <th>Email</th>
      </tr>
    </thead> -->
    <tbody>
      <tr >
        <td style="font-size:15px;color:black;font-weight:bold;">PHP</td>
        <td style="font-size:15px;color:black;font-weight:bold;">C Programming</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Python</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Linux</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Game Design</td>
      </tr>
      <tr>
        <td style="font-size:15px;color:black;font-weight:bold;">WordPress</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Blog</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Software Architecture</td>
        <td style="font-size:15px;color:black;font-weight:bold;">eCommerce</td>
        <td style="font-size:15px;color:black;font-weight:bold;">Articles</td>
      </tr>
      <!-- <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr> -->
    </tbody>
  </table>
</div>


               
                 
			 </div>
            </div>
		</div>
	</div>
</div>
<center>

</center>
<br>
<br>


		
     </body>
     </html>
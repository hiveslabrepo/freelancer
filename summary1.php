<?php require_once ("header.php");?>

<style>
.well{
  margin-top: -170px;
  width:50%;
  border: 1px solid lightgray;
  font-size:25px;
}
.table{
   margin-top: -332px;
  width:50%;
  border: 1px solid lightgray;
  color:blue;
  font-size: 21px;

}
.table > tbody > tr > td{
  color:black;
}


</style>
<script>
    // Starrr plugin (https://github.com/dobtco/starrr)
var __slice = [].slice;

(function($, window) {
  var Starrr;

  Starrr = (function() {
    Starrr.prototype.defaults = {
      rating: void 0,
      numStars: 5,
      change: function(e, value) {}
    };

    function Starrr($el, options) {
      var i, _, _ref,
        _this = this;

      this.options = $.extend({}, this.defaults, options);
      this.$el = $el;
      _ref = this.defaults;
      for (i in _ref) {
        _ = _ref[i];
        if (this.$el.data(i) != null) {
          this.options[i] = this.$el.data(i);
        }
      }
      this.createStars();
      this.syncRating();
      this.$el.on('mouseover.starrr', 'span', function(e) {
        return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('mouseout.starrr', function() {
        return _this.syncRating();
      });
      this.$el.on('click.starrr', 'span', function(e) {
        return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('starrr:change', this.options.change);
    }

    Starrr.prototype.createStars = function() {
      var _i, _ref, _results;

      _results = [];
      for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
        _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
      }
      return _results;
    };

    Starrr.prototype.setRating = function(rating) {
      if (this.options.rating === rating) {
        rating = void 0;
      }
      this.options.rating = rating;
      this.syncRating();
      return this.$el.trigger('starrr:change', rating);
    };

    Starrr.prototype.syncRating = function(rating) {
      var i, _i, _j, _ref;

      rating || (rating = this.options.rating);
      if (rating) {
        for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
        }
      }
      if (rating && rating < 5) {
        for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        }
      }
      if (!rating) {
        return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
      }
    };

    return Starrr;

  })();
  return $.fn.extend({
    starrr: function() {
      var args, option;

      option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      return this.each(function() {
        var data;

        data = $(this).data('star-rating');
        if (!data) {
          $(this).data('star-rating', (data = new Starrr($(this), option)));
        }
        if (typeof option === 'string') {
          return data[option].apply(data, args);
        }
      });
    }
  });
})(window.jQuery, window);

$(function() {
  return $(".starrr").starrr();
});

$( document ).ready(function() {
      
  $('#stars').on('starrr:change', function(e, value){
    $('#count').html(value);
  });
  
  $('#stars-existing').on('starrr:change', function(e, value){
    $('#count-existing').html(value);
  });
});

    </script>




<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
  <div class="container">
    <div class="header-left grid">
      <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
        <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
      </div>
    </div>
    <div class="header-middle">
      <!-- <ul>
        <li><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>+123 456 7890</li>
        <li><a href="mailto:info@example.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>info@example.com</a></li>
      </ul> -->
      <div class="search">
        <form action="#" method="post">
          <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
          <input type="submit" value=" ">
        </form>
      </div>
    </div>
    <div class="header-right">
      
      <!--  <ul class="nav navbar-nav menu__list">


            <li ><a href="index.php">LogOut </a></li>
            

          
          </ul> -->
    </div>
   <!--  <div class="clearfix"></div> -->
  </div>
</div>
          </ul>
    </div>
   <!--  <div class="clearfix"></div> -->
  </div>
</div>
<div class="row">
  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
     <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <div class="container">
 
     <div class="well well-sm">
      <h4>Posted Job</h4>
     </div></div>
     <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
    </div>
    </div>
   <!--  ********************1st row end************ -->

   <div class="row">
     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">


     </div>
     <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

      
<div class="container">
  
  <table class="table"class="table-responsive">

    <!-- <thead>
      <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
      </tr>
    </thead> -->
    <tbody>

      
      <tr>
        <div class="col-md-2">
        <td>Job Title</td>
        </div>
        <div class="col-md-2" style="float:left;">
        <td>xyz</td>
      </div>
      </tr>
      
      <tr>
        <td>Category</td>
        
        <td>xyz</td>
      </tr>
      <tr>
        <td>Skills</td>
        
        <td>Web Designer</td>
      </tr>
      <tr>
        <td>payment</td>
        
        <td>xyz</td>
      </tr>
      <tr>
        <td>Address</td>
        
        <td>xyz</td>
      </tr>
      <tr>
        <td>Discription</td>
        
        <td>xyz</td>
      </tr>

    </tbody>

  </table>

</div>
    </div>
     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"style="margin-left:-5%;">
     <button type="button" class="btn btn-warning"><a href="postjob1.php" style="text-decoration:none;color:white;">Cancle</a></button>
      <button type="button" class="btn btn-success">Confirm</button>
    </div>
    </div>
    <!-- ********************row3rd end*********** -->
    
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css"/>
<div class="container">
  <div class="well" style="margin-top:20px;width:70%;margin-left:140px;height:200px;">
      <div class="media">
        <a class="pull-left" href="postedprofile.php">
        <img class="media-object" src="images/team1.jpg">
      </a>
      <div class="media-body">
        <h4 class="media-heading">Priyanshe Yadav</h4>
          <a href="postedprofile.php"><p class="text-right">Go To Profile</p></a>
        
          <ul class="list-inline list-unstyled">
        <li><span><i class="glyphicon glyphicon-calendar"></i> in 10days </span></li>
            <li>|</li>
            <span>500<i class="fa fa-inr" style="font-size:24px"></i></span>
            <li>|</li>
            <li>
             <div id="stars-existing" class="starrr" data-rating='4' style="margin-left:30px;margin-top:-200px;"></div>
            </li>
           
      </ul>
       </div>
    </div>
  </div>
 <!--  *****************************2nd**************** -->
  <div class="well" style="margin-top:20px;width:70%;margin-left:140px;height:200px;">
      <div class="media">
        <a class="pull-left" href="postedprofile.php">
        <img class="media-object" src="images/team1.jpg">
      </a>
      <div class="media-body">
        <h4 class="media-heading">Priyanshe Yadav</h4>
          <a href="postedprofile.php"><p class="text-right">Go To Profile</p></a>
         
          <ul class="list-inline list-unstyled">
        <li><span><i class="glyphicon glyphicon-calendar"></i> in 10days </span></li>
            <li>|</li>
            <span>500<i class="fa fa-inr" style="font-size:24px"></i></span>
            <li>|</li>
            <li>
             <div id="stars-existing" class="starrr" data-rating='4' style="margin-left:30px;margin-top:-200px;"></div>
            </li>
           
      </ul>
       </div>
    </div>
  </div>
 <!--  ***************************** -->
  <div class="well" style="margin-top:20px;width:70%;margin-left:140px;height:200px;">
      <div class="media">
        <a class="pull-left" href="postedprofile.php">
        <img class="media-object" src="images/team1.jpg">
      </a>
      <div class="media-body">
        <h4 class="media-heading">Priyanshe Yadav</h4>
          <a href="postedprofile.php"><p class="text-right">Go To Profile</p></a>
        
          <ul class="list-inline list-unstyled">
        <li><span><i class="glyphicon glyphicon-calendar"></i> in 10days </span></li>
            <li>|</li>
            <span>500<i class="fa fa-inr" style="font-size:24px"></i></span>
            <li>|</li>
            <li>
             <div id="stars-existing" class="starrr" data-rating='4' style="margin-left:30px;margin-top:-200px;"></div>
            </li>
           
      </ul>
       </div>
    </div>
  </div>
  <!-- **************************** -->
   <div class="well" style="margin-top:20px;width:70%;margin-left:140px;height:200px;">
      <div class="media">
        <a class="pull-left" href="postedprofile.php">
        <img class="media-object" src="images/team1.jpg">
      </a>
      <div class="media-body">
        <h4 class="media-heading">Priyanshe Yadav</h4>
          <a href="postedprofile.php"><p class="text-right">Go To Profile</p></a>
        
          <ul class="list-inline list-unstyled">
        <li><span><i class="glyphicon glyphicon-calendar"></i> in 10days </span></li>
            <li>|</li>
            <span>500<i class="fa fa-inr" style="font-size:24px"></i></span>
            <li>|</li>
            <li>
             <div id="stars-existing" class="starrr" data-rating='4' style="margin-left:30px;margin-top:-200px;"></div>
            </li>
           
      </ul>
       </div>
    </div>
  </div>
   
</div>

 


</body>
</html>
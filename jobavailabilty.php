<?php require_once ("header.php");?>
<head>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  background: #F1F3FA;

}

/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}
 .rating span {
  font-size: 25px;
  cursor: pointer;
  float: right; //remove 'float right' for right to left
}
.rating span:hover, .rating span:hover ~ span{
  color: red; //gold or any other color 
}
td:hover {
          /*background-color: #ffff99;*/
          -webkit-box-shadow: 0px 2px 4px #e06547;
    -moz-box-shadow: 0px 2px 4px #e06547;
    box-shadow: 0px 2px 4px #e06547;
    background-color: #d4d5d6;
        }

        .dropdown {
    display:inline-block;
    margin-left:20px;
    padding:10px;
  }
   .animated {
    -webkit-transition: height 0.2s;
    -moz-transition: height 0.2s;
    transition: height 0.2s;
}

.stars
{
    margin: 20px 0;
    font-size: 24px;
    color: #d17581;
}
.well {
  width:50%;
}
</style>
 <script>
  $('.rating span').click(function(){
    alert($(this).attr('id'));
})

</script>  


</head>
<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
    <div class="container">
        <div class="header-left grid">
            <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
                <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
            </div>
        </div>
        <div class="header-middle">
      
      <div class="search">
        <form action="#" method="post">
          <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
          <input type="submit" value=" ">
        </form>
      </div>
      
    </div>
  </div>
     </div>
    
    
    
<div class="container"> 
    <div class="row profile">
    <div class="col-md-2"></div>

    <div class="col-md-8">

            <div class="profile-content">
              <h3>Jobs For Logo Design</h3>
              <div class="container">
  <div class="well" style="margin-top:20px;width:55%;height:auto;">
      <div class="media">
        <a class="pull-left" href="postedprofile.php">
        <img class="media-object" src="images/job.png" >
      </a>
      <div class="media-body">
        <h4 class="media-heading">Design a Logo</h4>
        <p>We need a site developed in a php framework , preferably laravel. The site is a kind of freelancer/job site for entertainment industry where employers can post projects and interested ...</p><br>
       
       <i class="material-icons" style="font-size:15px;font-weight:bold;">query_builder</i><span> 10:35AM | 30-Nov-2016 | Wednesday</span>
        
      
       </div>
    </div>
  </div>
 <!--  ************************well 1***************** -->
  <div class="well" style="margin-top:20px;width:55%;height:auto;">
      <div class="media">
        <a class="pull-left" href="postedprofile.php">
        <img class="media-object" src="images/job.png" >
      </a>
      <div class="media-body">
        <h4 class="media-heading">Design a Logo</h4>
        <p>We need a site developed in a php framework , preferably laravel. The site is a kind of freelancer/job site for entertainment industry where employers can post projects and interested ...</p><br>
        <i class="material-icons" style="font-size:15px;font-weight:bold;">query_builder</i><span> 10:35AM | 30-Nov-2016 | Wednesday</span>
        
      
       </div>
    </div>
  </div>
  <!-- ****************************well 2******************* -->
   <div class="well" style="margin-top:20px;width:55%;height:auto;">
      <div class="media">
        <a class="pull-left" href="postedprofile.php">
        <img class="media-object" src="images/job.png" >
      </a>
      <div class="media-body">
        <h4 class="media-heading">Design a Logo</h4>
        <p>We need a site developed in a php framework , preferably laravel. The site is a kind of freelancer/job site for entertainment industry where employers can post projects and interested ...</p><br>
        <i class="material-icons" style="font-size:15px;font-weight:bold;">query_builder</i><span> 10:35AM | 30-Nov-2016 | Wednesday</span>
        
      
       </div>
    </div>
  </div>
  <!-- ************************* well 3********************* -->

  <div class="well" style="margin-top:20px;width:55%;height:auto;">
      <div class="media">
        <a class="pull-left" href="postedprofile.php">
        <img class="media-object" src="images/job.png" >
      </a>
      <div class="media-body">
        <h4 class="media-heading">Design a Logo</h4>
        <p>We need a site developed in a php framework , preferably laravel. The site is a kind of freelancer/job site for entertainment industry where employers can post projects and interested ...</p><br>
        <i class="material-icons" style="font-size:15px;font-weight:bold;">query_builder</i><span> 10:35AM | 30-Nov-2016 | Wednesday</span>
        
      
       </div>
    </div>
  </div>
  <!-- ************************* well 4********************* -->
          

    </div>


        <a href="index.php">
         <abbr title="Back"> <span class="glyphicon glyphicon-circle-arrow-left" style="font-size:36px;"></span></abbr>
        </a>    
        
      </div>
      
             </div>
              <div class="col-md-2"> </div>
           </div>

        </div>     
    
     </body>
     </html>
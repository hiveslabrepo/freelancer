<?php require_once ("header.php");?>
<head>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  background: #F1F3FA;
}

/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}
 .rating span {
  font-size: 25px;
  cursor: pointer;
  float: right; //remove 'float right' for right to left
}
.rating span:hover, .rating span:hover ~ span{
  color: red; //gold or any other color 
}
</style>
 <script>
  $('.rating span').click(function(){
    alert($(this).attr('id'));
})

</script>  


</head>
<body>
<!-- <div class="header wow fadeInDown animated" data-wow-delay=".5s">
    <div class="container">
        <div class="header-left grid">
            <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
                <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
            </div>
        </div>
        <div class="header-middle">
			
			<div class="search">
				<form action="#" method="post">
					<input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
					<input type="submit" value=" ">
				</form>
			</div>
			
		</div>
	</div>
     </div> -->
     <?php require_once ("navigation1.php");?>
    
		
		
<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="images/blank_img.jpg" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
					Sameer Kulkarni
					</div>
				
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<a href="#"><button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a>
					<!-- <button type="button" class="btn btn-danger btn-sm">Message</button> -->
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">


						
					
						<li class="active">
							<a href="workprofile.php">
          <i class="glyphicon glyphicon-user"></i>
          Profile
        </a></li>

        <li>
							<a href="workprofile.php">
      <i class="glyphicon glyphicon-user"></i>
          Membership
        </a></li>
         
						<li>
							<a href="#">
          <i class="glyphicon glyphicon-cog"></i>
        
							 Settings </a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-user"></i>
							Invite friends </a>
						</li>
						<li>
							<a href="index.php">
							<i class="glyphicon glyphicon-home"></i>
							Logout </a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
              <h3>Post Job</h3><br>
               <form class="form-horizontal">
    <div class="form-group">
      <label class="control-label col-sm-3" for="email" style="text-align:right;">What is your job title:</label>
      <div class="col-sm-9">
        <input type="email" class="form-control" id="email" placeholder="What is your job title">
      </div>
    </div>
   <!--  ***************************field 1***************** -->
    <div class="form-group">
      <label class="control-label col-sm-3" for="sel1" style="text-align:right;">Select Category:</label>
       <div class="col-sm-9">
      <select class="form-control" id="sel1">
        <option value="default">Select Category</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
    </div>
  </div>
 <div class="form-group">
      <label class="control-label col-sm-3" for="sel2">Choose Skills :</label>
       <div class="col-sm-9">
      <select class="form-control" id="sel2">
        <option value="default">Choose Skills for your job</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
    </div>
  </div>

  <div class="form-group">
      <label class="control-label col-sm-3" for="sel2">Area:</label>
       <div class="col-sm-9">
      <select class="form-control" id="sel3">
        <option value="default">Area</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
    </div>
  </div>

  <div class="form-group">
      <label class="control-label col-sm-3" for="sel2">Payment Duration Wise:</label>
       <div class="col-sm-9">
      <select class="form-control" id="sel1">
        <option value="default">Payment Duration Wise</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
    </div>
  </div>


 <div class="form-group">
  <label  class="control-label col-sm-3" for="address">Address:</label>
  <div class="col-sm-9">
  <textarea class="form-control" rows="5" id="address"></textarea>
</div>
</div>

<div class="form-group">
  <label  class="control-label col-sm-3" for="discription">Job Discription:</label>
  <div class="col-sm-9">
  <textarea class="form-control" rows="5" id="discription"></textarea>
</div>
</div>

 <div class="form-group">
       <div class=" col-lg-8"></div>
      <div class=" col-lg-4">
     <!--  <a href="postjob.php">  <button type="button" class="btn btn-info">Edit</button></a> -->
   <a href="summary.php"> <button type="button" class="btn btn-info">Post</button></a>
  </div>
</div>

  </form>
  <br>
   
<div class="well well-sm" style="width:100%;">
  <h4>Previouslly Posted Job</h4>
</div>
<div class="container">
<div class="col-md-3">
 <a href="job1info.php"> <img src="images/hives.png" style="border:1px solid black;"></a>

  </div>
  <div class="col-sm-6">
   <a href="job1info.php" style="color:black;"> <h3 style="margin-left:-41%;color:black;">Hives lab</h3></a>
   <P style="margin-left:-41%;"> HTML5,CSS3,JAVA SCRIPT,BOOTSTRAP</P>
   <P style="margin-left:-41%;"><strong style="font-size:15px;"> discription - </strong> We build light weight web pages which increase speed and reduce waiting time .</P>
     
</div>

</div>
<hr>
<div class="container">
<div class="col-md-3">
 <a href="job1info.php"> <img src="images/hives.png" style="border:1px solid black;"></a>

  </div>
  <div class="col-sm-6">
   <a href="job1info.php" style="color:black;"> <h3 style="margin-left:-41%;color:black;text-decoration:none;">Hives lab</h3></a>
   <P style="margin-left:-41%;"> HTML5,CSS3,JAVA SCRIPT,BOOTSTRAP</P>
   <P style="margin-left:-41%;"><strong style="font-size:15px;"> discription - </strong> We build light weight web pages which increase speed and reduce waiting time .</P>
</div>

</div>
<hr>
<div class="container">
<div class="col-md-3">
  <a href="job1info.php" style="color:black;"><img src="images/hives.png" style="border:1px solid black;"></a>

  </div>
  <div class="col-sm-6">
   <a href="job1info.php" style="color:black;"> <h3 style="margin-left:-41%;text-decoration:none;color:black;">Hives lab</h3></a>
   <P style="margin-left:-41%;"> HTML5,CSS3,JAVA SCRIPT,BOOTSTRAP</P>
   <P style="margin-left:-41%;"><strong style="font-size:15px;"> discription - </strong> We build light weight web pages which increase speed and reduce waiting time .</P>
</div>

</div>
<hr>
<div class="container">
<div class="col-md-3">
  <a href="job1info.php"><img src="images/hives.png" style="border:1px solid black;"></a>

  </div>
  <div class="col-sm-6">
   <a href="job1info.php" style="color:black;"> <h3 style="margin-left:-41%;text-decoration:none;color:black;">Hives lab</h3></a>
   <P style="margin-left:-41%;"> HTML5,CSS3,JAVA SCRIPT,BOOTSTRAP</P>
   <P style="margin-left:-41%;"><strong style="font-size:15px;"> discription - </strong> We build light weight web pages which increase speed and reduce waiting time .</P>
</div>

</div>
<hr>
			
			 </div>

    </div>

    

		
     </body>
     </html>
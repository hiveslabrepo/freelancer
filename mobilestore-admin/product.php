<!DOCTYPE html>
<html>
    <head>
        <title>Products</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap -->
        <link rel="stylesheet" media="screen" href="css/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="css/bootstrap-theme.min.css">

        <!-- Bootstrap Admin Theme -->
        <link rel="stylesheet" media="screen" href="css/bootstrap-admin-theme.css">
        <link rel="stylesheet" media="screen" href="css/bootstrap-admin-theme-change-size.css">

        <!-- Vendors -->
        <link rel="stylesheet" media="screen" href="vendors/easypiechart/jquery.easy-pie-chart.css">
        <link rel="stylesheet" media="screen" href="vendors/easypiechart/jquery.easy-pie-chart_custom.css">
		
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script type="text/javascript" src="js/jquery.min.js"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/twitter-bootstrap-hover-dropdown.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-admin-theme-change-size.js"></script>
        <script type="text/javascript" src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        
    </head>
    <body class="bootstrap-admin-with-small-navbar" style="background-color:#C1CDCD">
        <!-- small navbar -->
        <nav class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar-sm" role="navigation" style="background-color:#C1CDCD">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left bootstrap-admin-theme-change-size">
                                <!-- <li class="text">Change size:</li>
                                <li><a class="size-changer small">Small</a></li>
                                <li><a class="size-changer large active">Large</a></li> -->
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <!-- <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li>
                                    <a href="#">Reminders <i class="glyphicon glyphicon-bell"></i></a>
                                </li>
                                <li>
                                    <a href="#">Settings <i class="glyphicon glyphicon-cog"></i></a>
                                </li> -->
                                <li>
                                    <a href="#">Go to frontend <i class="glyphicon glyphicon-share-alt"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" role="button" class="dropdown-toggle" data-hover="dropdown"> <i class="glyphicon glyphicon-user"></i> Username <i class="caret"></i></a>
                                    <ul class="dropdown-menu">
                                        <!-- <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li> -->
                                        <li role="presentation" class="divider"></li>
                                        <li><a href="index.html"><i class="glyphicon glyphicon-off"></i>Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <!-- main / large navbar -->
        <nav class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar bootstrap-admin-navbar-under-small" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="about.html">Admin Panel</a>
                        </div>
                        <div class="collapse navbar-collapse main-navbar-collapse">
                            <ul class="nav navbar-nav">
                                <!-- <li class="active"><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li> -->
                                <!-- <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-hover="dropdown">Dropdown <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li role="presentation" class="dropdown-header">Dropdown header</li>
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li role="presentation" class="divider"></li>
                                        <li role="presentation" class="dropdown-header">Dropdown header</li>
                                        <li><a href="#">Separated link</a></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li> -->
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                </div>
            </div><!-- /.container -->
        </nav>

        <div class="container">
            <!-- left, vertical navbar & content -->
            <div class="row">
                <!-- left, vertical navbar -->
                <div class="col-md-2 bootstrap-admin-col-left">
                    <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
                        <li>
                            <a href="brand.php"><i class="glyphicon glyphicon-chevron-right"></i> Brands</a>
                        </li>
                        <li class="active">
                            <a href="product.php"><i class="glyphicon glyphicon-chevron-right"></i> Products</a>
                        </li>
                        <!-- <li class="disabled">
                            <a href="#"><i class="glyphicon glyphicon-chevron-right"></i> Calendar</a>
                        </li>
                        <li class="disabled">
                            <a href="#"><i class="glyphicon glyphicon-chevron-right"></i> Statistics (Charts)</a>
                        </li>
                        <li>
                            <a href="forms.html"><i class="glyphicon glyphicon-chevron-right"></i> Forms</a>
                        </li>
                        <li>
                            <a href="tables.html"><i class="glyphicon glyphicon-chevron-right"></i> Tables</a>
                        </li>
                        <li>
                            <a href="buttons-and-icons.html"><i class="glyphicon glyphicon-chevron-right"></i> Buttons &amp; Icons</a>
                        </li>
                        <li>
                            <a href="wysiwyg-editors.html"><i class="glyphicon glyphicon-chevron-right"></i> WYSIWYG Editors</a>
                        </li>
                        <li>
                            <a href="ui-and-interface.html"><i class="glyphicon glyphicon-chevron-right"></i> UI &amp; Interface</a>
                        </li>
                        <li>
                            <a href="error-pages.html"><i class="glyphicon glyphicon-chevron-right"></i> Error pages</a>
                        </li>
                        <li>
                            <a href="forms.html"><i class="glyphicon glyphicon-chevron-down"></i> Submenu</a>
                            <ul class="nav navbar-collapse bootstrap-admin-navbar-side">
                                <li><a href="about.html"><i class="glyphicon glyphicon-chevron-right"></i> Submenu 1</a></li>
                                <li><a href="about.html"><i class="glyphicon glyphicon-chevron-right"></i> Submenu 2</a></li>
                                <li><a href="about.html"><i class="glyphicon glyphicon-chevron-right"></i> Submenu 3</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><span class="badge pull-right">731</span> Orders</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge pull-right">812</span> Invoices</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge pull-right">27</span> Clients</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge pull-right">1,234</span> Users</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge pull-right">2,221</span> Messages</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge pull-right">11</span> Reports</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge pull-right">83</span> Errors</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge pull-right">4,231</span> Logs</a>
                        </li> -->
                    </ul>
                </div>

                <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header bootstrap-admin-content-title">
                                <h1>Products</h1>
								<a href="#" class="action">
                                   <button class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus-sign"></i>ADD NEW</button>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-success bootstrap-admin-alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <h4>Success</h4>
                                The operation completed successfully
                            </div>
                        </div>
                    </div> -->

                    
						<div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Available Products</div>
                                </div>
                                <div class="bootstrap-admin-panel-content"id="pagination_data">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
</div>
            </div>
        </div>
									<div class="pagination-container pagination pagination-sm" style="margin-left:900px">
										<?php
										include ('config.php');
										$page_query = "SELECT * FROM product ORDER BY id ASC";  
										$page_result = mysqli_query($conn, $page_query);  
										$total_records = mysqli_num_rows($page_result);  
										$total_pages = ceil($total_records/5);  
										for($i=1; $i<=$total_pages; $i++)  
										{  ?>
										<span class="pagination_link page<?php echo $i ;?>" style="cursor:pointer; padding:6px; border:1px solid #fff; background:white" id="<?php echo $i ;?>"><?php echo $i ;?></span>  
										<?php }  
										  
										?>
							
                                    </div>
									
                    

        <!-- footer -->
        <div class="navbar navbar-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <footer role="contentinfo">
                            <p class="left">Hives Online India Pvt. Ltd.</p>
                            <p class="right">&copy; 2016</p>
                        </footer>
                    </div>
                </div>
            </div>
        </div>

		

    </body>
</html>
<?php require_once('product-pop.php');?>
<?php require_once('edit-productpop.php');?>
<script type="text/javascript">
$(document).on("click", ".open-Editdialog", function () {
	var productname = $(this).data('product-name');
	var productid = $(this).data('product-id');
	var price = $(this).data('product-price');
	var date = $(this).data('product-date');
	var features = $(this).data('product-features');
	// alert(features);exit;
	$(".modal-body #editproductname").val( productname );
	$(".modal-body #editproductprice").val( price );
	$(".modal-body #date01").val( date );
	$(".modal-body #editproductid").val( productid );
	$(".modal-body #editproductfeatures").text(features);
});
$(document).ready(function(){  
      load_data();  
      function load_data(page)  
      {
		  if(page == undefined){
			  
			$('.page1').css('background-color', '#1e7ae3');
	      
		  }
		  
	       $.ajax({  
                url:"productpagination.php",  
                method:"POST",  
                data:{page:page},  
                success:function(data){  
                     $('#pagination_data').html(data); 
					 
                }  
           })  
      }  
      $(document).on('click', '.pagination_link', function(){ 
		$('.pagination_link').css('background-color', '');
		
		$(this).css('background-color', '#1e7ae3');
           var page = $(this).attr("id");  
		   load_data(page);  

           
      }); 
	  $(document).on('click', '.delete-row',function (event) {
			// alert('hii');
		event.preventDefault();
		var id = $(this).data('id');
		$.ajax({
			url: "deleteproduct.php",
			method: "POST",
			cache: false,
			data: { id: id },
			success: function (html) {
			$(this).parent().parent().remove();
			location.reload();
			}
	});
});
});

</script>
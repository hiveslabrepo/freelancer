<?php  

include 'config.php';
 // $bid = 1;
 $record_per_page = 5;  
 $page = '';  
 $output = '';  
 if(isset($_POST["page"]))  
 {  
	$page = $_POST["page"];
}  
 else  
 {  
      $page = 1;  
 }  
 if($page ==1){
	// $record_per_page =1;
	$bid = 1;
 }
 else{
	 $bid = $record_per_page*($page-1)+1;
  }
 $start_from = ($page - 1)*$record_per_page;  
 $query = "SELECT * FROM product ORDER BY id ASC LIMIT $start_from, $record_per_page";  
 $result = mysqli_query($conn, $query);  
 $output .= "  
      <table class='table bootstrap-admin-table-with-actions'>
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Product Name</th>
                                                <th>Product Image</th>
                                                <th>Price</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
 ";  
 while($row = mysqli_fetch_array($result))  
 {  
      $output .= '  
           <tr>									<td>'.$bid.'</td>	
                                                <td>'.$row['id'].'</td>
                                                <td>'.$row['name'].'</td>
                                                <td><img src='."images/".$row['img_name'].' alt="Image" height="42" width="42"></td>
                                                <td>'.$row['price'].'</td>
                                                <td class="actions">
                                                    <a href="#myModal2" data-toggle="modal" class="open-Editdialog" data-product-name='.$row['name'].' 
													data-product-price='.$row['price'].' data-product-date='.$row['date'].' 
													data-product-features='.$row['features'].' data-product-id='.$row['id'].'>
                                                        <button class="btn btn-sm btn-primary">
                                                            <i class="glyphicon glyphicon-pencil"></i>
                                                            Edit
                                                        </button>
                                                    </a>
                                                    
                                                    <a href="#" data-id='.$row["id"].' class="delete-row">
                                                        <button class="btn btn-sm btn-danger">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                            Delete
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>';
      $bid++;  
 }  
 $output .= '</tbody></table><br />';  
 
 echo $output;  
 ?>  
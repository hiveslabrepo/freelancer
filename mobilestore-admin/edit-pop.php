
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Edit Brands</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
</head>
<body>

<div class="container">
  

  <!-- Modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Edit Brands</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in edit-content">
				
                                    <form class="form-horizontal" method="post" action="brand.php">
                                        <fieldset>
                                            <legend></legend>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="typeahead">Brand Name</label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="brandname" id="typeahead1">
													<input type="hidden" name="brandid" id="typeahead2">
                                                  
											</div>
						
                                            </div>
                                       
                                            <button type="submit" class="btn btn-primary" name="edit" style="margin-left:150px;" ng-click="insertdata()">Save Changes</button>
                                            <button type="reset" class="btn btn-default" style="margin-left:20px;">Cancel</button>
                                        </fieldset>
                                    </form>
				    
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
  
    </div>
  </div>
  
</div>

</body>
</html>
<?php
error_reporting(0);
include 'config.php';
if(isset($_POST['edit'])) {
	$updateBrand = $_POST['brandname'];
	$brandid = $_POST['brandid'];
	$query = "UPDATE brand SET name='$updateBrand' WHERE id='$brandid'";
	$conn->query($query);
		
}
?>
<script>
var app = angular.module('myApp',[]);
app.controller('cntrl',function($scope,$http){
	$scope.insertdata = function() {
		$http.post("insert.php",{'brandname':$scope.brand})
		.success(function(){
			
		$scope.msg = "Data Inserted";
		location.reload();
		
			
		});
	}
	
});
</script>

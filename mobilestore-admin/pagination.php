<?php  

include 'config.php';
 // $bid = 1;
 $record_per_page = 5;  
 $page = '';  
 $output = '';  
 if(isset($_POST["page"]))  
 {  
	$page = $_POST["page"];
}  
 else  
 {  
      $page = 1;  
 }  
 if($page ==1){
	// $record_per_page =1;
	$bid = 1;
 }
 else{
	 $bid = $record_per_page*($page-1)+1;
  }
 $start_from = ($page - 1)*$record_per_page;  
 $query = "SELECT * FROM brand ORDER BY id ASC LIMIT $start_from, $record_per_page";  
 $result = mysqli_query($conn, $query);  
 $output .= "  
      <table class='table bootstrap-admin-table-with-actions'>
				<thead>
					<tr>
						
						<th>Id</th>
						<th>Brands Name</th>
						
						<th>Actions</th>
					</tr>
				</thead> 
				<tbody>
 ";  
 while($row = mysqli_fetch_array($result))  
 {  
      $output .= '  
           <tr>
											
			<td>'.$bid.'</td>
			<td>'.$row["name"].'</td>
			
			<td class="actions">
				<a href="#myModal1" data-toggle="modal" data-brand-id='.$row["id"].' data-brand-name='.$row["name"].' class="open-EditDialog">
					<button class="btn btn-sm btn-primary">
						<i class="glyphicon glyphicon-pencil"></i>
						Edit
					</button>
				</a>
				
				<a href="#" data-id='.$row["id"].' class="delete-row">
					<button class="btn btn-sm btn-danger">
						<i class="glyphicon glyphicon-trash"></i>
						Delete
					</button>
				</a>
			</td>

		</tr>';
      $bid++;  
 }  
 $output .= '</tbody></table><br />';  
 
 echo $output;  
 ?>  
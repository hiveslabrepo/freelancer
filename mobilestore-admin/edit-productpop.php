<!DOCTYPE html>
<html lang="en">
<head>
  <title>Edit Products</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<div class="container">
  

  <!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Products</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Products Details</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    <form class="form-horizontal" method ="post" enctype ="multipart/form-data" action="product.php">
                                        <fieldset>
                                            <legend></legend>
											<div class="form-group">
                                                <label class="col-lg-2 control-label" for="select02">Select Brand</label>
                                                <div class="col-lg-10">
                                                    <select id="brandselect" name="brandselect" class="selectize-select" style="width: 200px;height:30px;">
													<?php
													include 'config.php';
													$query = "SELECT * FROM brand";
													$result = $conn->query($query);
													while($row=$result->fetch_assoc()){?>
														
													
													
                                                        <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
													<?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="typeahead">Product Name</label>
                                                <div class="col-lg-10">
                                                    <input type="text" name = "productname" class="form-control col-md-6" id="editproductname">
                                                    <input type="hidden" name = "productid" class="form-control col-md-6" id="editproductid">
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="col-lg-2 control-label" for="typeahead">Price</label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="price" class="form-control col-md-6" id="editproductprice" style="width:80px">
                                                    
                                                </div>
                                            </div>
											<div class="form-group">
												<label class="col-lg-2 control-label" for="typeahead">Product Image</label>
											<div class="col-lg-10">
											<div id="uniform-fileInput" class="uploader">
													<input id="fileInput" name="fileInput" id="editproductimage" class="uniform_on" type="file">
													
													<span class="action" style="-moz-user-select: none;">Choose File</span>
											</div>
										</div>
									</div>
											<div class="form-group">
                                                <label class="col-lg-2 control-label" for="date01">Date</label>
                                                <div class="col-lg-10">
                                                    <input type="date" name="addate" class="form-control datepicker" id="date01" placeholder="Date">
                                                    
                                                </div>
                                            </div>
											<div class="form-group">	
                                                <label class="col-lg-2 control-label" for="textarea-wysihtml5">Features</label>
                                                <div class="col-lg-10">
                                                    <textarea  id="editproductfeatures" name="features" style="width: 100%; height: 200px"></textarea>
                                                </div>
                                            </div>
									  
                                            <button type="submit" id="submit" name="editProduct" class="btn btn-primary" style="margin-left:200px;margin-top:20px;">Submit</button>
                                            <button type="reset" class="btn btn-default" style="margin-left:20px;margin-top:20px;">Reset</button>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
	  
	  
      
    </div>
  </div>
  
</div>

</body>
</html>
<?php
error_reporting(0);
include 'config.php';
if(isset($_POST['editProduct'])) {
	$updateProduct = $_POST['productname'];
	$updatePrice = $_POST['price'];
	$updateDate = $_POST['addate'];
	$updateProductid = $_POST['productid'];
	$updateFeatures = $_POST['features'];
	$updtaeImgName = $_FILES['fileInput']['name'];
	$fileInputType = $_FILES['fileInput']['type'];
	$fileInputSize = $_FILES['fileInput']['size'];
	$fileInputTemp = $_FILES['fileInput']['tmp_name'];
	$addate = $_POST['addate'];
	$features = $_POST['features'];
	
	move_uploaded_file($fileInputTemp,"images/$updtaeImgName");
	$query = "UPDATE product SET name='$updateProduct',price='$updatePrice',date='$updateDate',features='$updateFeatures',img_name='$updtaeImgName' WHERE id='$updateProductid'";
	$conn->query($query);
		
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>ADD Brands</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
</head>
<body>

<div class="container">
  

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">ADD NEW BRANDS</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Brands</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
				<div ng-app="myApp" ng-controller="cntrl">
                                    <form class="form-horizontal">
                                        <fieldset>
                                            <legend></legend>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="typeahead">Brand Name</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control col-md-6" ng-model="brand" id="typeahead" autocomplete="off">
                                                  
						</div>
						<h5 style="color:red">{{msg}}<h5>
                                            </div>
                                       
                                            <button type="submit" class="btn btn-primary" style="margin-left:200px;" ng-click="insertdata()">Submit</button>
                                            <button type="reset" class="btn btn-default" style="margin-left:20px;">Cancel</button>
                                        </fieldset>
                                    </form>
				    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
  
    </div>
  </div>
  
</div>

</body>
</html>
<script>
var app = angular.module('myApp',[]);
app.controller('cntrl',function($scope,$http){
	$scope.insertdata = function() {
		$http.post("insert.php?id=2",{'brandname':$scope.brand})
		.success(function(){
			
		$scope.msg = "Data Inserted";
		location.reload();
		
			
		});
	}
	// $scope.displaybrands = function(){
		// $http.get("select.php")
		// .success(function(data){
			// $scope.data = data
		// });
	// }
});
</script>

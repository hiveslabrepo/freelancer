<!DOCTYPE html>
<html>
    <head>
        <title>Brands</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap Docs -->
        <link href="http://getbootstrap.com/docs-assets/css/docs.css" rel="stylesheet" media="screen">

        <!-- Bootstrap -->
        <link rel="stylesheet" media="screen" href="css/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="css/bootstrap-theme.min.css">

        <!-- Bootstrap Admin Theme -->
        <link rel="stylesheet" media="screen" href="css/bootstrap-admin-theme.css">
        <link rel="stylesheet" media="screen" href="css/bootstrap-admin-theme-change-size.css">
	
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/twitter-bootstrap-hover-dropdown.min.js"></script>
		<script type="text/javascript" src="js/bootstrap-admin-theme-change-size.js"></script>
	
	

        <!-- Custom styles -->
        <style type="text/css">
            @font-face {
                font-family: Ubuntu;
                src: url('fonts/Ubuntu-Regular.ttf');
            }
            .bs-docs-masthead{
                background-color: #6f5499;
                background-image: linear-gradient(to bottom, #563d7c 0px, #6f5499 100%);
                background-repeat: repeat-x;
            }
            .bs-docs-masthead{
                padding: 0;
            }
            .bs-docs-masthead h1{
                color: #fff;
                font-size: 40px;
                margin: 0;
                padding: 34px 0;
                text-align: center;
            }
            .bs-docs-masthead a:hover{
                text-decoration: none;
            }
            .meritoo-logo a{
                background-color: #fff;
                border: 1px solid rgba(66, 139, 202, 0.4);
                display: block;
                font-family: Ubuntu;
                padding: 22px 0;
                text-align: center;
            }
            .meritoo-logo a,
            .meritoo-logo a:hover,
            .meritoo-logo a:focus{
                text-decoration: none;
            }
            .meritoo-logo a img{
                display: block;
                margin: 0 auto;
            }
            .meritoo-logo a span{
                color: #4e4b4b;
                font-size: 18px;
            }
            .row-urls{
                margin-top: 4px;
            }
            .row-urls .col-md-6{
                text-align: center;
            }
            .row-urls .col-md-6 a{
                font-size: 14px;
            }
        </style>

        
    </head>
    <body class="bootstrap-admin-with-small-navbar" style="background-color:#C1CDCD">
        <!-- small navbar -->
        <nav class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar-sm" role="navigation"  style="background-color:#C1CDCD">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left bootstrap-admin-theme-change-size">
                                <!-- <li class="text">Change size:</li>
                                <li><a class="size-changer small">Small</a></li>
                                <li><a class="size-changer large active">Large</a></li> -->
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <!-- <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li>
                                    <a href="#">Reminders <i class="glyphicon glyphicon-bell"></i></a>
                                </li>
                                <li>
                                    <a href="#">Settings <i class="glyphicon glyphicon-cog"></i></a>
                                </li> -->
                                <li>
                                    <a href="#">Go to frontend <i class="glyphicon glyphicon-share-alt"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" role="button" class="dropdown-toggle" data-hover="dropdown"> <i class="glyphicon glyphicon-user"></i> Username <i class="caret"></i></a>
                                    <ul class="dropdown-menu">
                                        
                                        <li><a href="index.html"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
										
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <!-- main / large navbar -->
        <nav class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar bootstrap-admin-navbar-under-small" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="about.html">Admin Panel</a>
                        </div>
                        <div class="collapse navbar-collapse main-navbar-collapse">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                </div>
            </div><!-- /.container -->
        </nav>

        <div class="container">
            <!-- left, vertical navbar & content -->
            <div class="row">
                <!-- left, vertical navbar -->
                <div class="col-md-2 bootstrap-admin-col-left">
                    <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
                        <li class="active">
                            <a href="brand.php"><i class="glyphicon glyphicon-chevron-right"></i> Brands</a>
							
                        </li>
                        <li>
                            <a href="product.php"><i class="glyphicon glyphicon-chevron-right"></i> Products</a>
                        </li>
                        
                    </ul>
                </div>

                <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header bootstrap-admin-content-title">
                                <h1>Brands</h1>
                                <a href="#" class="action">
                                   <button class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus-sign"></i>ADD NEW</button>
                                </a>
                               				
                            </div>
                        </div>
                    </div>
					  <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Available Brands</div>
                                </div>

                                <div class="bootstrap-admin-panel-content" id="pagination_data">
								
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>		
					<div class="pagination-container pagination pagination-sm" style="margin-left:900px">
					<?php
					include ('config.php');
					$page_query = "SELECT * FROM brand ORDER BY id ASC";  
					$page_result = mysqli_query($conn, $page_query);  
					$total_records = mysqli_num_rows($page_result);  
					$total_pages = ceil($total_records/5);  
					for($i=1; $i<=$total_pages; $i++)  
					{  ?>
					<span class="pagination_link page<?php echo $i ;?>" style="cursor:pointer; padding:6px; border:1px solid #fff; background:white" id="<?php echo $i ;?>"><?php echo $i ;?></span>  
					<?php }  
					  
					?>
							
					</div>

        <!-- footer -->
        <div class="navbar navbar-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <footer role="contentinfo">
                            <p class="left">Hives Online India Pvt. Ltd.</p>
                            <p class="right">&copy; 2016</p>
                        </footer>
                    </div>
                </div>
            </div>
        </div>

        
	
    </body>
</html>
<?php require_once('brand-pop.php');?>
<?php require_once('edit-pop.php');?>
<script>
$(document).on("click", ".open-EditDialog", function () {
     var brandname = $(this).data('brand-name');
	 var brandid = $(this).data('brand-id');
     $(".modal-body #typeahead1").val( brandname );
	 $(".modal-body #typeahead2").val( brandid );
});
$(document).ready(function(){  
      load_data();  
      function load_data(page)  
      {
		  if(page == undefined){
			  
			$('.page1').css('background-color', '#1e7ae3');
	      
		  }
		  
	       $.ajax({  
                url:"pagination.php",  
                method:"POST",  
                data:{page:page},  
                success:function(data){  
                     $('#pagination_data').html(data); 
					 
                }  
           })  
      }  
      $(document).on('click', '.pagination_link', function(){ 
		$('.pagination_link').css('background-color', '');
		
		$(this).css('background-color', '#1e7ae3');
           var page = $(this).attr("id");  
		   load_data(page);  

           
      }); 
		$(document).on('click', '.delete-row',function (event) {
			// alert('hii');
		event.preventDefault();
		var id = $(this).data('id');
		$.ajax({
			url: "delete.php",
			method: "POST",
			cache: false,
			data: { id: id },
			success: function (html) {
			$(this).parent().parent().remove();
			location.reload();
			}
	});
});
 });  
</script>

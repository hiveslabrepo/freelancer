<?php require_once ("header.php");?>
<head>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  background: #F1F3FA;
}

/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
  border: 1px solid black
}
 .rating span {
  font-size: 25px;
  cursor: pointer;
  float: right; //remove 'float right' for right to left
}
.rating span:hover, .rating span:hover ~ span{
  color: red; //gold or any other color 
}
hr{
  width:60%;
  margin-left:1px;
 
}

</style>
 <script>
  $('.rating span').click(function(){
    alert($(this).attr('id'));
})

</script>  


</head>
<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
    <div class="container">
        <div class="header-left grid">
            <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
                <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
            </div>
        </div>
        <div class="header-middle">
      
      <div class="search">
        <form action="#" method="post">
          <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
          <input type="submit" value=" ">
        </form>
      </div>
      
    </div>
  </div>
     </div>
    
    
    
<div class="container">
    <div class="row profile">
    <div class="col-md-2"></div>
    <div class="col-md-8">
            <div class="profile-content">
               <div class="container">

    
  <img src="images/hives.png" style="border:1px solid black;"> <strong style="font-size:20px;"> Hives Lab</strong>


    <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Job Title</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
     <!-- *******************row 1 end****************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Category</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>Software engineering & Technology</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
     <!-- **************************row 2 end*************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Skills</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>HTML5 </h4>
        </div>
         <!--  <div class="col-md-5"></div> -->
     </div>
     <!-- ***********************row 3 end********************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Payment</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>XYZ</h4>
        </div>
         <!--  <div class="col-md-5"></div> -->
     </div>
    <!--  *****************************row 4 end******************** -->
     <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Address</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
    <!--  ************************* row 5 end************************* -->
     <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Discription</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div><br><hr><br>
       <div class="row" style="margin-top:20px;">
   
<div class="col-md-3">
 <a href="#"> <img src="images/hives.png" style="border:1px solid black;"></a>

  </div>
  <div class="col-sm-5">
   <a href="#" style="color:black;"> <h3 style="margin-left:-41%;">Hives lab</h3></a>
   <P style="margin-left:-41%;"> HTML5,CSS3,JAVA SCRIPT,BOOTSTRAP</P>
   <P style="margin-left:-41%;"><strong style="font-size:15px;"> discription - </strong> We build light weight web pages which increase speed and reduce<br> waiting time .</P>
   <!-- <button>Applied for this job</button> -->
    <button type="button" class="btn btn-default btn-sm" id="btn1" >
          <span class="glyphicon glyphicon-user"></span> Applied
        </button>


  
  <ul class="list-group chk1" style="width:40%;width: 30%;
    margin-top: 2px;">
    <li class="list-group-item">First item</li>
    <li class="list-group-item">Second item</li>
    <li class="list-group-item">Third item</li>
  </ul>

</div>

</div>
<hr>
   <div class="row" style="margin-top:20px;">
   
<div class="col-md-3">
 <a href="#"> <img src="images/hives.png" style="border:1px solid black;"></a>

  </div>
  <div class="col-sm-5">
   <a href="#" style="color:black;"> <h3 style="margin-left:-41%;">Hives lab</h3></a>
   <P style="margin-left:-41%;"> HTML5,CSS3,JAVA SCRIPT,BOOTSTRAP</P>
   <P style="margin-left:-41%;"><strong style="font-size:15px;"> discription - </strong> We build light weight web pages which increase speed and reduce<br> waiting time .</P>
   <!-- <button>Applied for this job</button> -->
    <button type="button" class="btn btn-default btn-sm" id="btn2">
          <span class="glyphicon glyphicon-user"></span> Applied
        </button>


  
  <ul class="list-group chk2" style="width:40%;width: 30%;
    margin-top: 2px;">
    <li class="list-group-item">First item</li>
    <li class="list-group-item">Second item</li>
    <li class="list-group-item">Third item</li>
  </ul>

</div>

</div>
<hr>
<div class="row" style="margin-top:20px;">
   
<div class="col-md-3">
 <a href="#"> <img src="images/hives.png" style="border:1px solid black;"></a>

  </div>
  <div class="col-sm-5">
   <a href="#" style="color:black;"> <h3 style="margin-left:-41%;">Hives lab</h3></a>
   <P style="margin-left:-41%;"> HTML5,CSS3,JAVA SCRIPT,BOOTSTRAP</P>
   <P style="margin-left:-41%;"><strong style="font-size:15px;"> discription - </strong> We build light weight web pages which increase speed and reduce<br> waiting time .</P>
   <!-- <button>Applied for this job</button> -->
    <button type="button" class="btn btn-default btn-sm" id="btn3">
          <span class="glyphicon glyphicon-user"></span> Applied
        </button>


  
  <ul class="list-group chk3" style="width:40%;width: 30%;
    margin-top: 2px;">
    <li class="list-group-item">First item</li>
    <li class="list-group-item">Second item</li>
    <li class="list-group-item">Third item</li>
  </ul>

</div>

</div>
<hr>
<div class="row" style="margin-top:20px;">
   
<div class="col-md-3">
 <a href="#"> <img src="images/hives.png" style="border:1px solid black;"></a>

  </div>
  <div class="col-sm-5">
   <a href="#" style="color:black;"> <h3 style="margin-left:-41%;">Hives lab</h3></a>
   <P style="margin-left:-41%;"> HTML5,CSS3,JAVA SCRIPT,BOOTSTRAP</P>
   <P style="margin-left:-41%;"><strong style="font-size:15px;"> discription - </strong> We build light weight web pages which increase speed and reduce<br> waiting time .</P>
   <!-- <button>Applied for this job</button> -->
    <button type="button" class="btn btn-default btn-sm" id="btn4">
          <span class="glyphicon glyphicon-user"></span> Applied
        </button>


  
  <ul class="list-group chk4" style="width:40%;width: 30%;
    margin-top: 2px;">
    <li class="list-group-item">First item</li>
    <li class="list-group-item">Second item</li>
    <li class="list-group-item">Third item</li>
  </ul>

</div>

</div>
<hr>
     
    </div>
   <!--  ************************end jumbotron*************** -->
  </div>
  <!-- *************************end container********************* -->

             
  

           </div>
   </div>
  
<div class="col-md-2"></div>

 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#btn1").click(function(){
        $(".chk1").toggle();
    });

    $("#btn2").click(function(){
        $(".chk2").toggle();
    });
     $("#btn3").click(function(){
        $(".chk3").toggle();
    });
      $("#btn4").click(function(){
        $(".chk4").toggle();
    });
});
</script>



    

    
     </body>
     </html>
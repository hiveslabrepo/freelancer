<!DOCTYPE html>
<html lang="en">
<head>
  <title>Hire</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Pets Love Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- <link href="css/freelancer.min.css" rel="stylesheet"> -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>

<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Acme' rel='stylesheet' type='text/css'><!-- //fonts -->

  <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $(".scroll").click(function(event){   
          event.preventDefault();
          $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
      });
    </script>
  <!-- start-smoth-scrolling -->
    <!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="js/wow.min.js"></script>
  <script>
     new WOW().init();
  </script>
<!--//end-animate-->
<link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
  .portfolio-modal .close-modal}{position:absolute;width:75px;height:75px;background-color:transparent;top:25px;right:25px;cursor:pointer}
  [class*="dom-heading"] {
   margin: 0;
 }
 .container .rectangle {
   background: white;
   -moz-box-shadow: none;
   -webkit-box-shadow: none;
   -o-box-shadow: none;
   box-shadow: none;
   border-radius: 0px;
   margin-left: 0px !important;
   border: none;
   height: auto;
   width:100%;
   margin-top: 30px;
   border: 2px solid lightgray;

 }
 .rectangle p{
  font-size: 15px;
  font-weight: normal;
 }
  </style>

</head>

<body>
  <div class="header wow fadeInDown animated" data-wow-delay=".5s">
    <div class="container">
        <div class="header-left grid">
            <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
                <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
            </div>
        </div>
        <div class="header-middle">
            <!-- <ul>
                <li><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>+123 456 7890</li>
                <li><a href="mailto:info@example.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>info@example.com</a></li>
            </ul> -->
           <!--  <div class="search">
                <form action="#" method="post">
                    <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
                    <input type="submit" value=" ">
                </form>
            </div> -->
        </div>
        <div class="header-right">
            <!-- <div class="search">
                <form action="#" method="post">
                    <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
                    <input type="submit" value=" ">
                </form>
            </div> -->
             <ul class="nav navbar-nav menu__list">


                        <!-- <li class="active menu__item menu__item--current" class="page-scroll" a href="#portfolio" data-toggle="modal" data-target="#login"><a href="#portfolio" data-toggle="modal" data-target="#login">Login <span class="sr-only">(current)</span></a></li> -->
                       <!--  <li class=" menu__item menu__item--current"><a class="menu__link" href="index.php">Logout</a></li> -->
            

            <li class=" menu__item"><a href="index.php">Sign-Out</a></li>
          </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
                    </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="jumbotron">
  <h2 style="margin-left:10%;">Create Your Profile</h2>
  <div class="container">
  <div class="rectangle span12">
    <p>When you submit a proposal, clients review your profile to decide if your skills and experience match what they are looking for. It's time to create a profile with the jobs you want in mind. This is your opportunity to market your freelance business to clients.

Note that your profile must be approved, so take time to fill it out accurately.

Looking for inspiration? Check out these strong profile examples from a writer/translator, a developer, and a customer service agent.</p>

  </div>
  <div class="container">
    <div class="row-fluid">
      <!-- <span class="span12">
        <img src="images/one-pager-blogcta-08.png" class="pull-right image">
      </span> -->
    </div>
  </div>

</div>
<div class="container">
  <div class="rectangle span12">
    <p>Tell us more about you

Please upload a professional portrait that clearly shows your face</p>

  </div>
  <div class="container">
    <div class="row-fluid">
      <!-- <span class="span12">
        <img src="images/one-pager-blogcta-08.png" class="pull-right image">
      </span> -->
    </div>
  </div>
  
</div>
<div class="container">
  <div class="rectangle span12">
      <p><span class="glyphicon glyphicon-link" style="margin-left:10%;"> What is your Job title</span></p>


  </div>
  <div class="container">
    <div class="row-fluid">
      <!-- <span class="span12">
        <img src="images/one-pager-blogcta-08.png" class="pull-right image">
      </span> -->
    </div>
  </div>
  
</div>

</div>



</body>
</html>


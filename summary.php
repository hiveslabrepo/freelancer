<?php require_once ("header.php");?>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

<style>
@import url(http://fonts.googleapis.com/css?family=Lato:400,700);
body
{
    font-family: 'Lato', 'sans-serif';

  background: #F1F3FA;

    }
.profile 
{
    min-height: auto;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }
/*.dropdown-menu 
{
    background-color: #34495e;    
    box-shadow: none;
    -webkit-box-shadow: none;
    width: 250px;
    margin-left: -125px;
    left: 50%;
    }
.dropdown-menu .divider 
{
    background:none;    
    }
.dropdown-menu>li>a
{
    color:#f5f5f5;
    }
.dropup .dropdown-menu 
{
    margin-bottom:10px;
    }
.dropup .dropdown-menu:before 
{
    content: "";
    border-top: 10px solid #34495e;
    border-right: 10px solid transparent;
    border-left: 10px solid transparent;
    position: absolute;
    bottom: -10px;
    left: 50%;
    margin-left: -10px;
    z-index: 10;
    }*/
    .user-row {
    margin-bottom: 14px;
}

.user-row:last-child {
    margin-bottom: 0;
}

.dropdown-user {
    margin: 13px 0;
    padding: 5px;
    height: 100%;
}

.dropdown-user:hover {
    cursor: pointer;
}

.table-user-information > tbody > tr {
    border-top: 1px solid rgb(221, 221, 221);
}

.table-user-information > tbody > tr:first-child {
    border-top: 0;
}


.table-user-information > tbody > tr > td {
    border-top: 0;
}
.toppad
{margin-top:20px;
}

.threed:hover
{
        box-shadow:
                2px 2px 2px 2px #006600,
                2px 2px  2px 2px #006600,
                2px 2px  2px 2px #006600,
                2px 2px  2px 2px #006600;
        -webkit-transform: translateX(-3px);
        transform: translateX(-3px);
}
/*table td{
    font-weight:bold;

}*/


</style>
<body>
<?php require_once ("navigation1.php");?>
<div class="container">
      <div class="row" style="margin-top:30px;">
     
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
   
   
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Sheena Shrestha</h3>
            </div>
            <div class="panel-body">
              <div class="row" style="margin-top: 50px;">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive"> </div>
                
                <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                  <dl>
                    <dt>DEPARTMENT:</dt>
                    <dd>Administrator</dd>
                    <dt>HIRE DATE</dt>
                    <dd>11/12/2013</dd>
                    <dt>DATE OF BIRTH</dt>
                       <dd>11/12/2013</dd>
                    <dt>GENDER</dt>
                    <dd>Male</dd>
                  </dl>
                </div>-->
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information" style="margin-top:-40px;">
                    <tbody style="font-size: 20px;">
                      <tr>
                        <td style="color:black;font-weight:bold;">Job Title:</td>
                        <td style="color:black;">XYZ</td>
                      </tr>
                      <tr>
                        <td style="color:black;font-weight:bold;">Category:</td>
                        <td style="color:black;">XYZ</td>
                      </tr>
                      <tr>
                        <td style="color:black;font-weight:bold;">Skills :</td>
                        <td style="color:black;">XYZ</td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td style="color:black;font-weight:bold;">payment :</td>
                        <td style="color:black;">XYZ</td>
                      </tr>
                        <tr>
                        <td style="color:black;font-weight:bold;">Address</td>
                        <td style="color:black;">Kathmandu,Nepal</td>
                      </tr>
                      <tr>
                        <td style="color:black;font-weight:bold;">Discription :</td>
                        <td style="color:black;">XYZ</td>
                      </tr>
                       <!--  <td>Phone Number</td>
                        <td>123-4567-890(Landline)<br><br>555-4567-890(Mobile) -->
                        </td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                  
               <!--    <a href="#" class="btn btn-primary">My Sales Performance</a>
                  <a href="#" class="btn btn-primary">Team Sales Performance</a> -->
                </div>
              </div>
            </div>
              
            
          </div>
        </div>
      </div>
    </div>
    <!-- *************************summary***************** -->

 
<div class="container">
  <div class="row" style="margin-top:5px;">
    <div class="col-md-6">
        <div class="threed">
      <a href="test2.php" style="color:black;"  > <div class="well profile">
            <div class="col-sm-12">
                <div class="col-xs-12 col-sm-8">
                    <h2 style="font-size:20px;">Nicole Pearson</h2>
                    <p ><strong style="font-size:20px;">About: </strong> Web Designer / UI. </p>
                    <p><strong style="font-size:20px;">Hobbies: </strong> Read, out with friends, listen to music, draw and learn new things. </p>
                    <p><strong style="font-size:20px;">Skills: </strong>
                        <span class="tags">html5</span> 
                        <span class="tags">css3</span>
                        <span class="tags">jquery</span>
                        <span class="tags">bootstrap3</span>
                    </p>
                </div> <br>            
                <div class="col-xs-12 col-sm-4 text-center">
                    <figure>
                        <img src="images/pic.jpg" alt="" class="img-circle img-responsive" style="margin-top: -6px;margin-left:30px;">
                        <figcaption class="ratings">
                            <p>Ratings
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                 <span class="fa fa-star-o"></span>
                            </a> 
                            </p>
                        </figcaption>
                    </figure>
                </div>
            </div> </a>           
            </div>
       </div>                 
    </div>

   <!--  ***************************2nd********************** -->
   <div class="col-md-6">
     <div class="threed">
       <a href="test2.php" style="color:black;"><div class="well profile">
            <div class="col-sm-12">
                <div class="col-xs-12 col-sm-8">
                    <h2 style="font-size:20px;">Nicole Pearson</h2>
                    <p ><strong style="font-size:20px;">About: </strong> Web Designer / UI. </p>
                    <p><strong style="font-size:20px;">Hobbies: </strong> Read, out with friends, listen to music, draw and learn new things. </p>
                    <p><strong style="font-size:20px;">Skills: </strong>
                        <span class="tags">html5</span> 
                        <span class="tags">css3</span>
                        <span class="tags">jquery</span>
                        <span class="tags">bootstrap3</span>
                    </p>
                </div> <br>            
                <div class="col-xs-12 col-sm-4 text-center">
                    <figure>
                        <img src="images/pic.jpg" alt="" class="img-circle img-responsive" style="margin-top: -6px;margin-left:30px;">
                        <figcaption class="ratings">
                            <p>Ratings
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                 <span class="fa fa-star-o"></span>
                            </a> 
                            </p>
                        </figcaption>
                    </figure>
                </div>
            </div>            
            
       </div> </a> 
       </div>               
    </div>
  </div>
 <!--  ************************************row 2*********************** -->
   <div class="row" style="margin-top:-20px;">
    <div class="col-md-6" style="margin-top: 30px;">
         <div class="threed">
       <a href="test2.php" style="color:black;"><div class="well profile">
            <div class="col-sm-12">
                <div class="col-xs-12 col-sm-8">
                    <h2 style="font-size:20px;">Nicole Pearson</h2>
                    <p ><strong style="font-size:20px;">About: </strong> Web Designer / UI. </p>
                    <p><strong style="font-size:20px;">Hobbies: </strong> Read, out with friends, listen to music, draw and learn new things. </p>
                    <p><strong style="font-size:20px;">Skills: </strong>
                        <span class="tags">html5</span> 
                        <span class="tags">css3</span>
                        <span class="tags">jquery</span>
                        <span class="tags">bootstrap3</span>
                    </p>
                </div> <br>            
                <div class="col-xs-12 col-sm-4 text-center">
                    <figure>
                        <img src="images/pic.jpg" alt="" class="img-circle img-responsive" style="margin-top: -6px;margin-left:30px;">
                        <figcaption class="ratings">
                            <p>Ratings
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                 <span class="fa fa-star-o"></span>
                            </a> 
                            </p>
                        </figcaption>
                    </figure>
                </div>
            </div>            
            
       </div> </a>
       </div>                
    </div>
   <!--  ***************************2nd********************** -->
   <div class="col-md-6" style="margin-top: 30px;">
     <div class="threed">
      <a href="test2.php" style="color:black;"> <div class="well profile">
            <div class="col-sm-12">
                <div class="col-xs-12 col-sm-8">
                    <h2 style="font-size:20px;">Nicole Pearson</h2>
                    <p ><strong style="font-size:20px;">About: </strong> Web Designer / UI. </p>
                    <p><strong style="font-size:20px;">Hobbies: </strong> Read, out with friends, listen to music, draw and learn new things. </p>
                    <p><strong style="font-size:20px;">Skills: </strong>
                        <span class="tags">html5</span> 
                        <span class="tags">css3</span>
                        <span class="tags">jquery</span>
                        <span class="tags">bootstrap3</span>
                    </p>
                </div> <br>            
                <div class="col-xs-12 col-sm-4 text-center">
                    <figure>
                        <img src="images/pic.jpg" alt="" class="img-circle img-responsive" style="margin-top: -6px;margin-left:30px;">
                        <figcaption class="ratings">
                            <p>Ratings
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                 <span class="fa fa-star-o"></span>
                            </a> 
                            </p>
                        </figcaption>
                    </figure>
                </div>
            </div>            
            
       </div> </a>
       </div>                
    </div>
    <button type="button" class="btn btn-success" style="margin-left:44%;margin-top: 20px;">View More Profiles</button>
</div>

</div>
<script>
$(document).ready(function() {
    var panels = $('.user-infos');
    var panelsButton = $('.dropdown-user');
    panels.hide();

    //Click dropdown
    panelsButton.click(function() {
        //get data-for attribute
        var dataFor = $(this).attr('data-for');
        var idFor = $(dataFor);

        //current button
        var currentButton = $(this);
        idFor.slideToggle(400, function() {
            //Completed slidetoggle
            if(idFor.is(':visible'))
            {
                currentButton.html('<i class="glyphicon glyphicon-chevron-up text-muted"></i>');
            }
            else
            {
                currentButton.html('<i class="glyphicon glyphicon-chevron-down text-muted"></i>');
            }
        })
    });


    $('[data-toggle="tooltip"]').tooltip();

    $('button').click(function(e) {
        e.preventDefault();
        alert("This is a demo.\n :-)");
    });
});

</script>
</body>
</html>
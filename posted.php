<?php require_once ("header.php");?>

<style>
/*.banner{
    background: url("images/work.jpg") no-repeat center;
    background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    -ms-background-size: cover;
    -moz-background-size: cover;
    min-height: 200px;	
    /*margin-top: -60px;	*/


/*.row
{
	width:600px;
	height:auto;
	margin-left:150px;
	background-color: white;
	border:1px solid black;
}*/
/*.col-md-3 {
    margin-top: 100px;
}*/
.jumbotron{
	border-radius: 2px;
	border: 1px solid lightgray;
  height:600px;
	
}
/*img{
	border-radius: :2px;
	border:1px solid lightgray;
	 box-shadow: 5px 5px 5px  #888888;

}*/
.col-md-3{
	border-radius: :2px;

}
.btn {
	margin-left: 830px;
	margin-top:-55px;
	box-shadow: 5px 5px 5px  #888888;
}
/*.form-control{
	box-shadow: 5px 5px 5px  #888888;
}*/
.card {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width:50%;
}

/* On mouse-over, add a deeper shadow */
.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

/* Add some padding inside the card container */
.container {
    padding: 2px 16px;
}
.second{
	margin-top:-230px;
}
#p.container {
     box-shadow: none;
}
a{
  text-decoration: none;
  color:black;
}
</style>
<script>
    // Starrr plugin (https://github.com/dobtco/starrr)
var __slice = [].slice;

(function($, window) {
  var Starrr;

  Starrr = (function() {
    Starrr.prototype.defaults = {
      rating: void 0,
      numStars: 5,
      change: function(e, value) {}
    };

    function Starrr($el, options) {
      var i, _, _ref,
        _this = this;

      this.options = $.extend({}, this.defaults, options);
      this.$el = $el;
      _ref = this.defaults;
      for (i in _ref) {
        _ = _ref[i];
        if (this.$el.data(i) != null) {
          this.options[i] = this.$el.data(i);
        }
      }
      this.createStars();
      this.syncRating();
      this.$el.on('mouseover.starrr', 'span', function(e) {
        return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('mouseout.starrr', function() {
        return _this.syncRating();
      });
      this.$el.on('click.starrr', 'span', function(e) {
        return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('starrr:change', this.options.change);
    }

    Starrr.prototype.createStars = function() {
      var _i, _ref, _results;

      _results = [];
      for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
        _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
      }
      return _results;
    };

    Starrr.prototype.setRating = function(rating) {
      if (this.options.rating === rating) {
        rating = void 0;
      }
      this.options.rating = rating;
      this.syncRating();
      return this.$el.trigger('starrr:change', rating);
    };

    Starrr.prototype.syncRating = function(rating) {
      var i, _i, _j, _ref;

      rating || (rating = this.options.rating);
      if (rating) {
        for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
        }
      }
      if (rating && rating < 5) {
        for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        }
      }
      if (!rating) {
        return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
      }
    };

    return Starrr;

  })();
  return $.fn.extend({
    starrr: function() {
      var args, option;

      option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      return this.each(function() {
        var data;

        data = $(this).data('star-rating');
        if (!data) {
          $(this).data('star-rating', (data = new Starrr($(this), option)));
        }
        if (typeof option === 'string') {
          return data[option].apply(data, args);
        }
      });
    }
  });
})(window.jQuery, window);

$(function() {
  return $(".starrr").starrr();
});

$( document ).ready(function() {
      
  $('#stars').on('starrr:change', function(e, value){
    $('#count').html(value);
  });
  
  $('#stars-existing').on('starrr:change', function(e, value){
    $('#count-existing').html(value);
  });
});

    </script>




<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
	<div class="container">
		<div class="header-left grid">
			<div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
				<h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
			</div>
		</div>
		<div class="header-middle">
			<!-- <ul>
				<li><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>+123 456 7890</li>
				<li><a href="mailto:info@example.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>info@example.com</a></li>
			</ul> -->
			<div class="search">
				<form action="#" method="post">
					<input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
					<input type="submit" value=" ">
				</form>
			</div>
		</div>
		<div class="header-right">
			
			 <ul class="nav navbar-nav menu__list">


						<li ><a href="index.php">LogOut </a></li>
            

          
          </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
					</ul>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

 

<div class="container">

	 <div class="jumbotron">
	 	<h3>Posted Job</h3><br>

	 	<form>
	 		<label for="formGroupExampleInput">Job Title</label>
  <div class="form-group">
    
    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Job Title">
    <button type="button" class="btn btn-default btn-sm">
     	
          <span class="glyphicon glyphicon-pencil"></span> 
         
        </button>
  </div>
  <!-- <label for="formGroupExampleInput">Skills</label><br>
  <label class="form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Technical
</label>

<label class="form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Non-Technical
</label> -->

  <label for="formGroupExampleInput">Skills</label>
  <div class="form-group">
    
    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Skills">
    <button type="button" class="btn btn-default btn-sm">
     	<a href="popup3.php" style="text-decoration:none;">
          <span class="glyphicon glyphicon-pencil" ></span> 
          </a>
        </button>
  </div>
  <label for="formGroupExampleInput">Area</label>
  <div class="form-group">
    
    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Area">
    <button type="button" class="btn btn-default btn-sm">
     
          <span class="glyphicon glyphicon-pencil"></span> 
         
        </button>
  </div>
  <label for="formGroupExampleInput">Price & Duration</label>
  <div class="form-group">
    
    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Price & Duration">
    <button type="button" class="btn btn-default btn-sm">
     
          <span class="glyphicon glyphicon-pencil"></span> 
         
        </button>
  </div>
  <label for="formGroupExampleInput">Job Discription</label>
  <div class="form-group">
    
    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Job Discription">
    <button type="button" class="btn btn-default btn-sm">
     
          <span class="glyphicon glyphicon-pencil"></span> 
         
        </button>
  </div>
</form>
</div>
</div>

<!-- <div class="container">

	 <div class="jumbotron">
	 	<div class="row">
	 		<div class="col-sm-3">
	 	<div class="card">
  <img src="images/team2.jpg" alt="Avatar" style="width:100%">
  <div class="container">
    <h4><b>John Doe</b></h4> 
    <p>Architect & Engineer</p> 
  </div>
</div>
</div>
</div>
	 	
	 	</div>

	 </div> -->
    <!--  <div class="container">

	 <div class="jumbotron">
	 	<div class="second">
	 <div class="row">
  <div class="col-sm-3">
  		<a href="index.php">
  			<div class="card">
  <img src="images/team2.jpg" alt="Avatar" style="width:100%">
  <div class="container">
    
    <p style="font-size:12px;font-weight:bold;">Architect & Engineer</p> 
  </div>
</div>
</a>

  </div>
  <div class="col-sm-3">
  	<a href="index.php">
  			<div class="card">
  <img src="images/team2.jpg" alt="Avatar" style="width:100%">
  <div class="container">
    
    <p style="font-size:12px;font-weight:bold;">Architect & Engineer</p> 
  </div>
</div>
</a>


  </div>
  <div class="col-sm-3"><a href="index.php">
  			<div class="card">
  <img src="images/team2.jpg" alt="Avatar" style="width:100%">
  <div class="container">
    
    <p style="font-size:12px;font-weight:bold;">Architect & Engineer</p> 
  </div>
</div>
</a>

  </div>
  <div class="col-sm-3"><a href="index.php">
  			<div class="card">
  <img src="images/team2.jpg" alt="Avatar" style="width:100%">
  <div class="container">
    
    <p style="font-size:12px;font-weight:bold;">Architect & Engineer</p> 
  </div>
</div>
</a>

  </div>
</div>
</div>
</div>
</div> -->
 <div class="container">

   <div class="jumbotron" style="height:300px;">
    <div class="row" style="margin-top:-40px;">
      <div class="col-md-3">
        <div class="card card-block" style="width: 18rem;height:30px;">
        <img src="images/team2.jpg" alt="images"style="width: 18rem;border:1px solid gray;">
        <p style="font-size:15px;font-weight:bold;margin-left:15px;">Pradeep Yadav</p>
         <div class="row lead">
        
        <div id="stars-existing" class="starrr" data-rating='4' style="margin-left:30px;margin-top:-200px;"></div>
       <a href="postedjob.php"> <p style="margin-left:30px;font-size:12px;font-weight:bold;">Go To Profile</p></a>
        <span id="count-existing"></span> 
    </div>
    

   
  <!-- <a href="#" class="btn btn-primary" style="">Go somewhere</a> -->
</div>
       
      </div>
       <div class="col-md-3">
        <div class="card card-block" style="width: 18rem;height:30px;">
        <img src="images/team2.jpg" alt="images"style="width: 18rem;border:1px solid gray;">
        <p style="font-size:15px;font-weight:bold;margin-left:15px;">Pradeep Yadav</p>
         <div class="row lead">
        
        <div id="stars-existing" class="starrr" data-rating='4' style="margin-left:30px;margin-top:-200px;"></div>
       <a href="postedjob.php"> <p style="margin-left:30px;font-size:12px;font-weight:bold;">Go To Profile</p></a>
        <span id="count-existing"></span> 
    </div>
    

   
  <!-- <a href="#" class="btn btn-primary" style="">Go somewhere</a> -->
</div>
       
      </div>

       <div class="col-md-3">
        <div class="card card-block" style="width: 18rem;height:30px;">
        <img src="images/team2.jpg" alt="images"style="width: 18rem;border:1px solid gray;">
        <p style="font-size:15px;font-weight:bold;margin-left:15px;">Pradeep Yadav</p>
         <div class="row lead">
        
        <div id="stars-existing" class="starrr" data-rating='4' style="margin-left:30px;margin-top:-200px;"></div>
       <a href="postedjob.php"> <p style="margin-left:30px;font-size:12px;font-weight:bold;">Go To Profile</p></a>
        <span id="count-existing"></span> 
    </div>
    

   
  <!-- <a href="#" class="btn btn-primary" style="">Go somewhere</a> -->
</div>
      </div>

       <div class="col-md-3">
         <div class="card card-block" style="width: 18rem;height:30px;">
        <img src="images/team2.jpg" alt="images"style="width: 18rem;border:1px solid gray;">
        <p style="font-size:15px;font-weight:bold;margin-left:15px;">Pradeep Yadav</p>
         <div class="row lead">
        
        <div id="stars-existing" class="starrr" data-rating='4' style="margin-left:30px;margin-top:-200px;"></div>
       <a href="postedjob.php"> <p style="margin-left:30px;font-size:12px;font-weight:bold;">Go To Profile</p></a>
        <span id="count-existing"></span> 
    </div>
    

   
  <!-- <a href="#" class="btn btn-primary" style="">Go somewhere</a> -->
</div>
      </div>

    </div>
    </div>
</div>
</body>
</html>
<?php require_once ("header.php");?>
<head>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css"/>
<style>
body {
  background: #F1F3FA;
}

/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}
 .rating span {
  font-size: 25px;
  cursor: pointer;
  float: right; //remove 'float right' for right to left
}
.rating span:hover, .rating span:hover ~ span{
  color: red; //gold or any other color 
}
.well{
  margin-top: -170px;
  width:50%;
  border: 1px solid lightgray;
  font-size:25px;
}
</style>



</head>
<body>

     <?php require_once ("navigation1.php");?>
    
    
   
<div class="container">
    <div class="row profile">
    <div class="col-md-3">
      <div class="profile-sidebar">
        <!-- SIDEBAR USERPIC -->
        <div class="profile-userpic">
          <img src="images/blank_img.jpg" class="img-responsive" alt="">
        </div>
        <!-- END SIDEBAR USERPIC -->
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
          <div class="profile-usertitle-name">
            Shashi Bahir
          </div>
          <div class="profile-usertitle-job">
            For Work
          </div>
        </div>
        <!-- END SIDEBAR USER TITLE -->
        <!-- SIDEBAR BUTTONS -->
        <div class="profile-userbuttons">
          <a href="#"><button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a>
          <!-- <button type="button" class="btn btn-danger btn-sm">Message</button> -->
        </div>
        <!-- END SIDEBAR BUTTONS -->
        <!-- SIDEBAR MENU -->
        <div class="profile-usermenu">
          <ul class="nav">


            
            <!-- <li class="active">
              <a href="index.php">
              <i class="glyphicon glyphicon-home"></i>
              Logout </a>
            </li> -->
            <li class="active">
              <a href="workprofile.php">
          <i class="glyphicon glyphicon-user"></i>
          Profile
        </a></li>

        <li>
              <a href="workprofile.php">
      <i class="glyphicon glyphicon-user"></i>
          Membership
        </a></li>
         
            <li>
              <a href="#">
          <i class="glyphicon glyphicon-cog"></i>
        
               Settings </a>
            </li>
            <li>
              <a href="#" target="_blank">
              <i class="glyphicon glyphicon-user"></i>
              Invite friends </a>
            </li>
            <li>
              <a href="index.php">
              <i class="glyphicon glyphicon-home"></i>
              Logout </a>
            </li>
          </ul>
        </div>
        <!-- END MENU -->
      </div>
    </div>
    <div class="col-md-9">
            <div class="profile-content">
               <img src="images/hives.png" style="border:1px solid black;"> <strong style="font-size:20px;"> Hives Lab</strong>


    <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Job Title</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
      <!-- *******************row 1 end****************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Category</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>Software engineering & Technology</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
     <!-- **************************row 2 end*************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Skills</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>HTML5 </h4>
        </div>
         <!--  <div class="col-md-5"></div> -->
     </div>
     <!-- ***********************row 3 end********************** -->
      <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Payment</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>XYZ</h4>
        </div>
         <!--  <div class="col-md-5"></div> -->
     </div>
    <!--  *****************************row 4 end******************** -->
     <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Address</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div>
    <!--  ************************* row 5 end************************* -->
     <div class="row" style="margin-top:20px;">
     
      <div class="col-md-2">
     
        <h4>Discription</h4>
      </div> 
        <div class="col-md-6">
      
        <h4>XYZ</h4>
        </div>
          <!-- <div class="col-md-5"></div> -->
     </div><br><hr><br>
   
<div class="container">
  <div class="well" style="margin-top:20px;width:55%;height:150px;">
      <div class="media">
        <a class="pull-left" href="profile1.php">
        <img class="media-object" src="images/blank_img.jpg">
      </a>
      <div class="media-body">
        <h4 class="media-heading">Kalpesh Vasaikar</h4>
          <a href="profile1.php"><p class="text-right" style="font-size: 15px;">Go To Profile</p></a>
        
          <ul class="list-inline list-unstyled">
        <li><span><i class="glyphicon glyphicon-calendar" style="font-size:15px;color:black;"></i><strong style="font-size:17px;"> in 10days</strong> </span></li>
            <li>|</li>
            <span style="font-size:17px;"><strong style="font-size:17px;">500</strong> <i class="fa fa-inr" style="font-size:15px;"></i></span>
            <li>|</li>
            <li>
             <div id="stars-existing" class="starrr" data-rating='4' style="margin-top:-200px;font-size:15px;"></div>
            </li>
           
      </ul>
       </div>
    </div>
  </div>
          

    </div>
   <!--  *****************container1 end*************** -->
   <div class="container">
  <div class="well" style="margin-top:20px;width:55%;height:150px;">
      <div class="media">
        <a class="pull-left" href="profile1.php">
        <img class="media-object" src="images/blank_img.jpg">
      </a>
      <div class="media-body">
        <h4 class="media-heading">Kalpesh Vasaikar</h4>
          <a href="profile1.php"><p class="text-right" style="font-size: 15px;">Go To Profile</p></a>
        
          <ul class="list-inline list-unstyled">
        <li><span><i class="glyphicon glyphicon-calendar" style="font-size:15px;color:black;"></i><strong style="font-size:17px;"> in 10days</strong> </span></li>
            <li>|</li>
            <span style="font-size:17px;"><strong style="font-size:17px;">500</strong> <i class="fa fa-inr" style="font-size:15px;"></i></span>
            <li>|</li>
            <li>
             <div id="stars-existing" class="starrr" data-rating='4' style="margin-top:-200px;font-size:15px;"></div>
            </li>
           
      </ul>
       </div>
    </div>
  </div>
          

    </div>
   <!--  *************container2 end************** -->
   <div class="container">
  <div class="well" style="margin-top:20px;width:55%;height:150px;">
      <div class="media">
        <a class="pull-left" href="profile1.php">
        <img class="media-object" src="images/blank_img.jpg">
      </a>
      <div class="media-body">
        <h4 class="media-heading">Kalpesh Vasaikar</h4>
          <a href="profile1.php"><p class="text-right" style="font-size: 15px;">Go To Profile</p></a>
        
          <ul class="list-inline list-unstyled">
        <li><span><i class="glyphicon glyphicon-calendar" style="font-size:15px;color:black;"></i><strong style="font-size:17px;"> in 10days</strong> </span></li>
            <li>|</li>
            <span style="font-size:17px;"><strong style="font-size:17px;">500</strong> <i class="fa fa-inr" style="font-size:15px;"></i></span>
            <li>|</li>
            <li>
             <div id="stars-existing" class="starrr" data-rating='4' style="margin-top:-200px;font-size:15px;"></div>
            </li>
           
      </ul>
       </div>
    </div>
  </div>
          

    </div>
   <!--  ******************container3 end************** -->
   <div class="container">
  <div class="well" style="margin-top:20px;width:55%;height:150px;">
      <div class="media">
        <a class="pull-left" href="profile1.php">
        <img class="media-object" src="images/blank_img.jpg">
      </a>
      <div class="media-body">
       <h4 class="media-heading">Kalpesh Vasaikar</h4>
          <a href="profile1.php"><p class="text-right" style="font-size: 15px;">Go To Profile</p></a>
        
          <ul class="list-inline list-unstyled">
        <li><span><i class="glyphicon glyphicon-calendar" style="font-size:15px;color:black;"></i><strong style="font-size:17px;"> in 10days</strong> </span></li>
            <li>|</li>
            <span style="font-size:17px;"><strong style="font-size:17px;">500</strong> <i class="fa fa-inr" style="font-size:15px;"></i></span>
            <li>|</li>
            <li>
             <div id="stars-existing" class="starrr" data-rating='4' style="margin-top:-200px;font-size:15px;"></div>
            </li>
           
      </ul>
       </div>
    </div>
  </div>
          

    </div>
   <!--  ******************container 4 end****************** -->
  </div>
</div>
</div>
</div>
</div>

  <script>
    // Starrr plugin (https://github.com/dobtco/starrr)
var __slice = [].slice;

(function($, window) {
  var Starrr;

  Starrr = (function() {
    Starrr.prototype.defaults = {
      rating: void 0,
      numStars: 5,
      change: function(e, value) {}
    };

    function Starrr($el, options) {
      var i, _, _ref,
        _this = this;

      this.options = $.extend({}, this.defaults, options);
      this.$el = $el;
      _ref = this.defaults;
      for (i in _ref) {
        _ = _ref[i];
        if (this.$el.data(i) != null) {
          this.options[i] = this.$el.data(i);
        }
      }
      this.createStars();
      this.syncRating();
      this.$el.on('mouseover.starrr', 'span', function(e) {
        return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('mouseout.starrr', function() {
        return _this.syncRating();
      });
      this.$el.on('click.starrr', 'span', function(e) {
        return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('starrr:change', this.options.change);
    }

    Starrr.prototype.createStars = function() {
      var _i, _ref, _results;

      _results = [];
      for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
        _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
      }
      return _results;
    };

    Starrr.prototype.setRating = function(rating) {
      if (this.options.rating === rating) {
        rating = void 0;
      }
      this.options.rating = rating;
      this.syncRating();
      return this.$el.trigger('starrr:change', rating);
    };

    Starrr.prototype.syncRating = function(rating) {
      var i, _i, _j, _ref;

      rating || (rating = this.options.rating);
      if (rating) {
        for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
        }
      }
      if (rating && rating < 5) {
        for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        }
      }
      if (!rating) {
        return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
      }
    };

    return Starrr;

  })();
  return $.fn.extend({
    starrr: function() {
      var args, option;

      option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      return this.each(function() {
        var data;

        data = $(this).data('star-rating');
        if (!data) {
          $(this).data('star-rating', (data = new Starrr($(this), option)));
        }
        if (typeof option === 'string') {
          return data[option].apply(data, args);
        }
      });
    }
  });
})(window.jQuery, window);

$(function() {
  return $(".starrr").starrr();
});

$( document ).ready(function() {
      
  $('#stars').on('starrr:change', function(e, value){
    $('#count').html(value);
  });
  
  $('#stars-existing').on('starrr:change', function(e, value){
    $('#count-existing').html(value);
  });
});

    </script>

    

    
     </body>
     </html>
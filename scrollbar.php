<!DOCTYPE html>
<html>
<head>
	<style>
	header
{
	font-family: 'Lobster', cursive;
	text-align: center;
	font-size: 25px;	
}

#info
{
	font-size: 18px;
	color: #555;
	text-align: center;
	margin-bottom: 25px;
}

a{
	color: #074E8C;
}

.scrollbar
{
	margin-left: 30px;
	float: left;
	height: 300px;
	width: 65px;
	background: #F5F5F5;
	overflow-y: scroll;
	margin-bottom: 25px;
}

.force-overflow
{
	min-height: 450px;
}

#wrapper
{
	text-align: center;
	width: 500px;
	margin: auto;
}

/*
 *  STYLE 2
 */

#style-2::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}

#style-2::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
}

#style-2::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: black;
}
.popup {
		display: none;
		background-color: #FFFFFF;
		position: fixed;
		top: 150px;
		padding: 40px 25px 25px 25px;
		width: 350px;
		z-index: 999;
		left: 50%;
		margin-left: -200px;
	}
	
	.pop_overlay{
		height: 100%;
		width: 100%;
		background-color: #F6F6F6;
		opacity: 0.9;
		position: fixed;
		z-index: 998;
	}
	
	a.close{
		color: #999;
		text-decoration: none;
		position: absolute;
		right: 15px;
		top: 15px;
	}



	</style>
	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		// Show the popup on click
		$('#show_pop1, #show_pop2').on('click', function (e) {
			$('body').prepend('<div class="pop_overlay"></div>');
			if ($(this).attr('id') == 'show_pop1') 
				$('#popup1').fadeIn(500);
			else 
				$('#popup2').fadeIn(500);
			e.preventDefault();
		});		
		
		// Open popup from link inside another popup
		$('#pop1, #pop2').on('click', function (e) {
			toFadeOut = $('#popup1');
			toFadeIn = $('#popup2');
			if ($(this).attr('id') == 'pop1') {
				toFadeOut = $('#popup2');
				toFadeIn = $('#popup1');
			}
			toFadeOut.fadeOut(500, function () {
				toFadeIn.fadeIn();
			})
			return false;
		});
		
		// Close popup
		$(document).on('click', '.pop_overlay, .close', function () {
			$('#popup1, #popup2').fadeOut(500, function () {
				$('.pop_overlay').remove();
			});
			return false;
		});		
	});
</script>

</head>
<body>
	<form>
	<a id="show_pop1" href="" style="text-decoration:none;"> <label class="radio-inline">
      <input type="radio" name="optradio">Technical
    </label></a> | <a id="show_pop2" href="" style="text-decoration:none;"><label class="radio-inline">
      <input type="radio" name="optradio">Non-Technical
    </label>
    </a>
</form>
	 <div class="scrollbar" id="style-2">
      <div class="force-overflow">
      	<div id="popup1" class="popup" style="width:auto;height:auto;">
    <h3><a id="pop2" href="">list</a></h3>
     <div class="row">
        <div class="col-lg-12 col-lg-offset-12;">
            <input type="search" id="container-search" value="" class="form-control" placeholder="Search..." style="margin-left:-10px;width:550px;">
        </div>
    </div>
    <hr/>
    <!-- <h1 align="center">POPUP 1</h1>	
    <p align="justify">This is first popup. Place your content here.</p> -->
    <div class="row" style="margin-top:30px;margin-left:10px;">
  <div class="col-lg-3" style="border:2px solid lightgray;width:170px;height:100px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4>Website Development
    </h4></a>
  </div>
  <div class="col-lg-3" style="border:2px solid lightgray;width:170px;height:100px;margin-left:32.5%;margin-top: -19.7%;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4>Website Design
    </h4></a>
  </div>
  <div class="col-lg-3" style="border:2px solid lightgray;width:170px;height:100px;margin-left:65%;margin-top: -19.7%;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4>Website Design
    </h4></a>
  </div>
  </div>
  <div class="row" style="margin-top:30px;margin-left:10px;">
  <div class="col-lg-3" style="border:2px solid lightgray;width:170px;height:100px;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4>Website Development
    </h4></a>
  </div>
  <div class="col-lg-3" style="border:2px solid lightgray;width:170px;height:100px;margin-left:32.5%;margin-top: -19.7%;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4>Website Design
    </h4></a>
  </div>
  <div class="col-lg-3" style="border:2px solid lightgray;width:170px;height:100px;margin-left:65%;margin-top: -19.7%;">
    <a id="pop2"><center><img src="images/k1.png" width="50px"style="margin-top:5px;"></center>
    <h4>Website Design
    </h4></a>
  </div>
  </div>
    <a class="close" href="">[ x ]</a>
</div>
<div id="popup2" class="popup">
    <h3><a id="pop1" href=""> Back to category</a></h3>
    <hr />
    <h1 align="center">POPUP 2</h1>	
    <p align="justify">This is second popup. Place your content here.</p>
    <a class="close" href="">[ x ]</a>    
</div>

      </div>
    </div>

	</body>
	</html>
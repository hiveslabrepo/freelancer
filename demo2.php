<?php require_once ("header.php");?>
<body>

<div class="container">
  <div class="container">
    <div class="header-left grid">
      <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
        <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
      </div>
    </div>
    <div class="header-middle">
      <!-- <ul>
        <li><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>+123 456 7890</li>
        <li><a href="mailto:info@example.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>info@example.com</a></li>
      </ul> -->
      <div class="search">
        <form action="#" method="post">
          <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
          <input type="submit" value=" ">
        </form>
      </div>
    </div>
    <div class="header-right">
      <!-- <div class="search">
        <form action="#" method="post">
          <input type="search" name="Search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
          <input type="submit" value=" ">
        </form>
      </div> -->
       <ul class="nav navbar-nav menu__list">
            <li class="active menu__item menu__item--current" class="page-scroll" a href="#portfolio" data-toggle="modal" data-target="#login"><a href="#portfolio" data-toggle="modal" data-target="#login">Login <span class="sr-only">(current)</span></a></li>
            

            <li class=" menu__item"><a href="#portfolio" data-toggle="modal" data-target="#register">Sign Up</a></li>
          </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
 <div class="container">
  <!-- Modal -->
  <div class="modal fade" id="register" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Get Started</h4>
          <img src="images/login.jpg" height="60" width="60">
        </div>
        <div class="modal-body">
      
      <form method="post" action="loginins.php">
      <div class="form-group">
      <label for="name">Name:</label>
      <input type="text" class="form-control" id="name">
    </div>
    <div class="form-group">
      <label for="lastname">Last Name:</label>
      <input type="text" class="form-control" id="last">
    </div>
 
      <div class="form-group">
     <label for="email">Email:</label>
     <input type="text" class="form-control" id="mail">
    </div>
    
     <div class="form-group">
     <label for="email">Password:</label>
     <input type="password" class="form-control" id="password">
    </div>

    <div class="container">
      <button type="Submit" name="submit"class="btn btn-primary">Create Account</button>
       
        <input type="hidden" name="MM_insert" value="frmworkmaster" />
   </div> 
  </form>
     </div>
        <div class="modal-footer">
          <button type="Submit" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

  <div class="container">
  <!-- Modal -->
  <div class="modal fade" id="login" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">User Login</h4>
          <img src="images/login.jpg" height="60" width="60">
        </div>
        <div class="modal-body">
      
      <form method="post" action="loginins.php">
      
  <div class="radio">
  <label><input type="radio" name="optradio"><strong>Company</strong></label>
  </div>
  <div class="radio">
  <label><input type="radio" name="optradio"><strong>Individuls<strong></label>
  </div>
  
    <div class="form-group input-group">
          <span class="input-group-addon">
             <i class="glyphicon glyphicon-user"></i>
          </span>
          <input class="form-control" placeholder="username" name="username" type="name" required="">
        </div>
    
      <div class="form-group input-group">
          <span class="input-group-addon">
            <i class="glyphicon glyphicon-lock">
            </i>
          </span>
          <input class="form-control" placeholder="Password" name="password" type="password" value="" required="">
        </div>
    
     
     <div class="checkbox">
      <label><input type="checkbox" value="">Remember Me</label>
    </div>
         <p><a href="">Forget Password</a></p>
 
    <div class="container">
      <button type="Submit" name="submit"class="btn btn-primary">Login</button>
        <button type="button" class="btn btn-primary">Cancel</button>
        <input type="hidden" name="MM_insert" value="frmworkmaster" />
   </div> 
  </form>
     </div>
        <div class="modal-footer">
          <button type="Submit" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<div class="banner">
 
  
  <h2>Modal Example</h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

</body>
</html>


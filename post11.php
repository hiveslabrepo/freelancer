<?php require_once ("header.php");?>

<style>
body{
  background-image: url("images/bg5.jpg");
}
.banner{
    background: url("images/bg2.jpg") no-repeat center;

    background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    -ms-background-size: cover;
    -moz-background-size: cover;
    min-height: 1000px;      
}
.navbar-nav > li > a{
    font-size: 15px;
}

.caption {
        width:100%;
        bottom: .3rem;
        position: absolute;
        background:#000;
        background: -webkit-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: -moz-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: -o-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: linear-gradient(to top, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
    }

    .thumbnail {
        border: 0 none;
        box-shadow: none;
        margin:0;
        padding:0;
    }

    .caption h4 {
        color: #fff;
        -webkit-font-smoothing: antialiased;
         margin-top:-400px;
         margin-left:500px;
    }
    
    .col-md-3 h2{
        margin-top:160px;
        
    }
   .container .well {
    margin-top:100px;

    }
   /* .control-label{
        margin-right: 10px;
    }*/
    .row{
            margin-top: -50px;
            /*margin-left: 500px;*/
    }
    select{
            width: 250px;
    }
    label{
      margin:-25px;
    }
    strong{
      font-size: 20px;
    }
    .add-photo-btn1{
   position:relative;
   overflow:hidden;
   cursor:pointer;
   text-align:center;
   background-color:#428bca;
   color:#fff;
   display:block;
   width:180px;
   height:28px;
   font-size:12px;
   line-height:30px;
   float:right;
   margin-right: 260px;
   margin-top: 1px;
 }
 .form input[type="file"]{
  z-index: 999;
  line-height: 0;
  font-size: 50px;
  position: absolute;
  opacity: 0;
  filter: alpha(opacity = 0);-ms-filter: "alpha(opacity=0)";
  cursor: pointer;
  _cursor: hand;
  margin: 0;
  padding:0;
  left:0;

  }

     
    
</style>
<body>
<div class="header wow fadeInDown animated" data-wow-delay=".5s">
    <div class="container">
        <div class="header-left grid">
            <div class="grid__item color-1 wow zoomIn" data-wow-duration="2s" data-wow-delay="0.5s">
                <h1><a href="index.html"><i></i><span class="link link--kukuri" data-letters="Work To Finish">Work To Finish</span></a></h1>
            </div>
        </div>
        <div class="header-middle">
           
        </div>
        <div class="header-right">
          
             <ul class="nav navbar-nav menu__list">


                      
          </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
                    </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
 <div class="banner">
     
   <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">

<div class="well well-lg" style="margin-top:50px;">
<div class="container-fluid text-center">
  <div class="row content">
    <div class="col-sm-10 text-left">
      <h1 style="font-size:25px;">Create and post your work brief:</h1>
      <div class="container">
      
 <form>
    <div class="form-group">
   <div class="col-xs-7">
  <label for="skill"><h3><strong style="font-size:18px;">Works of:</strong></h3></label><br>
  <div class="radio">
  <label><input type="radio" name="optradio" id="company"><h4><strong style="font-size:18px;">Company</strong></h4></label>
  </div>
  <div class="radio">
  <label><input type="radio" name="optradio" id="individualid"><h4><strong style="font-size:18px;">Individuls<strong></h4></label>
  </div>
  </div>
  
      <div class="form-group">
    <div class="col-xs-7">
       <label for="pwd"><h3><strong style="font-size:18px;">User Name:</strong></h3></label> 
     <input type="text" class="form-control" placeholder="user name"id="usr"><br>
    </div>
  </div>
  
      <div class="form-group">
    <div class="col-xs-7">
       <label for="pwd"><h3><strong style="font-size:18px;">Password:</strong></h3></label> 
     <input type="password" class="form-control" placeholder="password"id="password"><br>
    </div>
  </div>
  
      
     <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">Contact Number:</strong></h3></label>
        <input type="text" class="form-control" id="mobile"><br>
    </div>
    </div>

     <div class="form-group" id="cmpnyid">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">Company Name:</strong></h3></label>
        <input type="text" class="form-control" id="mobile"><br>
    </div>
    </div>
    
     <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">Email:</strong></h3></label>
        <input type="text" class="form-control" placeholder="e.g. abc@xyz.com" id="email"><br>
    </div>
    </div>
    <div class="form-group">
    <div class="col-xs-7">
    <label for="pwd"><h3><strong style="font-size:18px;">Work Name:</strong></h3></label> 
      <input type="text" class="form-control" placeholder="Name of Project"id="usr"><br>
    </div>
  </div>
  
   <div class="form-group">
    <div class="col-xs-7">
      <label for="project_type"><h3><strong style="font-size:18px;">Work Type:</strong></h3></label></label>
      <div class="radio">
      <label><input type="radio" name="optradio"><h4>Technical</h4></label>&nbsp;
      <label style="margin-left:20px;"><input type="radio" name="optradio"><h4>Non technical</h4></label>
    </div>
       <select class="form-control" id="sel1">
        <option>Website IT & Software</option>
        <option>Mobile</option>
        <option>Writing</option>
        <option>Design</option>
        <option>Data Entry</option>
      </select>
      
      
    </div>
    </div>
    
  
     <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">Skills Needed:</strong></h3></label>
        <input type="text" class="form-control" placeholder="e.g. Java,php,Sql" id="skills"><br>
    </div>
    </div>

  
     <div class="form-group">
    <div class="col-xs-7">
    <label for="budget"><h3><strong style="font-size:18px;">Budget:</strong></h3></label>
    <select class="form-control" id="sel1">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select><br>
    </div>
    </div>
    
     <div class="form-group">
    <div class="col-xs-7">
    <label for="skill"><h3><strong style="font-size:18px;">CIN No.:</strong></h3></label>
    <input type="text" class="form-control" id="mobile"><br>
    </div>
    </div>
    
     <div class="form-group">
    <div class="col-xs-7">
      <label for="discription"><h3><strong style="font-size:18px;">Descriptions:</strong></h3></label>
      <div class="form-group">
      <textarea class="form-control" rows="5" id="comment" placeholder="Describe your project here....."></textarea>
    </div>
    </div>
  
      <div class="form-group">
      <div class="col-xs-7">
      <span class="icon-attach"></span>
     
      <p><span class="glyphicon glyphicon-paperclip"></span><strong style="font-size:15px;">Attach File related to Project:</strong></p>
     <input type="text" id="path" placeholder="Select File" style="font-size:15px;" />
  <label class="add-photo-btn1">Browse
    <span>
       <input type="file" id="myfile" name="myfile" />
    </span>
    </label><br><br>
    </div>
    </div>
  
   <div class="form-group">
        <div class="col-xs-7">
     <a href="jobpostprofile.php"> <button type="button" class="btn btn-primary btn-lg">Submit</button></a>
    </div>
    </div>
  </form>
  
   <div class="col-md-2"></div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-3">
                           
</div>
    
    </div>
    <script type="text/javascript">
 window.onload = function() {
    document.getElementById('ifYes').style.display = 'none';
    document.getElementById('ifNo').style.display = 'none';
}
function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.display = 'block';
        document.getElementById('ifNo').style.display = 'none';
        document.getElementById('redhat1').style.display = 'none';
        document.getElementById('aix1').style.display = 'none';
    } 
    else if(document.getElementById('noCheck').checked) {
        document.getElementById('ifNo').style.display = 'block';
        document.getElementById('ifYes').style.display = 'none';
        document.getElementById('redhat1').style.display = 'none';
        document.getElementById('aix1').style.display = 'none';
   }
}
function yesnoCheck1() {
   if(document.getElementById('redhat').checked) {
       document.getElementById('redhat1').style.display = 'block';
       kdocument.getElementById('aix1').style.display = 'none';
    }
   if(document.getElementById('aix').checked) {
       document.getElementById('aix1').style.display = 'block';
       document.getElementById('redhat1').style.display = 'none';
    }
}
$(function(){
    $("#cmpnyid").hide();
    $("#company").on("click", function(){
        $("#cmpnyid").show();

        $("#individualid").on("click", function(){
        $("#cmpnyid").hide();

        });
    });
});
</script>
   </body>
    </html>
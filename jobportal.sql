-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 11, 2016 at 08:42 AM
-- Server version: 5.5.20
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jobportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `comp_reg`
--

CREATE TABLE IF NOT EXISTS `comp_reg` (
  `cin` bigint(50) NOT NULL,
  `cname` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phno` bigint(10) NOT NULL,
  `addr` varchar(400) NOT NULL,
  `brn` varchar(100) NOT NULL,
  `no` int(10) NOT NULL,
  `ind` varchar(100) NOT NULL,
  `comp_desc` varchar(500) NOT NULL,
  `proof` varchar(1000) NOT NULL,
  PRIMARY KEY (`cin`),
  UNIQUE KEY `UNIQUE` (`phno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comp_reg`
--

INSERT INTO `comp_reg` (`cin`, `cname`, `pass`, `email`, `phno`, `addr`, `brn`, `no`, `ind`, `comp_desc`, `proof`) VALUES
(1, 'Wipro', 'wipro', 'wipro123@gmail.com', 8855221144, 'phase2 hinjewadi pune 411017', '2000', 500, 'Telecommunication', 'IT Industry', ''),
(2, 'TCS', 'tcs', 'TSC@gmail.com', 9874563210, 'phase2 last buliding hinjewadi 411013', '1998', 200, 'Telecommunication', 'IT Industry', ''),
(3, 'Atos', 'atos', 'Atos123@gmail.com', 9856321401, 'phase3 bluerich buliding hinjewadi 411011', '1995', 30000, 'Telecommunication', 'IT Industry', ''),
(4, 'Tech-mahidra', 'tech123', 'techmahindra@gmail.com', 9874562111, 'phase2 second last area hinjewadi pune 411056', '2001', 4000, 'java', 'IT Industry', ''),
(5, 'e-zest', 'zest', 'ezest@gmail.com', 1234567890, 'phase3 hinjwadi bluerich bulieing b5 612', '1985', 2000, 'Telecommunication', 'IT Industry', ''),
(6, 'motherson sumi Ltd', 'motherson', 'mothersin@gmail.com', 1234564321, 'Delhi:- 06', '2005', 10000, 'cpp', 'IT Industry', ''),
(7, 'sapient', 'sapient', 'sapient@gmail.com', 1234432156, 'bangolore', '2000', 10000, 'cpp', '', ''),
(8, 'fujisto', 'fujisto', 'fujisto@gmail.com', 1232112345, 'dehu pimpri chinchwad pune 411011 ', '1995', 10000, 'Telecommunication', 'IT Industry', ''),
(9, 'intel', 'intel', 'intel@gmail.com', 34566543, 'bangolore', '1996', 40000, 'c', 'IT Industry', ''),
(10, 'cisco', 'cisco', 'cisco@gmail.com', 34565432345, 'channai', '1997', 5000, '', 'IT Industry', ''),
(11, 'sksca', 'sksca', 'sksca@zxc.com', 45665434567, 'pune', '1998', 60000, 'Telecommunication', 'IT Industry', ''),
(12, 'vodaphone', 'vodaphone', 'vodaphone@zxc.com', 56787654, 'dehu pune ', '1991', 70000, 'python', 'IT Industry', ''),
(13, 'airtel', 'airtel', 'airtel@zxc.com', 2345678, 'delhi:-07', '2003', 10000, 'Telecommunication', 'IT Industry', ''),
(14, 'mercury', 'mercury', 'mercury@zxc.com', 123443212, 'bangolore', '2001', 1000, 'java', 'IT Industry', ''),
(15, 'hetero', 'hetero', 'hetero@zxc.com', 456765, 'hydrabad', '2004', 3000, 'Telecommunication', 'IT Industry', ''),
(16, 'BHEL', 'bhel', 'bhel@zxc.com', 12345676, 'bangalor', '1980', 100000, '.net', 'IT Industry', ''),
(17, 'billiontags', 'billiontags', 'billiontags@zxc.com', 1234543, 'chennai', '1979', 200000, '.net', 'IT Industry', ''),
(18, 'target', 'target', 'target@zxc.com', 123, 'bangalore', '1980', 10000, 'Telecommunication', 'IT Industry', ''),
(19, 'creatiosoft', 'creatiosoft', 'creatiosoft@zxc.com', 1234, 'noida', '1984', 200000, 'java', 'IT Industry', ''),
(20, 'smart', 'smart', 'smart@zxc.com', 1234576543, 'delhi', '2007', 100000, '.net', 'IT Industry', ''),
(21, 'feltso', 'feltso', 'feltso@gmail.com', 876543, 'hydrabad', '2010', 200, 'python', 'IT Industry', ''),
(22, 'IDC', 'idc', 'idc@zxc.com', 5433, 'bangolore', '1993', 10000, 'Telecommunication', 'IT Industry', ''),
(23, 'HCL', 'hcl', 'hcl@zxc.com', 344344, 'noida', '1990', 30006, 'java', 'IT Industry', ''),
(24, 'henkel', 'henkel', 'henkel@zxc.com', 22322322322, 'mumbai', '2011', 123456, '.net', 'IT Industry', ''),
(25, 'joda', 'joda', 'joda@zxc.com', 654465434, 'pune', '1995', 20000, '.net', 'IT Industry', ''),
(26, 'capgemini', 'capgemini', 'capgemini@zxc.com', 5675456, 'noida', '2000', 3000, 'java', 'IT Industry', ''),
(27, 'global edify', 'global', 'globali@zxc.com', 12321232232, 'bangalore', '2010', 300, 'java', 'IT Industry', ''),
(28, 'leo technosoft', 'leo', 'leo@zxc.com', 54454, 'pune', '2001', 400, 'cpp', 'IT Industry', ''),
(29, 'telebrahama', 'tele', 'telebrahama@zxc.com', 766566, 'west bangol', '2002', 3000, 'c', 'IT Industry', ''),
(30, 'CEB', 'ceb', 'ceb@zxc.com', 434343, 'gurgone', '2000', 1000, 'python', 'IT Industry', ''),
(31, 'Bharat Electrinics limited', 'bharat', 'bharat@zxc.com', 8446121210, 'pashan pune', '2003', 20000, 'c', 'IT Industry', ''),
(32, 'Sterlite Technologies ltd', 'sterlite', 'sterlitezxc@gmail.com', 2027272227, 'Magarpatta', '2004', 30000, 'cpp', 'IT Industry', ''),
(33, 'bajaj Electrical ltd', 'bajaj', 'bajajzxc@gmail.com', 7755669988, 'camp pune', '2005', 20000, 'DBMS', 'IT Industry', ''),
(34, 'Wpro lighting', 'wipro', 'wiprzxc@gmail.com', 8963254170, 'Ganesh peth pune', '1999', 40000, 'cpp', 'IT Industry', ''),
(35, 'Aar em Electronics', 'aar', 'aarzxc@gmail.com', 2060605255, 'Hadapsar pune', '1998', 40000, 'java', 'IT Industry', ''),
(36, 'Aeron System pvt ltd', 'aeron', 'aeron@zxc.com', 9638527412, 'Baner-Mhalunge road pune', '2000', 2000, 'vb', 'IT Industry', ''),
(37, 'G.k Energy marketers pvt ltd', 'g.k', 'g.k@zxc.com', 8523697414, 'sadashiv peth pune', '2002', 4000, '.net', 'IT Industry', ''),
(38, 'Sensography Inc', 'sensography', 'sensography@zxc.com', 7412589630, 'MIDC,Bhosari pune', '2003', 10000, 'python', 'IT Industry', ''),
(39, 'Sansui Electronics pvt ltd', 'sansui', 'sansui@zxc.com', 7896541230, 'Aundh pune', '2004', 4000, 'c', 'IT Industry', ''),
(40, 'WYSE Biometrics System pvt ltd', 'WTSE', 'WYSE@zxc.com', 8975642310, 'Aranyeshwar pune', '2005', 10000, 'php', 'IT Industry', ''),
(41, 'hill  pvt ltd', 'hill', 'hill@gmail.com', 8520369741, 'muhamadwadi jalgoan', '1999', 1002, 'Managment', 'IT Industry', ''),
(42, 'zxc', 'zxc', 'zxc@gmail.com', 1212121212, 'abc northway 411017', '2010', 400, 'java', 'IT Industry', ''),
(44, 'pratap@gmail.com', 'pratap', 'igate@gmail.com', 6757, 'jkhj', '2000', 12000, 'cpp', 'IT Industry', ''),
(45, 'Onida', 'onida', 'onida@gmail.com', 8520963741, 'ramjangoan shirur pune', '1980', 50000, 'Electronic', 'IT Industry', ''),
(47, 'LG', 'lg', 'lg@gmail.com', 121212123, 'lg near ranjangone', '1999', 30000, 'Electronic', 'IT Industry', ''),
(81, 'POR ', 'pqr', 'pqr@zxc.com', 12122343, 'abc  near pune 411017', '2000', 20000, 'java', 'our companay worked on hyber net ,stud ,spring', ''),
(96, 'rakesh pvt ltd', 'rakesh', 'rakesh@gmail.com', 8503666, 'near gahunje pune', '2014', 500, 'IT', 'our companay worked on hyber net ,stud ,spring', ''),
(963, 'sammy pvt ltd', 'sammy', 'sammy@gmail.com', 8521479630, 'pimpri pune 412266', '2014', 652, 'IT', 'our companay worked on hyber net ,stud ,spring', ''),
(1234567890, 'RAHUL pvt ltd', 'rahul', 'rahul@zxc.com', 12121234, 'near khandoba mal akurdi pune 411044', '2013', 300, 'INFORMATION TECHNOLOGY', 'our companay worked on hyber net ,stud ,spring', '');

-- --------------------------------------------------------

--
-- Table structure for table `comp_req`
--

CREATE TABLE IF NOT EXISTS `comp_req` (
  `crno` int(10) NOT NULL AUTO_INCREMENT,
  `cname` varchar(100) NOT NULL,
  `vtype` varchar(20) NOT NULL,
  `vacancy` int(10) NOT NULL,
  `deg` varchar(100) NOT NULL,
  `skill1` varchar(50) NOT NULL,
  `skill2` varchar(50) NOT NULL,
  `skill3` varchar(50) NOT NULL,
  `ldate` varchar(50) NOT NULL,
  `salary` varchar(500) NOT NULL,
  PRIMARY KEY (`crno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `comp_req`
--

INSERT INTO `comp_req` (`crno`, `cname`, `vtype`, `vacancy`, `deg`, `skill1`, `skill2`, `skill3`, `ldate`, `salary`) VALUES
(1, 'tech mahindra', 'Fresher', 10, 'B.E', 'Developer', 'tesing', 'networking', '12-Nov-2016', '1.8 PA'),
(2, 'TCS', 'Fresher', 10, 'MCA', 'Developer', 'tesing', 'networking', '11-Nov-2016', '2.0 PA'),
(3, 'wipro', 'Experianced', 20, 'M.Tech', 'design', 'marketing', 'chip coding', '12-Dec-2016', '2.2 PA'),
(4, 'global edify', 'Experianced', 5, 'M.E', 'desing ', 'developer', 'chip coding', '23-Oct-2016', '1.6 PA'),
(5, 'e-zest', 'Experianced', 15, 'M.Tech', 'desing', 'marketing', 'networking', '11-Nov-2016', '2.0 PA'),
(6, 'motherson sumi Ltd', 'Experianced', 5, 'BCA', 'sap', 'gates', '--------', '3-Nov-2016', '2.4 PA'),
(7, 'sapient', 'Fresher', 2, 'BCS', 'html5', 'css', '--------', '20-Oct-2016', '2.2 PA'),
(8, 'fujisto', 'Fresher', 30, 'M.Tech', 'sap', 'technical support', '--------', '13-Nov-2016', '2.2 PA'),
(9, 'intel', 'Experianced', 5, 'B.E', 'managerial', 'testing', '--------', '17-Oct-2016', '2.6 PA'),
(10, 'cisco', 'Fresher', 6, 'BCS', 'sap', 'zxc', 'abc', '21-Oct-2016', '2.2 PA'),
(11, 'sksca', 'Experianced', 12, 'B.Tech', 'marketing', 'sales', '--------', '25-Dec-2016', '3.0 PA'),
(12, 'vodaphone', 'Experianced', 12, 'BCA', 'communication', 'front office', '------', '18-Dec-2016', '1.8 PA'),
(13, 'airtel', 'Experianced', 11, 'M.E', 'GGG', 'FFF', '--------', '1-Nov-2016', '2.0 PA'),
(14, 'mercury', 'Experianced', 1, 'B.E', 'managerial', 'Assembling', '--------', '25-Dec-2016', '2.2 PA'),
(15, 'hetero', 'Fresher', 25, 'B.E', 'testing', 'logic gates', '--------', '25-Dec-2016', '3.0 PA'),
(16, 'TCS', 'Fresher', 11, 'B.E', 'developer', 'design', '', '1-Dec-2016', '2.6 PA'),
(17, 'Atos', 'Fresher', 12, 'M.E', 'Business devlope manager', 'Customer service representative', '', '1-Dec-2016', '2.6 PA'),
(18, 'TCS', 'Fresher', 5, 'M.E', 'Manager product', 'Tool room suppervisor', '', '1-Dec-2017', '2.8 PA'),
(21, 'e-zest', 'Fresher', 10, 'B.E', 'Tool room suppervisor', 'Account & business devlopement manager', '', '1-Dec-2016', '1.6 PA'),
(22, 'TCS', '5-7', 10, 'M.Tech', 'Business devlope manager', 'Customer service representative', '', '10-Dec-2016', '1.8 PA'),
(23, 'TCS', '2-4', 12, 'B.E', 'Engineer machanical', 'Engineer quality', '', '20-Nov-2016', ''),
(24, 'Wipro', 'Fresher', 5, 'M.Tech', 'Engineer project', 'Manager product', '', '5-Dec-2016', '');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `fid` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phno` bigint(10) NOT NULL,
  `msg` varchar(1000) NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`fid`, `name`, `email`, `phno`, `msg`) VALUES
(2, 'vishal more', 'vishal.more@gmail.com', 1234567890, 'nice to get job'),
(3, 'vidyashree mulay', 'vidya@gmail.com', 1234567891, 'good feedback from me');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `uname` varchar(40) NOT NULL,
  `pname` varchar(50) NOT NULL,
  `email` varchar(40) NOT NULL,
  `pass` varchar(14) NOT NULL,
  `mbno` bigint(10) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `pname` (`pname`),
  UNIQUE KEY `UNIQUE` (`email`),
  UNIQUE KEY `mbno` (`mbno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`uid`, `uname`, `pname`, `email`, `pass`, `mbno`) VALUES
(1, 'vishal more', 'vish', 'vishal.more45@gmail.com', 'vishal', 9552022326),
(2, 'jaydeep more', 'jay', 'jay.145@gmail.com', 'jaydeep', 8087554466),
(3, 'Suraj Kharote', 'sur', 'suraj124@gmail.com', 'suraj1', 8449121214),
(4, 'Rahul Surya', 'rs', 'rskumar@gmail.com', 'rskumar', 7276049173),
(8, 'shridhar Mulay', 'shree', 'shree5@gmail.com', 'shridhar', 9874563210),
(9, 'Kiran Jadhav', 'kiranJ', 'kiran123@gmail.com', 'kiran', 9898956541),
(11, 'Ajit Jadhav', 'AjitJ', 'Ajit15@gmail.com', 'ajit', 9898989898),
(18, 'Ankit Chodhari', 'creator', 'ankit.poo@gmail.com', 'ankit', 7239123456),
(19, 'Vinayak More', 'vinu', 'vinayak17587@gmail.com', 'vinayak', 8805414356),
(20, 'vidyashri mulay', 'vidya', 'vidya@gmail.com', 'vidyashri', 1234567896),
(21, 'pooja naik', 'poo', 'pooja@gmail.com', 'pooja', 12345676),
(22, 'nikita sonavane', 'niki', 'niki@gmail.com', 'niki', 1234432),
(23, 'Shashikant kumbhar', 'shashi', 'shashi009@gmal.com', 'shashi', 9638527410),
(24, 'yogesh  shinde', 'yog', 'shindeyogi@gmail.com', 'yog', 7796584120),
(25, 'manjushree deshpande', 'manju', 'md123@gmail.com', 'manju', 9011595959),
(26, 'pankaj bhokse', 'panki', 'pankaj@gmail.com', 'pankaj', 1231231231),
(27, 'Aniket bahir', 'Andy', 'aniket007@gmail.com', 'aniket', 9552022323),
(28, 'ramesh yadav', 'ram', 'rammu@gmail.com', 'ramesh', 9011586978),
(29, 'pratap surway', 'pappu', 'pratap@gmail.com', 'pratap', 123451234),
(30, 'sujata singh', 'chintu', 'sujata@gmail.com', 'suata', 7788992200),
(31, 'dhiraj dushpande', 'dhanu', 'dhiraj@gmail.com', 'dhiraj', 7744057219),
(33, 'ramesh sonar', 'ramesh', 'ramesh@zxc.com', 'ramesh', 7412589630),
(34, 'abhi wagh', 'raju', 'abhi@qwe.com', 'abhi', 852963),
(37, 'neha thorvay', 'nehu', 'neha@gmail.com', 'neha', 98521463),
(40, 'sita pawar', 'sita', 'sita@gmail.com', 'sita', 98748966),
(42, 'sadashiv appa', 'appa', 'appa@gmail.com', 'appa', 9879879879),
(43, 'madhuri dixit', 'mayu', 'dixit@gmail.com', 'dixit', 987987784),
(44, 'aman khan', 'twins', 'aman@gmail.com', 'aman', 96969655),
(45, 'sania mirza', 'sania', 'sania@gmail.com', 'sania', 2027272227),
(46, 'apurva khoche', 'appu', 'apurva@ccc.com', 'apurva', 5647893210),
(47, 'Abc Zxc', 'abc', 'abc12@gmail.com', 'abczxc', 1234565132),
(48, 'jasmin shaikh', 'abba', 'jasmin@gmail.com', 'jasmin', 15975364821),
(49, 'aaa aaa', 'aa', 'aaa@aaa.com', 'aaa', 9898986520),
(50, 'bbb bbb', 'bbb', 'bbb@bbb.com', 'bbb', 98654120);

-- --------------------------------------------------------

--
-- Table structure for table `stud_info`
--

CREATE TABLE IF NOT EXISTS `stud_info` (
  `sid` int(5) NOT NULL AUTO_INCREMENT,
  `unm` varchar(50) NOT NULL,
  `DOB` varchar(50) NOT NULL,
  `gen` varchar(30) NOT NULL,
  `ms` varchar(30) NOT NULL,
  `addr` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `img_path` varchar(500) NOT NULL,
  `deg` varchar(20) NOT NULL,
  `oth` varchar(50) NOT NULL,
  `uni` varchar(50) NOT NULL,
  `pyr` varchar(50) NOT NULL,
  `exp` varchar(50) NOT NULL,
  `skill1` varchar(50) NOT NULL,
  `skill2` varchar(50) NOT NULL,
  `certi` varchar(50) NOT NULL,
  `pcnm` varchar(50) NOT NULL,
  `ind` varchar(50) NOT NULL,
  `resume` varchar(4000) NOT NULL,
  `login_id` int(11) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=110 ;

--
-- Dumping data for table `stud_info`
--

INSERT INTO `stud_info` (`sid`, `unm`, `DOB`, `gen`, `ms`, `addr`, `email`, `img_path`, `deg`, `oth`, `uni`, `pyr`, `exp`, `skill1`, `skill2`, `certi`, `pcnm`, `ind`, `resume`, `login_id`) VALUES
(19, 'vishal more', '27-Jul-1992', 'Male', 'Unmarried', 'Pune kalewadi 411017', 'vishal.more45@gmail.com', '00Fsupra.jpg', 'B.E', 'NA', 'pune', '2016', 'Fresher', 'developer', 'design', 'java', 'NO', 'IT', '', 1),
(21, 'Ankit chodari', '6-Sep-1988', 'Male', 'Unmarried', 'Pune akurdi 411044', 'ankit.poo@gmail.com', 'aftow.jpg', 'B.E', 'NA', 'pune', '2014', '0-2', 'abc', 'xyz', 'NO', 'club29', 'Managment', '', 18),
(22, 'jaydeep more', '13-Oct-1995', 'Male', 'Unmarried', 'Pune Gahunje 411011', 'jay.145@gmail.com', 'three.jpg', 'B.E', 'NA', 'pune', '2016', 'Fresher', 'developer', 'design', 'big data', 'NA', 'IT', '', 2),
(23, 'Suraj kharote', '26-Jun-1995', 'Male', 'Unmarried', 'Pune kalewadi 411017', 'suraj124@gmail.com', '4jaguar5b.jpg', 'B.E', 'NA', 'pune', '2016', 'Fresher', 'front office', 'HTML5', 'java', 'NA', 'Computer', '', 3),
(24, 'Rahul surya', '12-May-1996', 'Male', 'Unmarried', 'Pune Akurdi 411044', 'rskumar@gmail.com', 'five.jpg', 'Other', 'BCS', 'Pune', '2016', 'Fresher', 'design', 'developer', 'php', 'NA', 'Electronic', '', 4),
(25, 'Shreedhar mulay', '8-May-1998', 'Male', 'Unmarried', 'Mumbai Airoli 411017', 'shree5@gmail.com', 'four.jpg', 'B.Tech', 'NA', 'Mumbai', '2016', 'Fresher', 'ciruit design', 'testing', 'logic gates', 'NA', 'Electronic', '', 8),
(26, 'kiran jadhav', '5-Dec-1990', 'Male', 'Unmarried', 'Mumbai Airoli 15', 'kiran123@gmail.com', 'image-slider-1.jpg', 'Other', 'MBA', 'Mumbai', '2014', '2-4', 'maketing', 'technical', 'NA', 'TECHNO', 'IT', '', 9),
(27, 'Ajit jadhav', '17-Aug-1988', 'Male', 'Unmarried', 'Mumbai Airoli 15', 'Ajit15@gmail.com', 'image-slider-2.jpg', 'M.E', 'NA', 'Mumbai', '2010', '2-4', 'machinary', 'manager', 'NA', 'abc', 'mechanical', '', 11),
(30, 'Vinayak More', '17-May-1987', 'Male', 'Unmarried', 'Pune kalewadi 411017', 'vinayak17587@gmail.com', 'image-slider-3.jpg', 'B.E', 'NA', 'Pune', '2009', '2-4', 'finance', 'marketing', 'NA', 'bajaj', 'mechanical', '', 19),
(33, 'vidyashree mulay', '11-Nov-1992', 'Female', 'Unmarried', 'Mumbai Airoli 04', 'vidya@gmail.com', 'image-slider-4.jpg', 'B.E', 'NA', 'mumbai', '2014', '0-2', 'circuit', 'design', 'NA', 'igate', 'E&tc', '', 20),
(35, 'pooja naik', '9-Jul-1991', 'Female', 'Unmarried', 'Pune kalewadi 411017', 'pooja@gmail.com', 'image-slider-5.jpg', 'Other', 'Diploma', 'DTE', '2012', '0-2', 'maketing', 'sales', 'NA', 'club29', 'managment', '', 21),
(37, 'nikita sonavane', '7-Jul-1993', 'Female', 'Unmarried', 'Pune pimpri 411014', 'niki@gmail.com', 'one.jpg', 'Other', 'diploma(E&TC)', 'MSBTE', '2016', 'Fresher', 'circuit ', 'logic gates', 'NA', 'NA', 'NA', '', 22),
(39, 'Shashikant kumbhar', '10-May-1990', 'Male', 'Unmarried', 'Pune Baner 411040', 'shashi009@gmal.com', 'two.jpg', 'M.E', 'NA', 'pune', '2014', 'Fresher', 'Chip coder', 'Designer', 'NA', 'NA', 'NA', '', 23),
(41, 'Yogesh Shinde', '2-Feb-1993', 'Male', 'Unmarried', 'Beed Gevrai 434400', 'shindeyogi@gmail.com', '4.jpg', 'B.E', 'NA', 'pune', '2015', '2-4', 'coder', 'VLSL', 'NA', 'Intel pvt ltd', 'Electronic', '', 24),
(43, 'manjushree deshpande', '5-Dec-1991', 'Female', 'Unmarried', 'Mumbai panvel 4004114', 'md123@gmail.com', 'eeeee.jpg', 'M.Tech', 'NA', 'mumbai', '2015', '0-2', 'Analog electronics', 'VLSL', 'NA', 'BHEL pvt ltd', 'Electrical', '', 25),
(49, 'pankaj Bhokse', '18-Feb-1995', 'Male', 'Unmarried', 'Pune Akurdi 411012', 'pankaj@gmail.com', 'Fall-09.jpg', 'B.Tech', 'NA', 'pune', '2016', 'Fresher', 'GGG', 'FFF', 'NA', 'NA', 'IT', '', 26),
(72, 'Aniket Bahir', '9-Jan-1990', 'Male', 'Unmarried', 'Aurangabad Cisdco 445532', 'aniket007@gmail.com', 'Fall-28.jpg', 'M.E', 'NA', 'Aurangabad', '2013', 'Fresher', 'Assembling', 'processor', 'Web technology', 'NA', 'IT', '', 27),
(85, 'pratap surway', '17-Jun-1987', 'Male', 'Married', 'Pune kalewadi 411017', 'pratap@gmail.com', '20150317_094736.jpg', 'B.E', 'NA', 'pune', '2012', '0-2', 'front office', 'developer', 'NA', 'zxc', 'IT', '', 29),
(86, 'sujata singh', '11-Apr-1990', 'Female', 'Married', 'Pune pune 411055', 'sujata@gmail.com', '0000.jpg', 'B.E', 'na', 'pune', '2010', 'Fresher', 'HR', 'Managment', 'na', 'na', 'na', '', 30),
(87, 'dhiraj deshpande', '15-Feb-1991', 'Male', 'Unmarried', 'Aurangabad paithan gate 405522', 'dhiraj@gmail.com', '20150720_075017.jpg', 'M.Tech', 'na', 'na', '2014', 'Fresher', 'designing', 'management', 'chif coder', 'na', 'na', 'vishalmore.docx', 31),
(89, 'ramesh sonar', '2-Oct-1990', 'Male', 'Married', 'baner Pune 411035', 'ramesh@zxc.com', '20160116_145419.jpg', 'M.E', 'na', 'pune', '2015', '0-2', 'Devloper', 'chip coder', 'JAVA', 'ABCpvt ltd', 'E&TC', 'Akshay Kale.docx', 33),
(90, 'abhi wagh', '5-Mar-1990', 'Male', 'Unmarried', 'pimpri Pune 41200', 'abhi@qwe.com', '; (37).jpg', 'M.E', 'na', 'pune', '2015', 'Fresher', 'Chip coder', 'testing', 'java', 'na', 'na', 'jaydeep more.docx', 34),
(91, 'akash waghamare', '6-Apr-1990', 'Male', 'Unmarried', 'baner Pune 412200', 'akash@gmail.com', '11140371_477761112390495_4332585931208538861_n.jpg', 'B.E', 'na', 'pune', '2015', 'Fresher', 'devloper', 'testing', 'DOT NET', 'na', 'na', '', 29),
(94, 'neha thorvay', '4-Mar-1995', 'Female', 'Unmarried', 'wakad Pune 411013', 'neha@gmail.com', '001-gsx1300r-rbFS.jpg', 'B.Tech', 'NA', 'pune', '2016', 'Fresher', '.net', 'java', 'na', 'na', 'na', 'jaydeep more.docx', 37),
(97, 'sita pawar', '3-Feb-1992', 'Female', 'Unmarried', 'wakad Pune 411046', 'sita@gmail.com', '001.jpg', 'B.E', 'na', 'pune', '2015', 'Fresher', 'devloper', 'coder', 'na', 'na', 'na', 'jaydeep more.docx', 40),
(99, 'sadashiv appa', '1-Feb-1992', 'Male', 'Unmarried', 'wakad Pune 411017', 'appa@gmail.com', '4_W.JPG', 'B.E', 'NA', 'pune', '2014', 'Fresher', 'java', '.net', 'na', 'na', 'na', 'jaydeep more.docx', 42),
(100, 'maduri dixit', '1-May-1982', 'Female', 'Unmarried', 'airoli Mumbai 411001', 'dixit@gmail.com', '3aa.jpg', 'M.E', 'na', 'pune', '2006', '2-4', 'coder ', 'testing', 'na', 'ABCpvt ltd', 'IT', 'jaydeep more.docx', 43),
(101, 'aman khan', '1-Jan-1992', 'Male', 'Unmarried', 'paithan gate Aurangabad 421055', 'aman@gmail.com', '1 (47).JPG', 'B.Tech', 'NA', 'Aurangabad', '2015', 'Fresher', 'devloper', 'chip coder', 'Dot net', 'na', 'NA', 'jaydeep more.docx', 44),
(102, 'sania mirza', '1-Jan-1992', 'Female', 'Unmarried', 'cidco Nashik 411585', 'sania@gmail.com', '16_W.JPG', 'B.E', 'na', 'nashik', '2015', 'Fresher', '.net', '-', 'MSCIT', 'NA', '', '', 45),
(103, 'sania mirza', '1-Jan-1992', 'Female', 'Unmarried', 'cidco Nashik 411585', 'sania@gmail.com', '16_W.JPG', 'B.E', 'na', 'nashik', '2015', 'Fresher', '.net', '-', 'MSCIT', 'NA', 'na', '', 45),
(104, 'sania mirza', '1-Jan-1992', 'Female', 'Unmarried', 'cidco Nashik 411585', 'sania@gmail.com', '16_W.JPG', 'B.E', 'na', 'nashik', '2015', 'Fresher', '.net', '-', 'MSCIT', 'NA', 'na', 'Suraj-project_Report.doc', 45),
(105, 'apurva khoche', '1-Mar-1995', 'Female', 'Unmarried', 'pimpri Pune 411018', 'apurva@cc.com', 'HON2D.JPG', 'B.E', 'na', 'pune', '2016', 'Fresher', 'product manager', 'sales representative`', 'Java', 'na', 'na', 'jaydeep more.docx', 46),
(106, 'Abc Zxc', '1-Jan-1990', 'Male', 'Unmarried', 'kalewadi Pune 411017', 'abc12@gmail.com', '2000_BUGATTI_16_4_VEYRON_1.JPG', 'M.E', 'na', 'pune', '2014', '0-2', 'Business devlope manager', 'Customer service representative', 'na', 'ABCpvt ltd', 'IT', 'jaydeep more.docx', 47),
(107, 'jasmin shaikh', '14-Oct-1991', 'Female', 'Unmarried', 'pimpri Pune 411015', 'jasmin@gmail.com', '2005-bmw-k12s-d-07-1[1].jpg', 'M.E', 'na', 'pune', '2012', '0-2', 'Design engineer', 'Customer service representative', 'java', 'iball pvt ltd', 'E&TC', 'jaydeep more.docx', 48),
(108, 'aaa aaa', '1-Oct-1992', 'Male', 'Unmarried', 'kalewadi Pune 411017', 'aaa@aaa.com', '; (36).jpg', 'M.E', 'NA', 'pune', '2014', '0-2', 'Business devlope manager', 'Customer service representative', 'na', 'zxc pvt ltd', 'IT', 'jaydeep more.docx', 49),
(109, 'bbb bbb', '1-Jan-1992', 'Male', 'Unmarried', 'kalewadi Pune 411017', 'bbb@bbb.com', '4car4_1024.jpg', 'M.Tech', 'NA', 'pune', '2014', '0-2', 'Business devlope manager', 'Customer service representative', 'java', 'ABCpvt ltd', 'IT', 'jaydeep more.docx', 50);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
